# -*- coding: utf-8 -*-
"""
Created on Tue Aug 21 16:55:30 2018

@author: James Ryland
"""


"""
This AxonGame will look very similar to the Fraser and Perkel 1990 Model

Key Differences:
-it simulates abstract axon branchlets
-creates a live window array and weight array
 that can be used by tensor flow
-Correlation strength uses actual pre-synaptic
 activations in it's calculations. 

"""
import pickle as pkl
import numpy as np
import scipy as sp
import scipy.sparse as spr
from scipy import ndimage as nd
import Ry_SparseUtil as spu
import Ry_SimpleStim as ss
import time

class AxonGame:
    
    
    def __init__(self, inChemMap, outChemMap, branchNum, plexNum=1, dendRad=0, dendStride=1, coactNum = 50, seed=False, seedNoise=0, targNoise=0, autoScale=False, branchNumStr=1, arbFitStr=.01, seniorStrs=[.0005, .0050], senLimit=100, boundStr=5, chemoStr1=.30, chemoStr2=.08, chemoAspect=[1,1], corrStr=3.0, compStr=.008, diffusionC = 10, diffusionA = 1.0, growProps=[1.0,1.0], branchExub = [2,  .125], prePeriod=0, exubPeriod = 200, stabilizePeriod = 100, nType='GauCrop', coType='CoAct', senByCo=False, senByCoBeta=.1, initDist="+uniform"):
        """
        Map Definitions
        3D arrays concat( AP[:,:],DV[:,:],Bound[:,:], axis=2 )
        inChemMap     :   Chemo-affinity map for projecting sheet  AP(0-1) DV(0-1) (1 in, 0 Out)
        outChemMap    :   Chemo-affinity map for recieving sheet   AP(0-1) DV(0-1) (1 in, 0 Out)
        
        For multiple input maps conjoined 2D array inChemMap usage:
        concat([APvec1 APvec2], axis=0)
        concat( AP[:],DV[:],Bound[:], axis=1 )
        Will try to auto-convert to usable representation
        
        Axon Arbor Forces:
        branchNum       :   desired number branchlets for axon arbors
        branchNumStr    :   strength of penalty for having too many branchlets or to few
        
        Axon Branchlet Forces
        boundStr    :   strength of boundary adhesion
        chemoStr1   :   strength of chemoaffinity AP,DV (target)
        chemoStr2   :   strength of chemoaffinity AP,DV (afferent)
        corrStr     :   strength of attraction for correlated firing
        compStr     :   strength of penalty for occupying the same space
        
        Developmentat stage forces:
        exubPeriod      :   steps till mature branching dist
        stabilizePeriod :   after exub, seniority ramps up reducing plasticity
        
        Methodology:
        nType   :   This is the type of probablistic neighborhood used for spawning branchlets
        coType  :   This is the method of measuring co-activation ("CoAct"=CoActivity  , "Cov"=abs(Covariance))
        senByCo :   Use running average of coact or cov to determine seniority
                    Instead of a lifespan tally
        
        plexNum :   This duplicates the dendritic blocks plexNum times,
                    effectively allowing for larger simulations by decreasing
                    the needed number of branchlets per arbor by reducing the
                    number of d-blocks to fill.
                    
        dendRad :   This gives each dendritic arbor access to a radius of
                    d-blocks smoothing the shape of the windows accross the
                    cortical sheet and reducing the number of branchlets needed
                    because each dendritic arbor spreads over a larger area.
                    Unfortunately, using this option voids this version of the
                    AxonGame from being able to keep track of all the unique
                    connections between the pre and post synaptic neurons. So
                    any W_pack matrices returned here will be randomly
                    generated each time they are called for according to
                    the init rules.
        
                    
        Default values are based on a 100x100 simulation
        Some values need to be scaled to compensate for
        larger or smaller models. The autoScale input
        will let the model try to guess how to adjust
        the input parameters/defaults for larger models.
        This is not an exact method and should not be
        relied on heavily.
        
        
        """
        

        # Store Values
        self.inChemMap = inChemMap
        self.outChemMap = outChemMap
        self.boundStr = boundStr
        self.chemoStr1 = chemoStr1
        self.chemoStr2 = chemoStr2
        self.corrStr = corrStr
        self.compStr = compStr
        self.branchNum = branchNum
        self.branchNumStr = branchNumStr
        self.arbFitStr = arbFitStr
        self.seniorStrs = seniorStrs
        self.seniorStr = seniorStrs[0]
        self.senLimit = senLimit
        self.euclidChemo = True
        self.normCoAct = True
        self.coactNum = coactNum
        self.plexNum = plexNum
        self.plexed = plexNum>1
        self.dendRad = dendRad
        self.dendStride = dendStride
        self.plexCount = self.plexNum*self.plexNum
        self.coType = coType
        self.senByCo = senByCo
        self.senByCoBeta = senByCoBeta
        self.growProps = growProps
        self.growProp = growProps[0]
        self.chemoAspect = chemoAspect

        self.initDist = initDist
        
        # Check if inputChemoMap is 2D or 3D and add dummy index if nescessary
        if len(np.array(self.inChemMap).shape)==2:
            self.inChemMap = np.reshape(self.inChemMap, [self.inChemMap.shape[0],1,3], order='F')
        
        # Generate internal reference values
        self.in_sz = self.inChemMap.shape[0:2]
        self.out_sz = self.outChemMap.shape[0:2]
        self.in_ln = np.prod(self.in_sz)
        self.out_ln = np.prod(self.out_sz)
        
        # Find desired number of branches in each window using pidgeon hole
        self.targDepth = (self.branchNum*self.in_ln)/self.out_ln
        
        
        # Determine auto-scaling of penalties for model size 
        self.as_mult = 1
        if autoScale:
            # Adjust reference values
            self.as_mult = np.max( self.out_sz)/100
            if self.as_mult>1:
                self.chemoStr1 = self.chemoStr1*self.as_mult
                branchExub[0] = branchExub[0]*self.as_mult
            
            
        # Generate static chemo affinity in and out map reference layers
        self.inChemVec_AP = np.ravel(inChemMap[:,:,0],order='F')
        self.inChemVec_DV = np.ravel(inChemMap[:,:,1],order='F')
        self.inChemVec_BND = np.ravel(inChemMap[:,:,2],order='F')
        
        self.outChemVec_AP = np.ravel(outChemMap[:,:,0],order='F')
        self.outChemVec_DV = np.ravel(outChemMap[:,:,1],order='F')
        self.outChemVec_BND = np.ravel(outChemMap[:,:,2],order='F')
        
        self.diffusionC = diffusionC
        self.diffusionA = diffusionA
        
        # Main data structures that need to be manipulated
        self.W = spr.lil_matrix( (self.in_ln, self.out_ln), dtype= np.float32)
        self.W_csc = self.W.tocsc()
            #This is the weights associated with the axon branchlets
            #Importantly this contains all information required to 
            #Maintain the arbor state from step to step
            #It will however be converted to different formats when nescessary
            #And many temporary derivations of this will be made   
        
        ## PLEX PLEX PLEX## Create multiple psuedo coppies of W kept with
        # Sparsity kept in sync
        if self.plexed:
            self.W_plexs = []
            for p in range(0, self.plexCount):
                self.W_plexs.append(spr.lil_matrix( (self.in_ln, self.out_ln), dtype= np.float32))
            
        self.C = spr.csc_matrix( (self.in_ln, self.out_ln), dtype= np.float32)
            # This is sparse matrix keeps track of how long a branch has been
            # Alive. Longer lived branches get a reward incouraging settling.
            # This can also be changed to a running correlation with neighbors
            # which can help stabalize maps too
        self.seed = seed
        self.seedNoise = seedNoise
        self.targNoise = targNoise
        self.seedBranches()
        
        
        # Store small number of incoming activation patterns
        self.act_buff = np.ones( (self.in_ln,1) )
        
        # Setup the branch proposel distribution
        self.branchOff = []
        self.numOffs = 0
        self.branchOffProb = []
        self.gauRad = int(np.ceil(15*self.as_mult))
        self.gauWidth = branchExub[0]
        self.gauWidths = branchExub
        self.pre = True
        self.prePeriod = prePeriod
        self.exubPeriod = exubPeriod
        self.stabilizePeriod = stabilizePeriod
        self.finishTime = prePeriod+exubPeriod+stabilizePeriod
        self.t_exub = 0
        self.nType = nType
        self.setupBranchDist()
        
        
        # Usefull display structures
        self.APmean = np.zeros(self.outChemVec_AP.shape)
        self.DVmean = np.zeros(self.outChemVec_DV.shape)
        self.branchDepth = np.zeros(self.outChemVec_DV.shape)
        self.avgFit = 0

        # Time Profile
        self.timeReport = ""
        
    
    def initWeights(self,shape):
        # This is essentially a wrapper for whatever init strategy has been
        # selected externally, will be called under a variety of circumstances
        
        # Importantly weights are not allowed to be true 0 to avoid de-synching
        # Sparsity between W and C matrices...       
        init = []
        if self.initDist=='+uniform':
            init = np.random.uniform(low=.000001, high=1, size=shape)
        elif self.initDist=='+-uniform':
            init = np.random.uniform(low=.000001, high=1, size=shape) 
            init = init*np.random.choice([-1,1], size=shape)
            
        return init
        
            
        
    def getPackedW(self, depthTo=[]):
        # create weight and index matrix that can be easily used by tensorflow like packages
        # if depthTo is specified w,ind matrices to maintain consistent size
        W_pack = []
        Ind_pack = []
        returnLst = [] 
            
        if self.dendRad==1 or self.dendRad==0:
            if not self.plexed:
                W_pack, Ind_pack = spu.ColSortPack(self.W)
            
            # PLEX PLEX PLEX ##
            # Make a plexing function to adjust W_pack and Ind_pack,
            else:
                W_pack, Ind_pack = PlexPack(self.W_plexs, self.out_sz, self.plexNum )
            
            
            depth = W_pack.shape[0]
            
                
            if depthTo==[]:
                returnLst = [W_pack, Ind_pack]
            else:
                if depth>=depthTo:
                    returnLst = [ W_pack[0:depthTo,:], Ind_pack[0:depthTo,:] ]
                else:
                    W_pack_crop   = np.zeros( (depthTo, W_pack.shape[1]) )                 
                    Ind_pack_crop = np.zeros( (depthTo, W_pack.shape[1]) ) 
                    
                    W_pack_crop[0:depth,:]   = W_pack                               
                    Ind_pack_crop[0:depth,:] = Ind_pack
                    
                    returnLst = [W_pack_crop.astype(np.float32), Ind_pack_crop.astype(int)]
        else:
            returnLst = self.getPackedW_DendriticWindow()
        
        return returnLst                      
                             
    def getPackedW_DendriticWindow(self, dendRad_Overwrite=-1, dendStride_Overwrite=-1):
        # This fetches a version of the window matrix where the post-synaptic
        # Neuron's dendrites extend over multiple dendritic blocks
        # This voids the ability of this version of the axon game to keep track
        # of each unique connection between dendritic arbors and axonal arbors
        # suitable randomly generated weights will be returned instead
        W_pack = []
        Ind_pack = []
        if (self.plexed):
            W_pack, Ind_pack = PlexPack(self.W_plexs, self.out_sz, self.plexNum )
        else:
            W_pack, Ind_pack = spu.ColSortPack(self.W)
        
        # Check to see if dendRad and dendStride are overwritten
        use_dendRad = self.dendRad
        use_dendStride = self.dendStride
        if dendRad_Overwrite:
            use_dendRad = dendRad_Overwrite            
        if dendStride_Overwrite:
            use_dendStride = dendStride_Overwrite
        
        # Calculate the new window and weight matrices
        size = np.array(self.out_sz)*self.plexNum
        Ind_pack_dw = DendriticWindow(size, Ind_pack, use_dendRad, use_dendStride)
        W_pack_dw = self.initWeights(Ind_pack_dw.shape)
        
        return W_pack_dw, Ind_pack_dw 
        

    def setPackedW(self, W_pack, Ind_pack):
        # update w using a packed weight/index matrices
        # Remember to use the forked indices when updating the axon game!!!
        
        # PLEX PLEX PLEX ##
        # Unplex Ind_pack and and W_pack into their n2-plex
        # Set the main W then the W_plexes
        if not self.plexed:
            self.W = spr.lil_matrix( spu.ColSortUnpack( W_pack, Ind_pack, self.W.shape) )
            
        else:
            self.W, self.W_plexs = UnplexPack(W_pack, Ind_pack, self.out_sz, self.plexNum, self.W.shape )
    
            
            
    def getPackedWT(self):
        # Get the transposed version of W pack
        # Convieniently this should have no empty elements
        # This can be usefull for auto-encoder learning
        W_pack_inv, Ind_pack_inv = spu.ColSortPack(self.W.transpose())
        return W_pack_inv, Ind_pack_inv
            
        
    def reset(self):
        self.W = spr.lil_matrix( (self.in_ln, self.out_ln), dtype= np.float32)
        self.seedBranches()
        
    def seedBranches(self):   
        
        # Normal init
        if not self.seed:
            inValid = np.nonzero(self.inChemVec_BND)[0]
            outValid = np.nonzero(self.outChemVec_BND)[0]
            
            rows = np.arange(0,self.in_ln)[inValid]
            outChoices = np.arange(0,self.out_ln)[outValid]
            
            cols = outChoices[np.random.choice(len(outChoices), size=[len(inValid)])]
            #self.W[rows,cols] = np.random.uniform(low=.000001, high=1, size=rows.shape) 
            self.W[rows,cols] = self.initWeights(rows.shape)
        
        # Target noise
        if self.targNoise>0:
            self.inChemVec_AP = self.inChemVec_AP + np.random.normal(scale=self.targNoise, size=self.inChemVec_AP.shape)
            self.inChemVec_DV = self.inChemVec_DV + np.random.normal(scale=self.targNoise, size=self.inChemVec_DV.shape)
        
        # Initialize branch closest to best spot
        if self.seed:
            for row in range(0, self.in_ln):
                # Only seed valid input branches
                if self.inChemVec_BND[row]==1:
                    noiseAP = np.random.normal(scale=self.seedNoise)
                    noiseDV = np.random.normal(scale=self.seedNoise)
                    diffAP = (self.inChemVec_AP[row]+noiseAP)- self.outChemVec_AP
                    diffDV = (self.inChemVec_DV[row]+noiseDV)- self.outChemVec_DV
                    bndBias = (1-self.outChemVec_BND)*10
                    col = np.argmin(np.abs(diffAP)+np.abs(diffDV)+bndBias)
                    self.W[row, col] = np.random.uniform(low=.000001, high=1, size=(1,1))[0,0]
                    ## PLEX PLEX PLEX##
                    # Do the same thing for the W_plexes
                    if self.plexed:
                        for p in range(0, self.plexCount):
                            #self.W_plexs[p][row,col] = np.random.uniform(low=.000001, high=1, size=(1,1))[0,0]
                            self.W_plexs[p][row,col] = self.initWeights((1,1))[0,0]
                            
                
    def setupBranchDist(self):
        # Setup unbiased moore-neighborhood
        if self.nType == "Moore":
            self.branchOff = np.array([ [-1,0],[0,-1],[1,0],[0,1] ], dtype=int)
            self.numOffs = 4
            self.branchOffProb = np.ones((4))/self.numOffs
        # Setup gaussian like branch probability        
        elif self.nType == "GauCrop":
            rad = self.gauRad
            X,Y = np.meshgrid( np.linspace(-rad,rad,rad*2+1),np.linspace(-rad,rad,rad*2+1), indexing='ij')
            width = self.gauWidth
            if width<self.gauWidths[1]:
                width = self.gauWidths[1]
            P = np.exp( -(np.power(X,2)+np.power(Y,2))/(2*width**2) )
            P[rad,rad] = 0
            P = P/(np.sum(P))
            xv = np.ravel(X, order='F')
            yv = np.ravel(Y, order='F')
            pv = np.ravel(P, order='F')
            self.branchOff = np.concatenate( (xv[:,np.newaxis], yv[:,np.newaxis]), axis=1 )
            self.branchOffProb = pv
            self.numOffs = xv.shape[0]
            
    
    def valueProject(self,values, mean=True):
        # takes values along axons and project average to pre-synaptic sheet        
            
        W_csc = self.W.tocsc()
        colStarts = W_csc.indptr
        rowsByCol = W_csc.indices
        
        # index projecting AP/DV values into branch positions
        val_dat = values[rowsByCol]
        V_csc = spr.csc_matrix((val_dat, rowsByCol, colStarts), shape=self.W.shape )
        V_sum = V_csc.sum(0)
        if mean:        
            V_avg = V_sum/(W_csc.getnnz(0)+.00001)
            return V_avg
        else:    
            return V_sum
        

    def applyDiffusion(self, blockVec, diff):
        # Apply gaussian smoothing to simulate diffusion of chemical signales
        if diff>0:
            sheetDensity = np.reshape(blockVec, self.out_sz, order='F')
            sheetDensity = nd.gaussian_filter(sheetDensity, (diff, diff), order=0, mode = 'mirror')
            blockVec = np.reshape(sheetDensity, blockVec.shape, order = 'F')
            return blockVec
        else:
            return blockVec
    
    def newActivitySamples(self, newActivity):
        self.act_buff = newActivity
        if self.coType == "Cov":
                self.act_buff = self.act_buff - np.mean(self.act_buff, axis=1, keepdims=True)    
        
    def run(self, numSteps, stepType="EV", newActivity=[]):
        
        self.timeReport = ""
        
        #reset activity buffer...
        if newActivity!=[]:
            self.newActivitySamples(newActivity)
           
                
        # Pick step to run
        if stepType=="EV":
            self.runEV(numSteps)
        elif stepType=="MCMC":
            self.runMCMC(numSteps) # NOT IMPLEMENTED YET
            
    def runEV(self, numSteps):
        
        
        for i in range(0,numSteps):

            
            #0) Adjust branching exuberance
            self.evExub()
            #1) Exploratory Branching
            arbSizes0, arbSizes1 = self.evBranch()
            
            # Calculate useful form for later
            self.W_csc = self.W.tocsc()
            
            #2) Average Co-activation all Branch
            A_data = 0
            if self.corrStr>0 and (not self.pre):
                A_data = self.evCoactiveFit()
            #3) APDV_nighb APDV_targ
            B_data = self.evChemoFit()
            #5) Competition
            C_data = self.evCompFit()
            #6) Update seniority
            D_data = 0
            if self.seniorStr>0:
                D_data = self.evSeniorFit(A_data)
            
            #7) total branch fitness 
            fit_data = -A_data+B_data+C_data-D_data
            self.avgFit = np.mean(fit_data)
            #8) fitness sparse, arbor branch mean fitness
            branchFit_csc, arbFit = self.evArborFit(fit_data)
            #9) prune to grow-ratio
            keep = self.evPruneNums(arbFit, arbSizes0, arbSizes1) 
            #10) sort-prune
            self.evArborPrune(branchFit_csc,keep)            
    
    def evExub(self):
        # Scale growth distribution to time relative to the end of the
        # Exuberance period
        self.t_exub = self.t_exub + 1
        
        # find where in exuberance phase we are
        t_e = self.t_exub-self.prePeriod
        val = np.maximum(np.minimum((t_e/self.exubPeriod), 1), 0)
        
        if t_e>=0:
            self.pre = False 
        
        # Lerp gauWidth
        self.gauWidth = val*(self.gauWidths[1]-self.gauWidths[0]) + self.gauWidths[0]
        
        # Lerp growProp
        self.growProp = val*(self.growProps[1]-self.growProps[0]) + self.growProps[0]
                             
        if self.t_exub>=(self.exubPeriod+self.prePeriod):
            self.nType="Moore"

        self.setupBranchDist()
        
        # find where in the stabilize phase we are
        t_s = self.t_exub-self.exubPeriod-self.prePeriod
        val = np.maximum(np.minimum(((t_s)/self.stabilizePeriod), 1), 0)
        
        # Lerp current seniorStr 
        self.seniorStr = val*(self.seniorStrs[1]-self.seniorStrs[0]) + self.seniorStrs[0]
        
        
    def evBranch(self):
        # Timer
        startTime = time.time()
        
        # Count arber branches before
        arbSizes0 = self.W.getnnz(axis=1)
        
        # Propose a set of random directions for all existing branches
        W_coo = spr.coo_matrix(self.W)
        
        # Propose new branch locations on 2D column grid
        xInds, yInds = spu.WrapInd2(self.out_sz, np.array(W_coo.col) )
        
        # Find random offsets
        offInds =  np.random.choice(self.numOffs, size=len(W_coo.col), p=self.branchOffProb)
        xOff = self.branchOff[:,0][offInds]
        yOff = self.branchOff[:,1][offInds]
        

        # Apply offsets safely with boundaries and get index in terms of c
        xInds_b, yInds_b = spu.SafeAdjacent2(self.out_sz, xInds, yInds , xOff, yOff )
        offset = spu.UnwrapInd2(self.out_sz, xInds_b, yInds_b )
        
        # Find available spots
        vals = np.asarray( self.W[np.array(W_coo.row),offset].todense())
        vals = np.squeeze(vals)
        empty = np.nonzero(vals==0)[0]

        
        # Setup proposed indices and init data for new branch locations
        propRow = np.array(W_coo.row)[empty]
        propCol = np.array(offset   )[empty]
        #propData = np.random.uniform(low=.000001, high=1, size=propRow.shape[0])
        propData = self.initWeights(propRow.shape[0])
        
        # Pair down new proposal branches accroding to current growth proportion
        keeps = np.random.uniform(size=propRow.shape[0])<self.growProp
        propRow = propRow[keeps]
        propCol = propCol[keeps]
        propData = propData[keeps]      
                       
        # Update W with new branches
        self.W[propRow,propCol]=propData   
        # PLEX PLEX PLEX ##
        #  Update the W_plexes too
        if self.plexed:
            for p in range(0, self.plexCount):
                #propData_plex = np.random.uniform(low=.000001, high=1, size=propRow.shape[0])
                propData_plex = self.initWeights(propRow.shape[0])
                self.W_plexs[p][propRow,propCol] = propData_plex
        
        # Count branches after
        arbSizes1 = self.W.getnnz(axis=1)
        
        
        self.timeReport = self.timeReport + "\nevBranch: "+str(np.round(time.time()-startTime,4))
        
        return arbSizes0, arbSizes1
    
    def evCoactiveFit(self):
        # Timer
        startTime = time.time()
        
        # Convert to compresses column format and extract indices
        W_csc = self.W_csc
        colStarts = W_csc.indptr
        rowsByCol = W_csc.indices
        
        # Penalty by branch
        coAct_data = np.zeros(W_csc.data.shape)
        SSb_data = np.zeros(W_csc.data.shape)
        SSc = np.zeros((1,self.W.shape[1]))
        
         
        for i in range(0, self.coactNum):

            # Choose an activation pattern
            t = np.random.choice(self.act_buff.shape[1])
            
            # Index activation into branch positions
            branchActs = self.act_buff[rowsByCol, t]

            # Sparse array of branches activating
            A_csc = spr.csc_matrix( (branchActs, rowsByCol, colStarts), shape=self.W.shape )
            SSb_data = SSb_data+np.square(A_csc.data)
            
            # Total activity per dendritic block
            blockActs = A_csc.sum(axis=0)
            SSc = SSc+np.square(blockActs)

            # Apply diffusion
            blockActs = self.applyDiffusion(blockActs, self.diffusionA)
            
            # Find total brach co-activation reward
            coAct_csc = A_csc.multiply(blockActs)
            coAct_data = coAct_data + coAct_csc.data
            
        self.timeReport = self.timeReport + "\nevCoact: "+str(np.round(time.time()-startTime,4))
        
        if self.coType == "Cov":
            coAct_data = np.abs(coAct_data)
            
        if self.normCoAct:            
            # Project the column sum of squares onto the branch locations
            ones = np.ones(W_csc.data.shape)
            NNZ = spr.csc_matrix( (ones, rowsByCol, colStarts), shape=self.W.shape )
            SSc_data = (NNZ.multiply(SSc) ).data
           
            # Apply modified r squared formula 
            coAct_data = coAct_data/(np.sqrt(SSb_data*SSc_data)+.000000000001)
            return coAct_data*self.corrStr/self.coactNum
        else:
            return coAct_data*self.corrStr/self.coactNum
            
    
        
    
    def evChemoFit(self):
        # Timer
        startTime = time.time()
        
        # Convert to csc format and extract indices
        W_csc = self.W_csc
        colStarts = W_csc.indptr
        rowsByCol = W_csc.indices
        
        # index projecting AP/DV values into branch positions
        branchAP = self.inChemVec_AP[rowsByCol]
        branchDV = self.inChemVec_DV[rowsByCol]

        # Make useful sparse matrices
        ones = np.ones(W_csc.data.shape)
        NNZ = spr.csc_matrix( (ones, rowsByCol, colStarts), shape=self.W.shape )
        AP = spr.csc_matrix( (branchAP, rowsByCol, colStarts), shape=self.W.shape )
        DV = spr.csc_matrix( (branchDV, rowsByCol, colStarts), shape=self.W.shape )
        
        # Penalty due to mismatch with target
        AP_t = NNZ.multiply(self.outChemVec_AP[np.newaxis,:])
        DV_t = NNZ.multiply(self.outChemVec_DV[np.newaxis,:])    
        APpenT = np.abs( AP.data-AP_t.data ) * self.chemoAspect[0]
        DVpenT = np.abs( DV.data-DV_t.data ) * self.chemoAspect[1]
        
        # Penalty due to mismatch with target
        #APpenT = np.abs((AP-NNZ.multiply(self.outChemVec_AP[np.newaxis,:])).data)
        #DVpenT = np.abs((DV-NNZ.multiply(self.outChemVec_DV[np.newaxis,:])).data)
        
        # Mean AP DV of branches per block
        nnzCol = AP.getnnz(axis=0)
        self.APmean = AP.sum(axis=0)/(nnzCol+.00000001)
        self.DVmean = DV.sum(axis=0)/(nnzCol+.00000001)
        self.APmean[:,nnzCol==0] = self.outChemVec_AP[nnzCol==0]
        self.DVmean[:,nnzCol==0] = self.outChemVec_DV[nnzCol==0]        
        APdiffuse = self.applyDiffusion(self.APmean, self.diffusionC) * self.chemoAspect[0]
        DVdiffuse = self.applyDiffusion(self.DVmean, self.diffusionC) * self.chemoAspect[1]
        
        # Penalty due to mismatch with neighbors
        APpenN = np.abs((AP-NNZ.multiply(APdiffuse)).data)
        DVpenN = np.abs((DV-NNZ.multiply(DVdiffuse)).data)
        
        # Penalty due to being out of bounds
        Bpen = NNZ.multiply(1-self.outChemVec_BND).data

        # Find total chemo branch penalties
        chemo_data = []
        if not self.euclidChemo:
            chemo_data = (APpenT+DVpenT)*self.chemoStr1+(APpenN+DVpenN)*self.chemoStr2
        else:
            chemo_data = np.sqrt((np.power(APpenT,2)+np.power(DVpenT,2)))*self.chemoStr1+np.sqrt((np.power(APpenN,2)+np.power(DVpenN,2)))*self.chemoStr2

        chemo_data = chemo_data+Bpen

        self.timeReport = self.timeReport + "\nevChemo: "+str(np.round(time.time()-startTime,4))
        
        return chemo_data        
        
        
    def evCompFit(self):
        # Timer
        startTime = time.time()
        
        # Convert to csc format and extract indices
        W_csc = self.W_csc
        colStarts = W_csc.indptr
        rowsByCol = W_csc.indices

        # Make useful sparse matrices
        ones = np.ones(W_csc.data.shape)
        NNZ = spr.csc_matrix( (ones, rowsByCol, colStarts), shape=self.W.shape )
        
        # Find the total number of competerters per branch
        self.branchDepth = NNZ.sum(axis=0)
        
        regulateDepth = True
        # Make competition relative to equal distribution target
        # This has no affect on behavior...
        if regulateDepth:
            dTarg = self.targDepth
        
        # Find each branches local competition
        compData = (NNZ.multiply(NNZ.sum(axis=0)-dTarg).data)*self.compStr

        return compData 
    
    def evSeniorFit(self, coData):
        # Timer
        startTime = time.time()
        
        W_csc = self.W_csc
        ones = np.ones_like(W_csc.data)
        colStarts = W_csc.indptr
        rowsByCol = W_csc.indices
        
        
        M_csc = []

        if not self.senByCo:
            # Create +1 sparse matric
            M_csc = spr.csc_matrix( (ones, rowsByCol, colStarts), shape=self.W.shape )
            # Add seniority to all living branches
            self.C = self.C + M_csc
        else:
            # Find cov or coact and make running average
            M_csc = spr.csc_matrix( (ones, rowsByCol, colStarts), shape=self.W.shape )  
            # Compute running average of coact or cov
            self.C = self.C*(1-self.senByCoBeta) + M_csc*(self.senByCoBeta)
        
        
        # Remove non-existent branches
        self.C = self.C.multiply(M_csc)
        self.C.eliminate_zeros()
        self.C = self.C.minimum(self.senLimit)
        
        # Hopefully this is the right shape
        senData = self.C.data*self.seniorStr
        
        
        self.timeReport = self.timeReport + "\nevSenior: "+str(np.round(time.time()-startTime,4))
        
        return senData
    
    def evArborFit(self, fit_data):
        
        # Convert to csc format and extract indices
        W_csc = self.W_csc
        colStarts = W_csc.indptr
        rowsByCol = W_csc.indices
        
        # Create sparse matrix of branch fitness
        branchFit_csc = spr.csc_matrix( (fit_data, rowsByCol, colStarts), shape=self.W.shape )
        
        # Find the mean branch fitness of each arbor
        arbFit = branchFit_csc.tocsr().mean(axis=1)
        
        
        return branchFit_csc, arbFit
        
        
    def evPruneNums(self, arbFit, arbSizes0, arbSizes1):
        
        #growth = arbSizes1-arbSizes0
        
        # Choose how many branches to eliminate (stable, grow, shrink)
        #delta = 1 + np.squeeze(np.asarray(arbFit) )*self.arbFitStr + (arbSizes1-self.branchNum)*self.branchNumStr
        #prune = growth*delta
        
        # Calculate the keep for each arbor
        #keep = np.round( np.maximum( np.minimum( arbSizes1-prune, 1 ), arbSizes1) )
        
        keep = self.branchNum
        
        return keep
        
    def evArborPrune(self, branchFit_csc, keep):
        # Timer
        startTime = time.time()
        
        # Find the top K mask for fitness
        keepMask = spu.RowTopK(-branchFit_csc,keep)
        
        # Mask with keep matrix to prune
        W_csc = self.W.tocsc().multiply( keepMask )
        W_csc.eliminate_zeros()
        self.W = W_csc.tolil()
        
        # PLEX PLEX PLEX
        if self.plexed:
            for p in range(0, self.plexCount):
                W_csc = self.W_plexs[p].tocsc().multiply( keepMask )
                W_csc.eliminate_zeros()
                self.W_plexs[p] = W_csc.tolil()
        
        
        
        self.timeReport = self.timeReport + "\nevPrune: "+str(np.round(time.time()-startTime,4))
        
        
        
    def runMCMC(self, numSteps):
        "One day maybe I will get around to this..."
        print("WIP")

        
def AgSaver(ag, name):
    # Store data (serialize)
    with open(name+'.rag.pickle', 'wb') as handle:
        pkl.dump(ag, handle, protocol=pkl.HIGHEST_PROTOCOL)
    
def AgLoader(name):
    # Load data (deserialize)
    with open(name+'.rag.pickle', 'rb') as handle:
        unserialized_data = pkl.load(handle)
        
    return unserialized_data    



    
def PlexPack(W_plexs, imShape, plexNum):
    """This function takes a set of plexed sparse W matrices and packs them
    such that the plexed columns are now in the right order for external use AKA, 
    the plexes will form square neighborhoods like simple upsampling an image"""
    
    in_ln = W_plexs[0].shape[0]
    out_ln = W_plexs[0].shape[1]
    out1 = imShape[0]
    out2 = imShape[1]
    p = plexNum*plexNum   
    p1 = plexNum
    p2 = plexNum   

    # ## Maybe we can use some clever reshapes to do this
    W_packs = []
    Ind_packs = []
    in_max = []
    # Pack each one and concatonate
    for i in range(len(W_plexs)):
        W_pack_i, Ind_pack_i = spu.ColSortPack(W_plexs[i])
        
        
        W_packs.append(W_pack_i[:,:,np.newaxis])
        Ind_packs.append(Ind_pack_i[:,:,np.newaxis])
        in_max = W_pack_i.shape[0]
        
        
    # in X out X p
    W_all = np.concatenate(W_packs,  axis=2)
    I_all = np.concatenate(Ind_packs, axis=2 )
    
    
    # 0    1      2      3    4
    # in X out1 X out2 X p1 X p2
    W_all = np.reshape(W_all, (in_max, out1, out2, p1, p2), order='F')
    I_all = np.reshape(I_all, (in_max, out1, out2, p1, p2), order='F')
    
    # permute
    # 0    3    1      4    2
    # in X p1 X out1 X p2 X out2
    perm = [0, 3, 1, 4, 2]
    
    # in X out1 X p1 X out2 X p2
    #perm = [0, 1, 3, 2, 4]
    
    W_all = np.transpose(W_all, perm )
    I_all = np.transpose(I_all, perm )
    
    # reshape down using -F order
    # in X p1Out1 X p2out2
    # in x p1out1,p2out2
    W_all = np.reshape(W_all, (in_max, out1*p1 * out2*p2), order='F')
    I_all = np.reshape(I_all, (in_max, out1*p1 * out2*p2), order='F')
    
    
    return W_all, I_all
    
def UnplexPack(W_all, I_all, imShape, plexNum, W_sz):
    
    #in_ln = W_plexs[0].shape[0]
    in_max = W_all.shape[0]     
    out_ln = np.prod(imShape)
    out1 = imShape[0]
    out2 = imShape[1]
    p = plexNum*plexNum   
    p1 = plexNum
    p2 = plexNum  
    #plexSize = int(out_ln/p)
    
    # Basically do the reverse of what happened up there...
    W_all = np.reshape(W_all, (in_max, p1, out1, p2, out2), order='F')
    I_all = np.reshape(I_all, (in_max, p1, out1, p2, out2), order='F')
    
    
    # Place into concatonated order
    # 0    1    2      3    4
    # in X p1 X out1 X p2 X out2
    
    # 0    2      3      1    4
    # in X out1 X out2 X p1 X p2
    perm = [0, 2, 4, 1, 3]
    W_all = np.transpose(W_all, perm )
    I_all = np.transpose(I_all, perm )
    
    # Reshape into monster pack matrix
    W_all = np.reshape(W_all , (in_max, out1*out2 * p1*p2) ,order='F')
    I_all = np.reshape(I_all , (in_max, out1*out2 * p1*p2) ,order='F')
    
    
    # ColSortUnpack each packed plex
    W_plexs = []
    for i in range(0, p):
        a = out_ln*i
        b = out_ln*(i+1)
        W_plex = W_all[:,a:b]
        I_plex = I_all[:,a:b]
        
        #print(np.sum( W_plex>0 ))
        
        W_csc = spu.ColSortUnpack(W_plex, I_plex, W_sz  )
        
        W_plexs.append( spr.lil_matrix( W_csc ))
    
        #print(W_plexs[i].nnz)
        
        
    # Copy the first plex as W
    W = W_plexs[0].copy()
    
    #print(W.shape)
    
    # return W, plexs
    return W, W_plexs
    
# Non circular roll
def GridQuickShift(Ind_grid, x,y):
    x = int(x)
    y = int(y)
    Ind_grid_shift = Ind_grid
    # x-shift
    xticks = np.arange(0,Ind_grid.shape[1])
    Ind_grid_shift = np.roll(Ind_grid_shift, x, axis=1)
    if x>0:
        Ind_grid_shift[:,:x,:] = 0    
    if x<0:
        Ind_grid_shift[:,x:,:] = 0

    # y-shift
    yticks = np.arange(0,Ind_grid.shape[2])
    Ind_grid_shift = np.roll(Ind_grid_shift, y, axis=2)
    if y>0:
        Ind_grid_shift[:,:,:y] = 0    
    if y<0:
        Ind_grid_shift[:,:,y:] = 0

    return Ind_grid_shift
    
def DendriticWindow(cShape, Ind_pack, radius=2, stride=1):
    
    # Setup XY grid
    d = int(radius*2+1)
    ticks = np.arange(-radius, radius+1)
    X,Y = np.meshgrid(ticks,ticks)
    
    
    # Find offsets within radius
    inRad = np.ravel( np.sqrt(X**2+Y**2)<radius, order='F')
    xin = np.ravel(X,order='F')[inRad] 
    yin = np.ravel(Y,order='F')[inRad]
    reps = len(xin)

    # Apply stride
    xin = xin*int(stride)
    yin = yin*int(stride)
    
    # reformat Ind_pack into AxHxW
    depth = Ind_pack.shape[0]
    Ind_grid = np.reshape(Ind_pack, [depth, cShape[0], cShape[1]], order='F')
    
    # pre-allocate larger array
    Ind_grid_new = np.zeros( (depth*reps, cShape[0],cShape[1]) , dtype=int)
    
    # Stack the shifted Ind_grids
    for i in range(0,reps):
        a = i*depth
        b = (i+1)*depth
        Ind_grid_new[a:b,:,:] = GridQuickShift(Ind_grid,xin[i],yin[i])

    # reformat Ind_pack into Ax(HW)
    Ind_pack_new = np.reshape(Ind_grid_new, [depth*reps, cShape[0]*cShape[1]], order='F')
    
    # Remove non-uniques for each column
    uCols = []
    depthMax = 0
    for i in range(0, Ind_pack_new.shape[1]):
        uCol = np.unique(Ind_pack_new[:,i])
        depthMax = np.maximum(len(uCol),depthMax)
        uCols.append(uCol)
    
    # Reconstitute as a matrix
    Ind_pack_final = np.zeros( (depthMax, Ind_pack.shape[1]), dtype=int )
    for i in range(0, Ind_pack_new.shape[1]):
        Ind_pack_final[0:len(uCols[i]),i] = uCols[i]
        
    return Ind_pack_final
    
import matplotlib.pyplot as plt    
class AxonGameFig:
    
    def __init__(self,ag):
        
        self.ag = ag
        self.fig = plt.figure()
        
        self.ax1 = self.fig.add_subplot(241)
        self.ax2 = self.fig.add_subplot(242)
        self.ax3 = self.fig.add_subplot(243)
        self.ax4 = self.fig.add_subplot(244)
        
        self.ax5 = self.fig.add_subplot(245)
        self.ax6 = self.fig.add_subplot(246)
        self.ax7 = self.fig.add_subplot(247)
        self.ax8 = self.fig.add_subplot(248)
        
        # Pick some random arbors to keep track of
        self.arbInds1 = np.random.choice(self.ag.in_ln, size = 3)
        self.arbInds2 = np.random.choice(self.ag.in_ln, size = 50)
        
        self.refresh()
        
    def refresh(self):
        
        self.chemPlot(self.ax1, self.ag.inChemMap, "Projecting Chemo Affinity Map")
        self.chemPlot(self.ax2, self.ag.outChemMap, "Pre-synaptic Chemo Affinity Map")
        self.plotArbors3(self.ax3, self.arbInds1, "3 Arbors")
        self.plotArborsN(self.ax4, self.arbInds2, "N Arbors")
        self.chemVecPlot(self.ax5, self.ag.out_sz, self.ag.APmean, self.ag.DVmean, "Mean Chemo Projection Org")
        self.depthVecPlot(self.ax6, self.ag.out_sz, self.ag.branchDepth, "Branch Depth")
        
        
        self.fig.canvas.draw()
        
        
    def chemPlot(self, ax, chemMap, title):
        img = np.zeros(chemMap.shape)
        img[:,:,0:2] = chemMap[:,:,0:2]*chemMap[:,:,2,np.newaxis]
        #img[:,:,2] = 1-.5*img[:,:,0]-.5*img[:,:,1]        
        ax.cla()        
        ax.set_title(title)        
        ax.imshow(img)
        ax.axis('off')  # clear x- and y-ticks

    def chemVecPlot(self, ax, shape, vec1, vec2, title):
        
        vec1=np.asarray(vec1)
        vec2=np.asarray(vec2)
        img = np.zeros((shape[0],shape[1],3))
        rChan = np.reshape(vec1,shape,order='F')[:,:,np.newaxis]
        bChan = np.reshape(vec2,shape,order='F')[:,:,np.newaxis]
        img[:,:,0:2] = np.concatenate( (rChan, bChan), axis=2)
        #img[:,:,2] = 1-.5*img[:,:,0]-.5*img[:,:,1]
        ax.cla()        
        ax.set_title(title)        
        ax.imshow(img)
        ax.axis('off')  # clear x- and y-ticks

    def depthVecPlot(self, ax, shape, vec, title):
        
        vec=np.asarray(vec)
        vec = vec/(np.max(vec)+.000001)
        img = np.zeros((shape[0],shape[1],3))
        rChan = np.reshape(vec,shape,order='F')[:,:,np.newaxis]
        img[:,:,:] = rChan*np.ones((1,1,3))
        ax.cla()        
        ax.set_title(title)        
        ax.imshow(img)
        ax.axis('off')  # clear x- and y-ticks

    def plotArbors3(self, ax, arbInds, title):
        
        sheetShape = self.ag.out_sz
        arbs = np.asarray( self.ag.W[arbInds,:].todense()!=0 ).astype(float)
        arbsRGB = np.reshape(arbs, (3,sheetShape[0],sheetShape[1]), order='F' )
        arbsRGB = np.transpose(arbsRGB, [1,2,0])
        ax.cla()
        ax.set_title(title)
        ax.imshow(arbsRGB)
        ax.axis('off')  # clear x- and y-ticks
        
    def plotArborsN(self, ax, arbInds, title):
        arbNum = arbInds.shape[0]
        sheetShape = self.ag.out_sz
        
        arbsN = np.asarray( self.ag.W[arbInds,:].todense()!=0 ).astype(float)        
        arbsN = np.reshape(arbsN, (arbNum,sheetShape[0],sheetShape[1]), order='F' )
        arbsN = np.transpose(arbsN, [1,2,0])
        
        dispImg = np.sum( arbsN, axis=2)
        dispImg = dispImg/np.max(dispImg)
        dispImg = dispImg[:,:,np.newaxis]*np.ones((1,1,3))
        
        ax.cla()
        ax.set_title(title)
        ax.imshow(dispImg)
        ax.axis('off')  # clear x- and y-ticks




class AxonGameTraceFig:
    
    def __init__(self,ag, values, title, mean=True, split=False, cmap='gray'):
        
        self.ag = ag
        self.fig = plt.figure()
        self.values = values
        self.title = title
        
        self.split= split
        self.mean = mean
        self.cmap = cmap
        
        self.ax1 = self.fig.add_subplot(111)
        
        self.refresh()
        
    def refresh(self):
        
        valImg = self.plotValues(self.ax1, self.values, self.title)
        self.fig.canvas.draw()
        return valImg
        
    def plotValues(self, ax, values, title):
        
        valVec = self.ag.valueProject(values, mean=self.mean)
        
        if self.split:
            valVec = (valVec>0)*1.0
        
        valImg = np.reshape(valVec, self.ag.out_sz, order="F")
        dispImg = valImg/(np.max(valImg)+.000000000001)
        
        ax.cla()
        ax.set_title(title)
        ax.imshow(dispImg, cmap=self.cmap)
        ax.axis('off')  # clear x- and y-ticks

        return valImg


# Simple Chemo-Affinity area geometries        
def AreaChemMaker(size, aType = "rectangle", gap=2):
    
    ap = np.ones(size)
    dv = np.ones(size)
    b = np.ones(size)
    
    # LR-Fused maps
    if aType=="rectangle":
        ap = ap * np.linspace(0,1,ap.shape[0])[:,np.newaxis]
        dv = dv * np.linspace(0,1,ap.shape[1])[np.newaxis,:]
        b[:,:] = 0
        b[3:-3,3:-3] = 1    

    # LR-Fused maps
    if aType=="rectangle-no-bound":
        ap = ap * np.linspace(0,1,ap.shape[0])[:,np.newaxis]
        dv = dv * np.linspace(0,1,ap.shape[1])[np.newaxis,:]                      


    # LR-Fused maps
    if aType=="rectangle-split":
        ap = ap * np.linspace(0,1,ap.shape[0])[:,np.newaxis]
        dv = dv * np.linspace(0,1,ap.shape[1])[np.newaxis,:]                      
        cent = ap.shape[1]/2
        gapA = np.floor(cent-gap).astype(int)
        gapB = np.ceil(cent+gap).astype(int)
        b[:,:] = 0
        b[3:-3,3:-3] = 1 
        b[:,gapA:gapB] = 0
                              
    if aType=="polar1":
        x = ap * np.linspace(-1,1,ap.shape[0])[:,np.newaxis]
        y = dv * np.linspace(-1,1,ap.shape[1])[np.newaxis,:]
        rho, phi = cart2pol(x,y)
        b[:,:] = rho<1
        ap = rho
        dv = phi/(2*np.pi)+.5

    if aType=="polar-split":
        x = ap * np.linspace(-1,1,ap.shape[0])[:,np.newaxis]
        y = dv * np.linspace(-1,1,ap.shape[1])[np.newaxis,:]
        rho, phi = cart2pol(x,y)
        b[:,:] = rho<1
        ap = ((rho*np.sign(y))+1)/2
        dv = np.abs(phi/(np.pi)) 

                          
    # LR-Gap Maps
    
    # Rounded Rectangle
    # Rectangle
    # Disk
    # Radial Disk
    
    # Combine
    chemMap = np.concatenate((ap[:,:,np.newaxis],dv[:,:,np.newaxis],b[:,:,np.newaxis]), axis=2)
    return chemMap
    
def RetinaChemMaker(x,y):
    # Given X,Y rentinal location for units
    # Create a Chemo Map
    print("WIP")

    
# Some simple functions for dealing with polar coordinate conversions
# Source stack exchange
def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)


#TestBoxSweeps()
#______________________________________________________________________________NORMAL TEST

def testAxonGame():
    import matplotlib.pyplot as plt
    
    inSize = [50,50]
    cSize = [50,75] 
    pacDepth = 50

    # Spontaniou activations
    #spontActs, spontImgs = SimpleBoxSweeps([50,50], 60, 50, 5)
    
    # Spontanious 2 Retina
    spontActs, spontImgs, eye = ss.SimpleBiSweeps(inSize, 60, 50, 5)
    
    # Input/output chemo maps
    chemMap1 = AreaChemMaker( inSize, aType="rectangle")
    chemMap2 = AreaChemMaker( cSize, aType="rectangle")
    # DEFINE AXON GAME AND VIEWER
    ag = AxonGame(chemMap1, chemMap2, coactNum=10, branchNum=25, autoScale=True, coType="Cov")
    
    # Figures
    agFig = AxonGameFig(ag)
    agTrace = AxonGameTraceFig(ag, eye, "Binocularity") 
    

    start = time.time()
    
    for j in range(0,1):
        for i in range(0,500):
            
            # Update model
            ag.run(1, newActivity = spontActs)
            
            # Test packing
            W_pack, Ind_pack = ag.getPackedW(depthTo=pacDepth)
            ag.setPackedW(W_pack, Ind_pack)
            
            # update graphics
            agFig.refresh()
            agTrace.refresh()
            
            # Calculate time
            mins = int( np.floor((time.time()-start)/60))
            secs = int( np.floor((time.time()-start)-mins*60))
            
            print("\n____________________")
            print("Time      : "+str(mins)+"m  "+str(secs)+"s")
            print("Step      : "+str(i))
            print("Exuberance: "+str(np.round(ag.gauWidth, 3)))
            print("Seniority : "+str(np.round(ag.seniorStr, 6)))
            print("Sparsity  : "+str(np.round(ag.W.nnz/np.prod(ag.W.shape), 4 ))+"%")
            print("Max Depth : "+str(np.max(ag.branchDepth))+" brchs")
            print("Avg Depth : "+str(np.round(np.mean(ag.branchDepth)))+" brchs")
            print("Pac Depth : "+str(W_pack.shape[0])+' brchs')
            print("Penalty   : "+str(np.round(ag.avgFit, 3))+" p/b")
            print(ag.timeReport)
            
            plt.pause(.1)
            
        ag.reset()

#testAxonGame()
        
#______________________________________________________________________________SEEDED TEST POOL-LIKE
def testAxonGame_Seeded():
    import matplotlib.pyplot as plt
    
    # Spontaniou activations
    #spontActs, spontImgs = SimpleBoxSweeps([50,50], 60, 50, 5)
    
    # Spontanious 2 Retina
    spontActs, spontImgs, eye = ss.SimpleBiSweeps([100,100], 60, 50, 5)
    
    # Input/output chemo maps
    chemMap1 = AreaChemMaker( (100,100), aType="rectangle")
    chemMap2 = AreaChemMaker( (200,250), aType="polar1")
    # DEFINE AXON GAME AND VIEWER
    ag = AxonGame(chemMap1, chemMap2, branchNum=10, seniorStrs = [.002, .004], boundStr = 10, corrStr=6, seed=True, branchExub = [1.5, .025])
    
    # Figures
    agFig = AxonGameFig(ag)
    agTrace = AxonGameTraceFig(ag, eye, "Binocularity") 
    
    
    start = time.time()
    
    for i in range(0,50):
        
        # Update model
        ag.run(1, newActivity = spontActs)
        
        # update graphics
        agFig.refresh()
        agTrace.refresh()
        
        # Calculate time
        mins = int( np.floor((time.time()-start)/60))
        secs = int( np.floor((time.time()-start)-mins*60))
        
        print("\n____________________")
        print("Time      : "+str(mins)+"m  "+str(secs)+"s")
        print("Step      : "+str(i))
        print("Exuberance: "+str(np.round(ag.gauWidth, 3)))
        print("Seniority : "+str(np.round(ag.seniorStr, 3)))
        print("Sparsity  : "+str(np.round(ag.W.nnz/np.prod(ag.W.shape), 4 ))+"%")
        print("Max Depth : "+str(np.max(ag.branchDepth))+" brchs")
        print("Avg Depth : "+str(np.round(np.mean(ag.branchDepth)))+" brchs")
        print("Penalty   : "+str(np.round(ag.avgFit, 3))+" p/b")
        
        print(ag.timeReport)
        plt.pause(.1)
        
    
    
            
#testAxonGame_Seeded()


#______________________________________________________________________________SEEDED TEST POOL-LIKE
def testAxonGame_Blank_Seeded():
    import matplotlib.pyplot as plt
    
    # Input/output chemo maps
    chemMap1 = AreaChemMaker( (100,100), aType="rectangle")
    chemMap2 = AreaChemMaker( (200,250), aType="rectangle")
    # DEFINE AXON GAME AND VIEWER
    ag = AxonGame(chemMap1, chemMap2, branchNum=10, autoScale=True, seed=True, nType='Moore', chemoStr1 = 1, boundStr = 10, corrStr=0, compStr = 0)
    
    # Figures
    agFig = AxonGameFig(ag)
    #agTrace = AxonGameTraceFig(ag, np.ones(), "Binocularity") 
    
    
    start = time.time()
    
    for i in range(0,15):
        
        # Update model
        ag.run(1)
        
        # update graphics
        agFig.refresh()
        #agTrace.refresh()
        
        # Calculate time
        mins = int( np.floor((time.time()-start)/60))
        secs = int( np.floor((time.time()-start)-mins*60))
        
        print("\n____________________")
        print("Time      : "+str(mins)+"m  "+str(secs)+"s")
        print("Step      : "+str(i))
        print("Exuberance: "+str(np.round(ag.gauWidth, 3)))
        print("Seniority : "+str(np.round(ag.seniorStr, 3)))
        print("Sparsity  : "+str(np.round(ag.W.nnz/np.prod(ag.W.shape), 4 ))+"%")
        print("Max Depth : "+str(np.max(ag.branchDepth))+" brchs")
        print("Avg Depth : "+str(np.round(np.mean(ag.branchDepth)))+" brchs")
        print("Penalty   : "+str(np.round(ag.avgFit, 3))+" p/b")
        plt.pause(.1)
            
        
#testAxonGame_Blank_Seeded()

#______________________________________________________________________________PLEXING TEST

def testAxonGame_Plex():
    import matplotlib.pyplot as plt
    
    inSize = [50,50]
    cSize = [50,50] 
    pacDepth = 400

    # Spontaniou activations
    #spontActs, spontImgs = SimpleBoxSweeps([50,50], 60, 50, 5)
    
    # Spontanious 2 Retina
    spontActs, spontImgs, eye = ss.SimpleBiSweeps(inSize, 60, 50, 5)
    
    # Input/output chemo maps
    chemMap1 = AreaChemMaker( inSize, aType="rectangle")
    chemMap2 = AreaChemMaker( cSize, aType="rectangle")
    # DEFINE AXON GAME AND VIEWER
    
    
    plex = 2
    branchNum = 200
    
    
    ag = AxonGame(chemMap1, chemMap2, plexNum=plex, coactNum=10, branchNum=branchNum, autoScale=True)
    
    # Figures
    agFig = AxonGameFig(ag)
    agTrace = AxonGameTraceFig(ag, eye, "Binocularity") 
    

    start = time.time()
    
    for j in range(0,1):
        for i in range(0,500):
            
            # Update model
            ag.run(1, newActivity = spontActs)
            
            # Test packing
            W_pack, Ind_pack = ag.getPackedW(depthTo=pacDepth)
            ag.setPackedW(W_pack, Ind_pack)
            
            print(W_pack.shape)
            
            # update graphics
            agFig.refresh()
            agTrace.refresh()
            
            # Calculate time
            mins = int( np.floor((time.time()-start)/60))
            secs = int( np.floor((time.time()-start)-mins*60))
            
            print("\n____________________")
            print("Time      : "+str(mins)+"m  "+str(secs)+"s")
            print("Step      : "+str(i))
            print("Exuberance: "+str(np.round(ag.gauWidth, 3)))
            print("Seniority : "+str(np.round(ag.seniorStr, 6)))
            print("Sparsity  : "+str(np.round(ag.W.nnz/np.prod(ag.W.shape), 4 ))+"%")
            print("Max Depth : "+str(np.max(ag.branchDepth))+" brchs")
            print("Avg Depth : "+str(np.round(np.mean(ag.branchDepth)))+" brchs")
            print("Pac Depth : "+str(W_pack.shape[0])+' brchs')
            print("Penalty   : "+str(np.round(ag.avgFit, 3))+" p/b")
            print(ag.timeReport)
            
            plt.pause(.1)
            
        ag.reset()

#testAxonGame_Plex()


def testAxonGame_saving():
    import matplotlib.pyplot as plt
    
    # Spontaniou activations
    #spontActs, spontImgs = SimpleBoxSweeps([50,50], 60, 50, 5)
    
    # Spontanious 2 Retina
    spontActs, spontImgs, eye = ss.SimpleBiSweeps([100,100], 60, 50, 5)
    
    # Input/output chemo maps
    chemMap1 = AreaChemMaker( (100,100), aType="rectangle")
    chemMap2 = AreaChemMaker( (200,250), aType="polar1")
    # DEFINE AXON GAME AND VIEWER
    ag = AxonGame(chemMap1, chemMap2, branchNum=10, seniorStrs = [.002, .004], boundStr = 10, corrStr=6, seed=True, branchExub = [1.5, .025])
    
    # Figures
    agFig = AxonGameFig(ag)
    agTrace = AxonGameTraceFig(ag, eye, "Binocularity") 
    
    
    start = time.time()
    
    for i in range(0,25):
        
        # Update model
        ag.run(1, newActivity = spontActs)
        
        # update graphics
        agFig.refresh()
        agTrace.refresh()
        
        # Calculate time
        mins = int( np.floor((time.time()-start)/60))
        secs = int( np.floor((time.time()-start)-mins*60))
        
        print("\n____________________")
        print("Time      : "+str(mins)+"m  "+str(secs)+"s")
        print("Step      : "+str(i))
        print("Exuberance: "+str(np.round(ag.gauWidth, 3)))
        print("Seniority : "+str(np.round(ag.seniorStr, 3)))
        print("Sparsity  : "+str(np.round(ag.W.nnz/np.prod(ag.W.shape), 4 ))+"%")
        print("Max Depth : "+str(np.max(ag.branchDepth))+" brchs")
        print("Avg Depth : "+str(np.round(np.mean(ag.branchDepth)))+" brchs")
        print("Penalty   : "+str(np.round(ag.avgFit, 3))+" p/b")
        
        print(ag.timeReport)
        plt.pause(.1)
    
        
    print("___________________\n____________________")
    print("Saving and loading test")
    
    print('\nSAVING')
    AgSaver(ag, 'test')
    
def testAxonGame_loading():
    print('\nLOADING')
    ag = []
    ag = AgLoader('test')

    
    # Figures
    # Spontanious 2 Retina
    spontActs, spontImgs, eye = ss.SimpleBiSweeps([100,100], 60, 50, 5)
    agFig = AxonGameFig(ag)
    agTrace = AxonGameTraceFig(ag, eye, "Binocularity") 
    
    start = time.time()
    for i in range(0,25):
        
        # Update model
        ag.run(1, newActivity = spontActs)
        
        # update graphics
        agFig.refresh()
        agTrace.refresh()
        
        # Calculate time
        mins = int( np.floor((time.time()-start)/60))
        secs = int( np.floor((time.time()-start)-mins*60))
        
        print("\n____________________")
        print("Time      : "+str(mins)+"m  "+str(secs)+"s")
        print("Step      : "+str(i))
        print("Exuberance: "+str(np.round(ag.gauWidth, 3)))
        print("Seniority : "+str(np.round(ag.seniorStr, 3)))
        print("Sparsity  : "+str(np.round(ag.W.nnz/np.prod(ag.W.shape), 4 ))+"%")
        print("Max Depth : "+str(np.max(ag.branchDepth))+" brchs")
        print("Avg Depth : "+str(np.round(np.mean(ag.branchDepth)))+" brchs")
        print("Penalty   : "+str(np.round(ag.avgFit, 3))+" p/b")
        
        print(ag.timeReport)
        plt.pause(.1)

#testAxonGame_saving()
#testAxonGame_loading()