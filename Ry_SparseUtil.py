# -*- coding: utf-8 -*-
"""
Created on Wed Aug 22 15:25:39 2018

@author: James Ryland
"""

"""
This is a series of functions for manipulating virtual 3D sparse arrays
that will be usefull for representing cellular automata like the Axon
Games
"""
import numpy as np
import scipy as sp
import scipy.sparse as spr

def ColSortPack(A, sortType='nnz'): ## Sorts and packs version of A that is dense
    
    # Make sure A is CSC format
    A_csc = sp.sparse.csc_matrix(A)
    
    # DATA VECTORS
    data = A_csc.data
    colStarts = A_csc.indptr # Start/stop points for each column 
    rowIndsByCol = A_csc.indices # row indices for each column 

    colDepths = (colStarts[1:]-colStarts[:-1])
    maxColDepth = np.max(colDepths)    

    
    # Forge new row inds to compress into reasonable size 
    rowIndsByCol_press = np.zeros(np.size(rowIndsByCol))
    colNum = colStarts.shape[0]
    for i in range(0,colNum-1):
        
        # span is 1..# of nnz elements in col (i)
        span = colDepths[i]
        # This will be the row inds for sorted sparse A
        rowIndsByCol_press[colStarts[i]:colStarts[i+1]] = np.arange(0,span)

    # Sort in place
    data_sort =  np.zeros(np.size(rowIndsByCol))
    rowIndsByCol_sort = np.zeros(np.size(rowIndsByCol))
    
    if sortType=='value':
        for i in range(0,colNum-1):
            
            # Sorted row indices for column (i)
            sortInds =  np.argsort(-data[colStarts[i]:colStarts[i+1]])
            # Apply sort to column (i) in data
            data_sort[colStarts[i]:colStarts[i+1]] = data[colStarts[i]:colStarts[i+1]][sortInds]
            # Apply sort to column (i) row inds    
            rowIndsByCol_sort[colStarts[i]:colStarts[i+1]] = rowIndsByCol[colStarts[i]:colStarts[i+1]][sortInds]
    
    elif sortType=='nnz':
        # Apply sort to column (i) in data
        data_sort = data
        rowIndsByCol_sort = rowIndsByCol
        
            

    offset = 1  # In order to disambiguate between index 0 in a column and a
                # sparse element we add 1 to all indices
    
    # Create new sparse A in packed oder, and row indices in packed order
    A_pack_sp = sp.sparse.csc_matrix((data_sort, rowIndsByCol_press, colStarts), shape=A.shape)
    A_pack_inds_sp = sp.sparse.csc_matrix((rowIndsByCol_sort+offset, rowIndsByCol_press, colStarts), shape=A.shape)
    
    # Create new dense packed A and dense packed Indices
    A_pack = A_pack_sp[0:maxColDepth, :].toarray()
    Inds_pack = A_pack_inds_sp[0:maxColDepth, :].toarray()
    
    # Returning A_crop, Inds_crop
    # This is enough to reconsitute
    # The original sparse matrix
    return A_pack, Inds_pack
    


    
def ColSortUnpack(A_pack, Inds_pack, shape): ## Unpacks a Column Packed version of A in csc format
    
    A_pack_sp = spr.csc_matrix(A_pack)
    Inds_pack_sp = spr.csc_matrix(Inds_pack)
    
    # DATA VECTORS
    data = A_pack_sp.data
    colStarts = A_pack_sp.indptr # Start/stop points for each column 
    rowIndsByCol = Inds_pack_sp.data # row indices for each column 
    
    offset = 1
    A_csc = spr.csc_matrix((data, rowIndsByCol-offset, colStarts), shape=shape)
    A_csc.sort_indices()
    
    return A_csc

        
# Takes 1D index and converts to 2D index: Parrallel Compatable    
def WrapInd2(shape, c):

    i = np.mod(c,shape[0])
    j = np.floor(c/shape[0]).astype(np.int32)
    return i,j
    
def UnwrapInd2(shape, i, j):
    
    return j*shape[0]+i

def SafeAdjacent2(shape, i, j, iOff, jOff): ## uses adjaceny offset to safely index on a bounded 2D board 
    
    # Find out of bounds upper
    #iSub = -np.maximum(i+iOff-(shape[0]-1), 0)
    #jSub = -np.maximum(j+jOff-(shape[1]-1), 0)
    
    # Find out of bounds lower
    #iAdd = -np.minimum(i+iOff,0)
    #jAdd = -np.minimum(j+jOff,0)
    
    # Normal moves in bounds + reflections
    iAdj = np.maximum(np.minimum(i+iOff,shape[0]-1),0) # +iSub+iAdd
    jAdj = np.maximum(np.minimum(j+jOff,shape[1]-1),0) # +jSub+jAdd
    
    return iAdj, jAdj


def WindexGather(): ## Gathers values by row indix from a vector 
    print("WOO")

def ColSum():
    print("WOO2")


def RowTopK(A, k):
    # Make a general purpose mask matrix for sparse multiplication
    # That will only keep the row topK, this should in theory be pretty fast
    
    A_csr = spr.csr_matrix(A)
    
    data = A_csr.data
    rowStarts = A_csr.indptr # Start/stop points for each row
    colIndsByRow = A_csr.indices # column indices for each row 
    
    # in case k is scaler
    k = np.squeeze(k)*np.ones( (A.shape[0] ) ).astype(int)
    
    data_mask =  np.zeros(np.size(colIndsByRow))
    
    rowNum = rowStarts.shape[0]
    for i in range(0,rowNum-1):
            
        # Find the permutation that sorts the row
        #sortInds = np.argsort(-data[rowStarts[i]:rowStarts[i+1]])
        
        # TRY USING ARGPARTITION
        nnz_ln = rowStarts[i+1]-rowStarts[i]
        sortInds = np.arange(nnz_ln)
        if  (nnz_ln)>k[i]:
            sortInds = np.argpartition(-data[rowStarts[i]:rowStarts[i+1]], k[i] )
        
        # Give rank order to each element using sort inds
        sortMask = np.zeros(sortInds.shape)
        sortMask[sortInds] = (np.arange(rowStarts[i+1]-rowStarts[i]))<k[i]
        
        # Add to full mask data vector
        data_mask[rowStarts[i]:rowStarts[i+1]] = sortMask
            
    A_topK_mask = spr.csr_matrix( (data_mask, colIndsByRow, rowStarts), shape=A.shape )
    A_topK_mask.eliminate_zeros()

    return A_topK_mask
    
def TestUtils():
    
    sz = 5
    
    print("\nSmall Sparse Packing Test ("+str(sz)+" to "+str(sz)+")")
    A = sp.sparse.random(sz,sz, 0.7, format='lil')
    A_pack, Inds_pack = ColSortPack(A, 'nnz')
    A_unp = ColSortUnpack(A_pack, Inds_pack, A.shape)
    
    A_topK_mask = RowTopK(A, 2)
    A_topK =  A_topK_mask.multiply( spr.csr_matrix(A) )
    
    print("\nOriginal")
    print(np.round(A.todense()*10))
    print("\nRowTopK")
    print(np.round(A_topK.todense()*10))
    print("\nCol Packed Values")
    print(np.round(A_pack*10))
    print("\nCol Packed Inds (row ind starts a 1)")
    print(Inds_pack)
    print("\nUnpacked Values")
    print(np.round(A_unp.todense()*10))
    
    if np.sum(np.abs(A.todense()-A_unp.todense())) == 0:
        print("\nPack-Unpack Success_______________(Pass)")
        
    else:
        print("\nPack-Unpack Failure_______________(Fail)")
    
    shape = [4,5]
    c1 = np.random.randint(0, np.prod(shape)-1)
    c2 = np.random.randint(0, np.prod(shape)-1)
    c = np.array([c1, c2])
    i,j = WrapInd2(shape, c)
    c_un = UnwrapInd2(shape, i, j)
    
    print("\nWrap Unwrap Test")
    print("Index  : c="+str(c))
    print("Wrap   : i="+str(i)+", j="+str(j))
    print("Unwrap : c="+str(c_un))
    
    if np.sum(np.abs(c-c_un)) == 0:
        print("Wrap-Unwrap Success_______________(Pass)")
        
    else:
        print("Wrap-Unwrap Failure_______________(Fail)")
    
    shape = [4, 5]
    i    = np.array([ 0, 3, 3, 0])
    j    = np.array([ 0, 0, 4, 4])
    iOff = np.array([-1, 0,+1, 0])
    jOff = np.array([ 0,-1, 0,+1])
    
    iAdj,jAdj = SafeAdjacent2(shape,i,j,iOff,jOff)
    print("\nSafe Adjacent Test")
    print("I,J")
    print(i)
    print(j)
    print("I,J Offsets")
    print(iOff)
    print(jOff)
    print("I,J Safe Adjacents")
    print(iAdj)
    print(jAdj)
    iAdj_c= np.array([1,3,2,0])
    jAdj_c= np.array([0,1,4,3])
    
    if np.sum(np.abs(iAdj-iAdj_c)+np.abs(jAdj-jAdj_c)) == 0:
        print("Safe Adj Success__________________(Pass)")
    else:
        print("Safe Adj Failure__________________(Fail)")
        
    a = np.zeros((3))
    inds = np.array([0, 1, 1, 1])    
    a[inds] = [5, 6, 7, 8]
    print("\nNumpy Assign Collision Test")
    print(a)
    if a[1]==8:
        print("Last Assign Wins__________________(Pass)")
    else:
        print("Last Assign Wins__________________(Fail)")
    
TestUtils()