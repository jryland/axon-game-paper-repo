# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 13:53:49 2018

@author: jwr071000
"""
import numpy as np
import scipy as sp
from scipy import ndimage as nd
from PIL import Image

def imBlurSharp(cicIm, sigma):
    cicIm[:,:,0] = nd.gaussian_filter(cicIm[:,:,0], sigma=(sigma), order=0)
    cicIm[:,:,1] = nd.gaussian_filter(cicIm[:,:,1], sigma=(sigma), order=0)
    cicIm[:,:,2] = nd.gaussian_filter(cicIm[:,:,2], sigma=(sigma), order=0)

    edge = .05
    sigma2 = sigma*2
    cicIm[:,:,0] = cicIm[:,:,0] - edge*nd.gaussian_laplace(cicIm[:,:,0], sigma=(sigma2))
    cicIm[:,:,1] = cicIm[:,:,1] - edge*nd.gaussian_laplace(cicIm[:,:,1], sigma=(sigma2))
    cicIm[:,:,2] = cicIm[:,:,2] - edge*nd.gaussian_laplace(cicIm[:,:,2], sigma=(sigma2))

    cicIm = (cicIm+np.min(cicIm))/(np.max(cicIm)-np.min(cicIm))
    cicIm = contrastRaise(cicIm, 2)
    
    return cicIm

def imAA(cicIm, sigma):
    cicIm[:,:,0] = nd.uniform_filter(cicIm[:,:,0], size=2)
    cicIm[:,:,1] = nd.uniform_filter(cicIm[:,:,1], size=2)
    cicIm[:,:,2] = nd.uniform_filter(cicIm[:,:,2], size=2)
    cicIm = (cicIm+np.min(cicIm))/(np.max(cicIm)-np.min(cicIm))
    
    return cicIm
    
def contrastRaise(im, r):
    
    im = im-.5
    im = np.clip(im*r+.5, 0, 1)
    return im

def levelSets(im, levNum):    
    im = np.ceil(im*levNum)/levNum
    return im