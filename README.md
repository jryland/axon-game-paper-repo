# Axon Game Paper Repo

This is the repo for the simulation software developed for the paper "Modeling Axonal Plasticity in Artificial Neural Networks". it contains python/tensorflow code that can simulate the Axon Game and H-LISSOM in conjuntion for developing cortical maps typically seen in V1. This package makes extensive use of numpy's sparse array methods and low-level tensorflow definitions, as such it may be hard to follow.

# Requirements
* Python 3.6* I believe (originally thought 3.7)
* Tensorflow 1.10.0 GPU-Support enabled (~6gigs VRAM)
* numpy 1.14.5
* scipi 0.19.1
* PIL 1.1.7 
* pickle 4.0

# Instructions
* Download/Clone the files to a directory of your choosing
* Compile and Run AG_MultiScale.py
-- this will replicate the axonal dev simulations
* Compile and Run AG_mapOR.py
-- this will replicate the axonal/synaptic dev simulations
