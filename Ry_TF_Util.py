# -*- coding: utf-8 -*-
"""
Created on Wed Sep 12 18:22:14 2018

@author: James Ryland

This is a set of basic tensor flow utilities needed for the ICL model and
for general efficiency purposes.

"""
import tensorflow as tf
import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy import ndimage as nd


"""
Book Keeeping   _______________________________________________________________
_______________________________________________________________________________
"""
# this class allows you to index into a list using the library
# interface so you can easily get and pull from lists generated using
# the sess.run command, because order will be maintained.. Do not put repeats

class OpLibList:
    
    def __init__(self, autoUnpack=True):
        # the list of opps
        self.list = []
        # the library
        self.lib = {}
        self.autoUnpack = autoUnpack
    
    def append(self, name, op):
        i = len(self.list)
        self.list.append(op)
        self.lib.update({name:[i, i+1]})
        
    def extend(self, name, opList):
        i = len(self.list)
        self.list.extend(opList)
        i2 = len(self.list)
        self.lib.update({name:[i, i2]})
        
    def get(self, key):
        inds = self.lib[key]
        ls = self.list[inds[0]:inds[1]]
        if self.autoUnpack and len(ls)==1:
            return ls[0]
        else:
            return ls

    def getFrom(self, valList, key):
        inds = self.lib[key]
        ls = valList[inds[0]:inds[1]]
        if self.autoUnpack and len(ls)==1:
            return ls[0]
        else:
            return ls

    def asList(self):
        return self.list

"""
Some very very helpful math functions__________________________________________
_______________________________________________________________________________=
"""
# Useful for defining evenly spaced by ratio frequency bands
def ConstRatioSeq(start, stop, num):
    
    x = np.log([start, stop])
    seq = np.exp( np.linspace(x[0], x[1], num) )
    ratio = seq[0]/seq[1]
    
    return seq, ratio 
    
def Test_ConstRatioSeq():
    
    seq = ConstRatioSeq(5, .75, 5)
    print( seq)
    print( seq[0:3]/seq[1:4])

#Test_ConstRatioSeq()

def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)

        
""" 
GENERATING STIMULI_____________________________________________________________
_______________________________________________________________________________
"""
def GaussianGenerator(shape, gauNum, aspect = [4,8], scaleRange = [.5,8], foveateCenters=False):
    """ This function creates a tensor flow op that returns an image with 
    randomly placed/shaped gaussians, this sould in theory be much faster than 
    generating spontanious stimuli on the CPU side"""

    
    xticks = np.linspace(-shape[0]/2,shape[0]/2, shape[0])
    yticks = np.linspace(-shape[1]/2,shape[1]/2, shape[1])
    xv, yv = np.meshgrid(xticks, yticks, sparse=False, indexing='ij')
    z = np.concatenate( (xv[:,:,np.newaxis], yv[:,:,np.newaxis]),  axis=2)
    im = np.zeros(shape)
    C = np.diag(aspect/np.linalg.norm(aspect))
    C = C.astype(np.float32)
    
    
    for i in range(0,gauNum):
        
        
        # Create random 2D rotation matrix
        ortho = np.array([[0,1],[-1,0]]).astype(np.float32)
        vDir = tf.nn.l2_normalize( tf.random_normal([1,2] ), axis=1 )
        vOrtho = tf.matmul(vDir, ortho)
        R = tf.concat((vDir, vOrtho), axis=0 )
        
        # Pick scale according to range
        sc = []
        cent = []
        if not foveateCenters:
            a0 = np.log(np.sqrt(scaleRange[0]))
            a1 = np.log(np.sqrt(scaleRange[1]))
            sc = np.square(tf.exp(tf.random_uniform([], a0, a1)))
            cent = tf.random_uniform([1,1,2], np.min(xticks)*1.33, np.max(xticks)*1.33)
        else:
            scales, ratio = ConstRatioSeq(scaleRange[0],scaleRange[1], gauNum)
            sc = scales[i]
            mod = sc/scaleRange[1]
            cent = tf.random_uniform([1,1,2], np.min(xticks)*1.33*mod, np.max(xticks)*1.33*mod)
            
        
        # Rotate covariance matrix
        C_rot = tf.matmul(C/sc, R)
        C2 = tf.matmul(tf.transpose(C_rot),C_rot)

        # Compute gaussain value for all pixels
        z_cent = z-cent
        # Tensor op Z_xyi * C_ij * Z_xyj = G_xy 
        B = tf.reduce_sum(z_cent[:,:,:,tf.newaxis]*C2[tf.newaxis,tf.newaxis,:,:], axis=2 )
        # Tensor op B_xyj * Z_xyj = G_xy
        D = tf.reduce_sum(B*z_cent, axis = 2)
        G = tf.exp(-tf.abs(D))
        
        # Normalize and give sign
        G = G*np.power( sc/scaleRange[1], 2 )
        G = G * tf.sign( tf.random_uniform([], -5, 5) )
        im = im+G
    
    return im  
        
        
        
"""
IMAGE FUNCTIONS _______________________________________________________________
_______________________________________________________________________________
    Assume BxWxHxC so these are easier to chain internally, possibly add some
WHCB compatibility functions for ease of conversion between formats.

"""

    
def BWHC_to_WHCB(dat):
    return tf.transpose(dat, [1,2,3,0])
def WHCB_to_BWHC(dat):
    return tf.transpose(dat, [3,0,1,2])
    
# Standard fast 2-step gaussian blur
def Gaussian_Blur(dat, std, padding='SAME'):
    
    print("GAUSSIAN BLUR USED, FUNCTION CHANGE RECENTLY, CHECK FORMULA ")
    
    # Create the filter
    num = int(2*np.ceil(std*3)+1)
    xD = np.ceil(std*3)
    x = np.linspace( -xD , xD, num)
    g = np.exp(-x**2/(2*std**2))
    g = g/np.sum(g)
    
    # Seperate channels
    numChan = dat.shape[3]
    gChan = np.zeros((g.shape[0], numChan, numChan))
    for i in range(0, numChan):
        gChan[:,i,i] = g
    
    # Kernel needs to be done twice along x and y
    kernelX = gChan[:,np.newaxis,:,:]
    kernelY = gChan[np.newaxis,:,:,:]
    
    # Perform the filters
    datGx = tf.nn.conv2d(dat  , kernelX, strides=[1, 1, 1, 1], padding=padding)
    datG  = tf.nn.conv2d(datGx, kernelY, strides=[1, 1, 1, 1], padding=padding)
    
    # Find the proportion of the filter over the image
    one   = tf.ones_like(dat)
    oneGx = tf.nn.conv2d(one  , kernelX, strides=[1, 1, 1, 1], padding=padding)
    oneG  = tf.nn.conv2d(oneGx, kernelY, strides=[1, 1, 1, 1], padding=padding)
    
    # Normalize by proportion of filter over image to avoid edge artifacts
    datG = datG/oneG
    
    return datG
    
def WHCB_Gaussian_Blur(dat,std,padding="SAME"):
    return BWHC_to_WHCB(Gaussian_Blur(WHCB_to_BWHC(dat),std,padding))

def test_Gaussian_Blur():
    import matplotlib.pyplot as plt
    
    img = np.random.uniform(size=(32,32,3) )
    
    dat = np.reshape(img, [1,32,32,3])
    datT = np.reshape(img, [32,32,3,1])
    
    tf.reset_default_graph()
    
    datG = Gaussian_Blur(dat, 2)
    datGT = WHCB_Gaussian_Blur(datT, 2)
    
    with tf.Session() as sess:
        
        datG_val  = sess.run(datG)
        datGT_val = sess.run(datGT)
        
        
        fig = plt.figure()
        ax1 = fig.add_subplot(131)
        ax1.imshow(img)
        ax2 = fig.add_subplot(132)
        ax2.imshow(np.squeeze(datG_val) )
        
        ax3 = fig.add_subplot(133)
        ax3.imshow(np.squeeze(datGT_val) )
        fig.canvas.draw()

#test_Gaussian_Blur()


# Take an input vector and perform lateral excitation/inhibition using
# 2 step gaussain filtering
def GauCenterSurround(s, imShape, b_ln, cent, surr, order='F', eBoost=1, iBoost=1):
    
    # Put in WHCB order
    dat = []
    if order=='F':
        dat = tf.reshape(s, [imShape[1], imShape[0], b_ln])
    else:
        dat = tf.reshape(s, [imShape[0], imShape[1], b_ln])
    
    # Add channel index
    dat = dat[:,:,tf.newaxis,:]
    
    # Perform 2-step blurs
    datCent = WHCB_Gaussian_Blur(dat,cent)*eBoost
    datSurr = WHCB_Gaussian_Blur(dat,surr)*iBoost
    
    datCentSurr = datCent-datSurr
    
    # Put back in SB order in theory should be put together correctly
    s_centSur = tf.reshape(datCentSurr, [imShape[1]*imShape[0], b_ln])
    
    return s_centSur
    

def GauSurround(s, imShape, b_ln, std, order='F'):
    
    # Put in WHCB order
    dat = []
    if order=='F':
        dat = tf.reshape(s, [imShape[1], imShape[0], b_ln])
    else:
        dat = tf.reshape(s, [imShape[0], imShape[1], b_ln])
    
    # Add channel index
    dat = dat[:,:,tf.newaxis,:]
    
    # Perform 2-step blurs
    datGau = WHCB_Gaussian_Blur(dat,std)

    # Put back in SB order in theory should be put together correctly
    s_gau = tf.reshape(datGau, [imShape[1]*imShape[0], b_ln])
    
    return s_gau
    
def Test_GauCenterSurround():
    
    tf.reset_default_graph()
    
    # Random image
    img_r = np.random.uniform(size=(200,100) )
    
    # Cross Image
    img = np.zeros( (200,100) )
    img[90:100] = 1 
    img[:,45:55] = 1    
    img = img +img_r*.5

    s = np.reshape(img, [200*100], order='F')
    
    s = tf.reshape(s, [200*100, 1])
    
    std_cent = 1.5
    std_surr = 3

    
    # Make a pyramid cropped to the image boundaries
    s_centSurr = GauCenterSurround(s, [200,100], 1, std_cent, std_surr, order='F')

    with tf.Session() as sess:
        
        s_centSurr_val = sess.run(s_centSurr)
    
    
    im_cs = np.reshape(s_centSurr_val, [200,100], order='F')
    
    fig=plt.figure()
    ax1 = fig.add_subplot(1,2,1)
    ax1.imshow(img)
    ax2 = fig.add_subplot(1,2,2)
    ax2.imshow(im_cs)
    
#Test_GauCenterSurround()    
        

# Some quicky image normalizations
def BoxNorm(vals):
    return (vals-np.min(vals))/(np.max(vals)-np.min(vals))
def OpposedNorm(vals):
    return vals/np.max(np.abs(vals))/2+.5
    
def PyrVec_to_Imgs(pyrVec_val, pyrLib, boxNorm=False, opNorm=False):
    
    pyrSizes = pyrLib["pyrSizes"]
    pyrInds = pyrLib["pyrInds"]
    pyrChan = pyrLib["pyrChan"]
    images = []    
    
    if boxNorm:
        pyrVec_val = BoxNorm(pyrVec_val)
    if opNorm:
        pyrVec_val = OpposedNorm(pyrVec_val)

    for i in range(0,len(pyrSizes)):
        
        # Get i-th image scale/frequency        
        imvec = pyrVec_val[  pyrInds[i]:pyrInds[i+1],  0]
        
        size = pyrSizes[i] 

        # reshape image vector (remember c-order)
        im = np.reshape(imvec, [size[0],size[1],pyrChan], order='C')
        
        if (boxNorm or opNorm) and pyrChan==1:
            im = np.repeat(im, 3, axis=2)
        
        # Add to image list
        images.append(im)
        
    return images



    
class PyrFigure:
    def __init__(self, pyrLib):
        self.pyrLib = pyrLib
        self.numF = pyrLib["numF"]
        self.fig = plt.figure()
        self.axes = []
        for i in range(0,self.numF):
            self.axes.append(self.fig.add_subplot(1,self.numF,i+1))
    def update(self,pyrVec_val, boxNorm=False, opNorm=False):
        images = PyrVec_to_Imgs(pyrVec_val, self.pyrLib, boxNorm=boxNorm, opNorm=opNorm)
        for i in range(0,self.numF):
            self.axes[i].imshow(np.squeeze(images[i]))
            self.axes[i].axis("off")
        self.fig.canvas.draw()

class ImageFigure:
    def __init__(self):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(1,1,1)
    def update(self, im, grey2rgb=False, boxNorm=False, opNorm=False):
        if boxNorm:
            im = BoxNorm(im)
        if opNorm:
            im = OpposedNorm(im)
        if grey2rgb:
            if len(im.shape)==2:
                im = im[:,:,np.newaxis]
            im = np.repeat(im, 3, axis=2)
        self.ax.imshow(im)
        self.ax.axis("off")
        self.fig.canvas.draw()

def SigSplit(valIm, mult):
    #x = BoxNorm(valIm)-.5
    return 1.0/(1.0+np.exp(-valIm*mult))
        
class ContourFigure:
    
    def __init__(self):
    
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(1,1,1)
        
    def refresh(self, valIms, levs, cmaps, linewidth=2, f=[], bg=[], colors=[]):
        
        self.ax.cla()
        
        if len(bg)>0:
            a = bg.shape[1]/bg.shape[0]
            self.ax.imshow(bg, interpolation='bilinear', origin='lower',
                           extent=(0, 1, 0, a))
        
        for i in range(0,len(valIms)):
            
            numLev = levs[i]
            valImg = valIms[i]

            a = valImg.shape[1]/valImg.shape[0]

            x = np.linspace(0, 1, valImg.shape[0])
            y = np.linspace(0, a, valImg.shape[1])
            X, Y = np.meshgrid(x, y)
        
            if len(colors) == 0:
                if len(f)==0:
                    self.ax.contour(X,Y,valImg,numLev, cmap=cmaps[i], linewidths=linewidth )
                elif f[i]:
                    self.ax.contourf(X,Y,valImg,numLev, cmap=cmaps[i], linewidths=linewidth )
                else:
                    self.ax.contour(X,Y,valImg,numLev, cmap=cmaps[i], linewidths=linewidth )
            else:
                if len(f)==0:
                    self.ax.contour(X,Y,valImg,numLev, colors=colors[i], linewidths=linewidth )
                elif f[i]:
                    self.ax.contourf(X,Y,valImg,numLev,  colors=colors[i], linewidths=linewidth )
                else:
                    self.ax.contour(X,Y,valImg,numLev,  colors=colors[i], linewidths=linewidth )    
            
            self.ax.axis("off")
            
        self.fig.canvas.draw()
            
            
def test_cont():
    
    
    img1 = nd.gaussian_filter( np.random.normal(size=[100,100]), sigma=2)
    img2 = nd.gaussian_filter( np.random.normal(size=[100,100]), sigma=2)
    
    cFig = ContourFigure()
    cFig.refresh( [img1, img2], [3, 5], ['gray', 'jet'] )
            
#test_cont()        
        
# This image pyramid is defined by an exterior size and an interior size.
#
#    Exterior
#    \     /
#     \   /
#      Int
#
# The interior size will be the constant resolution of all of the spatial
# Frequency slices. 
#
# dat    : the input is expected to be of the exterior resolution
# Ext       : integer exterior size
# Int       : integer interior size, 
# std_pix   : the size of the gaussain blur in pixels. Importantly, each scale im
# numF      : this is the number of frequency bands to cut the pyramid into
# chemo     : decide what kind of chemo-affinity map to use, euclidian, split-pol-scale, split-pol-rad, split-pol-lograd
#
def Dog_Pyramid_EndPoints(dat, Ext, Int, std_pix, numF, chemo='euclidian', tuning=1.0):
    
    
    # get the shape of the data
    # requires shape to be static
    shape = np.array(dat.get_shape().as_list())
    
    
    # Find sizes that are a constant ratio from Ext to Int
    Levs, ratio = ConstRatioSeq(Ext, Int, numF+1)
    
    print(Levs)
    
    Levs = np.concatenate(( Levs[:,np.newaxis], Levs[:,np.newaxis]), axis=1)
    
    
    # crop_and_resize
    scales = Levs/Ext
    
    constShape = np.array([Int,Int]).astype(int)
    
    
    pyrVec      = tf.zeros( (0,shape[0]))
    pyrOpList   = []
    pyrSizes    = []
    pyrChan     = shape[3]
    pyrInds     = [0]
    pyrChemoLS  = []   
    scaleVecLS  = [] 


    x_ls    = []
    y_ls    = []
    rad_ls  = []
    scl_ls  = []
    phi_ls  = []
    

    ln = 0
    
    
    for i in range(1, Levs.shape[0]):
        
        
        # Doing large convolutions on full image is very very expensive....
        std_low  =  Levs[i,0]/Int*std_pix*ratio
        std_high =  Levs[i,0]/Int*std_pix
        std_low = tuning*(std_low-std_high)+std_high

        
        gLow  = Gaussian_Blur(dat, std_low)
        gHigh = Gaussian_Blur(dat, std_high)
        
        # Create the DOG bandpass output
        dog = gHigh-gLow            
        
        # Scale image to constant shape
        sc      = scales[i,:]/2
        boxes   = np.array([[.5-sc[0], .5-sc[0], .5+sc[1], .5+sc[1]]])*np.ones((shape[0],1))
        boxInd  = np.arange(0,shape[0])
        
        dogSc = tf.image.crop_and_resize(dog, boxes, boxInd, constShape)
        
        # Adjust response for scale
        # i.e. High freqeucney -> higher energy -> even at lower amplitudes
        dogSc = dogSc / Levs[i,0]/Int
        
        #Remember TF uses C order   # B             #WHC
        dogVec = tf.reshape(dogSc, [shape[0], constShape[0]*constShape[1]*pyrChan])
        dogVec = tf.transpose(dogVec)
        
        chemoVec = []
        
        #Setup Chemo affinity representations and position signals
        X = np.ravel( np.linspace(-sc[0],sc[0],constShape[0])[:,np.newaxis,np.newaxis]*np.ones((1,constShape[1],pyrChan)), 'C')[:,np.newaxis]
        Y = np.ravel( np.linspace(-sc[1],sc[1],constShape[1])[np.newaxis,:,np.newaxis]*np.ones((constShape[0],1,pyrChan)), 'C')[:,np.newaxis]
        gradX = X+.5
        gradY = Y+.5
        RAD, PHI = cart2pol(X,Y)
        logRAD = np.log(RAD*np.e+1.0)
        SCL_base = (1-i/Levs.shape[0])*np.ones_like(Y)
        SCL = SCL_base/2*np.sign(Y)+.5
        gradPHI = np.abs(PHI/np.pi)            
        gradSCL = SCL*np.ones_like(gradPHI)
        gradRAD = logRAD*np.sign(Y)/2+.5
        bound = np.ravel( np.ones_like(gradSCL), 'C')[:,np.newaxis]
        if chemo=='euclidian' or chemo==[]:
            chemoVec = np.concatenate( (gradX,gradY,bound) , axis=1)
        elif chemo=='split-pol-scale' or chemo=='split-pol':
            chemoVec = np.concatenate( (gradPHI,gradSCL,bound) , axis=1)
        elif chemo=='split-pol-rad' or chemo=='split-pol-log':
            chemoVec = np.concatenate( (gradPHI,gradRAD,bound) , axis=1)
        else:
            print("PYRAMID: INVALID CHEMO AFFINITY SETTINGS")

        dog_ln = constShape[0]*constShape[1]*pyrChan
        ln = ln + dog_ln
        
        # Store scale info and output
        pyrVec = tf.concat( (pyrVec, dogVec), axis=0 )
        pyrOpList.append(dog)
        pyrSizes.append(constShape)
        pyrInds.append(ln)
        pyrChemoLS.append( chemoVec)
        scaleVecLS.append( i*np.ones( (dog_ln, 1) ))
        
        x_ls.append( np.ravel(X, order='C') )
        y_ls.append( np.ravel(Y, order='C') )
        rad_ls.append( np.ravel(logRAD, order='C') )
        scl_ls.append( np.ravel(SCL_base, order='C') )
        phi_ls.append( np.ravel(gradPHI, order='C') )
        
       
    # Concatenate lists of arrays into arrays
    pyrChemo = np.concatenate( pyrChemoLS, axis = 0 )
    scaleVec = np.concatenate( scaleVecLS, axis=0)
    # Store meta information about variation axes
    xVec = np.concatenate(x_ls,axis=0)
    yVec = np.concatenate(y_ls,axis=0)
    radVec = np.concatenate(rad_ls,axis=0)
    sclVec = np.concatenate(scl_ls,axis=0)
    phiVec = np.concatenate(phi_ls,axis=0)
        
    # Setup a library of values and ops for reconstructing the pyramid
    # and or visualizing its output evaluations
    pyrLib = {"std_pix":std_pix,"numF":numF, "scales":scales, "ratio":ratio,
              "std_low":std_low, "std_high":std_high, "scaleVec":scaleVec,
              "pyrOpList":pyrOpList, "pyrSizes":pyrSizes, "pyrChan":pyrChan,
              "xVec":xVec, 'yVec':yVec, 'radVec':radVec, 'sclVec':sclVec, 'phiVec':phiVec, 
              "pyrInds":pyrInds, "pyrChemo":pyrChemo, "pyrVecLength":pyrInds[-1]}
    
    return pyrVec, pyrLib

def WHCB_Dog_Pyramid_EndPoints(dat, Ext, Int, std_pix, numF, chemo='euclidian', tuning=1.0):
    return Dog_Pyramid_EndPoints(WHCB_to_BWHC(dat), Ext, Int, std_pix, numF, chemo=chemo, tuning=tuning)
    
    
def Test_Dog_Pyramids_EndPoints():
    
    tf.reset_default_graph()
    
    # Random image
    img_r = np.random.uniform(size=(200,200,3) )
    
    # Cross Image
    img = np.zeros( (200,200,3) )
    img[90:100,:,0] = 1 
    img[:,90:100,1] = 1    
    img = img +img_r*.5

    dat = tf.reshape(img, [1,200,200,3])
    
    std_pix = 1.5
    numF = 6
    Ext = 200
    Int = 25
    
    # Make a pyramid cropped using endpoint sizes
    pyrVec1, pyrLib1 = Dog_Pyramid_EndPoints(dat, Ext, Int, std_pix, numF, 'euclidian')

    with tf.Session() as sess:
        
        pyrVec1_val = sess.run(pyrVec1)
        
    pyrFig1 = PyrFigure(pyrLib1)
    pyrFig1.update(OpposedNorm(pyrVec1_val))
    
    #print(pyrLib1["scales"])
    #print(pyrLib1["ratio"])
    #print([pyrLib1["std_low"],  pyrLib1["std_high"]])
    
    pyrFig1 = PyrFigure(pyrLib1)
    pyrFig1.update(OpposedNorm(pyrVec1_val))
    
    # Display chemo maps
    pyrFigX1 = PyrFigure(pyrLib1)
    pyrFigX1.update(pyrLib1["pyrChemo"][:,0,np.newaxis])
    pyrFigY1 = PyrFigure(pyrLib1)
    pyrFigY1.update(pyrLib1["pyrChemo"][:,1,np.newaxis])
    

#Test_Dog_Pyramids_EndPoints()



def Test_Pyramid_EndPoint_Independence():
    
    tf.reset_default_graph()
    
    Ext      = 800
    Int      = 25
    extShape = [Ext, Ext]
    intShape = [Int, Int]
    numF     = 5
    std      = 2.0
    tuning   = 1
    
    # Generate random gaussians accross scale
    dat = GaussianGenerator(extShape, 32, aspect=[4, 8], scaleRange=[.5, 64], foveateCenters=True)
    
    # Add channel and batch dimensions
    dat = tf.reshape(dat, [Ext,Ext, 1,1])
    
    # Create scale pyramid
    pyrVec, pyrLib = WHCB_Dog_Pyramid_EndPoints(dat, Ext, Int,  std, numF, chemo='split-pol', tuning=tuning)
    
    
    # Show pyramid representation
    imFig = ImageFigure()
    pyrFig = PyrFigure(pyrLib)
    
    with tf.Session() as sess:
        for i in range(0, 200):
            pyrVec_val, image_val = sess.run([pyrVec, dat])
            image_val = np.squeeze(image_val)
            imFig.update(image_val, grey2rgb=True, opNorm=True)
            pyrFig.update(pyrVec_val,opNorm=True)
            plt.pause(1)
            
#Test_Pyramid_EndPoint_Independence()
        
#Test_Dog_Pyramids()


# This image pyramid is defined by an interior size and a number levels
# doubled in size. This allows for efficient stepwise-gaussian bluring as is
# done in standard LOG pyramids.
#
#   | Int*2**order|-> must be the size of dat
#   | \         / |
#   |  \       /  |
#   |   \     /   |
#   |     Int     |
#
# The interior size will be the constant resolution of all of the spatial
# Frequency slices. 
#
# dat       : the input is expected to be of the exterior resolution
# Int       : integer interior size,
# order     : number of powers of 2 to reach full image size
# std_pix   : the size of the gaussain blur in pixels. Suggested to be sigma>1
# chemo     : decide what kind of chemo-affinity map to use, euclidian, split-pol-scale, split-pol-rad, split-pol-lograd
def Dog_Pyramid_Standard(dat, Int, order, std_pix,  chemo='euclidian'):
    # get the shape of the data
    # requires shape to be static
    shape = np.array(dat.get_shape().as_list())
    constShape = np.array([Int,Int]).astype(int)
    
    pyrVec      = tf.zeros( (0,shape[0]))
    pyrOpList   = []
    pyrSizes    = []
    pyrChan     = shape[3]
    pyrInds     = [0]
    pyrChemoLS  = []   
    scaleVecLS  = [] 

    x_ls    = []
    y_ls    = []
    rad_ls  = []
    log_ls  = []
    scl_ls  = []
    phi_ls  = []
    scr_ls  = []
    
    ln = 0
    
    gauLevels = []
    dogLevels = []
    
    std_low  =  std_pix*2
    std_high =  std_pix
    
    Ext = Int*2**order
    print(Ext)
    
    # Init variable to be Updated at each level
    size = np.array([Ext, Ext])
    cDat = dat
    
    # Size in pixels in the original image that each scale
    # Level covers
    pixWidths = Int*2**np.flip(np.arange(0, order+1),axis=0)
    print(pixWidths)
    
    # feature scaling
    fScaling = (Int*2**np.arange(0, order+1))/Ext
    
    
    
    # Compute basic LOG pyramid
    for i in range(0, order+1):
        
        # Compute the LoG at this level
        gHigh  = Gaussian_Blur(cDat, std_high)
        gLow   = Gaussian_Blur(cDat, std_low)
        
        dog = gHigh-gLow
        
        # Down scale the image
        cDat = tf.image.resize_images(dat, size)
        
        # Add to lists
        gauLevels.append(cDat)
        dogLevels.append(dog)
        
          
    # Foveate representation
    for i in range(0, order+1):
        
        # Scale image to constant shape
        # Erro on cropping a little inward as tf, crop and resize does wierd
        # things when cropping to original image size...
        sc      = .95*np.array([pixWidths[i], pixWidths[i]])/Ext/2
        boxes   = np.array([[.5-sc[0], .5-sc[0], .5+sc[1], .5+sc[1]]])*np.ones((shape[0],1))
        boxInd  = np.arange(0,shape[0])
        
        dogSc = tf.image.crop_and_resize(dog, boxes, boxInd, constShape)
        
        # Adjust response for scale
        # i.e. High freqeucney -> higher energy -> even at lower amplitudes
        #dogSc = dogSc * fScaling[i]
        
        #Remember TF uses C order   # B             #WHC
        dogVec = tf.reshape(dogSc, [shape[0], constShape[0]*constShape[1]*pyrChan])
        dogVec = tf.transpose(dogVec)
        
        #Setup Chemo affinity representations and position signals
        chemoVec = []
        scX = np.ravel( np.linspace(-.5,.5,constShape[0])[:,np.newaxis,np.newaxis]*np.ones((1,constShape[1],pyrChan)), 'C')[:,np.newaxis]
        scY = np.ravel( np.linspace(-.5,.5,constShape[1])[np.newaxis,:,np.newaxis]*np.ones((constShape[0],1,pyrChan)), 'C')[:,np.newaxis]
        scRAD, scPhi = RAD, PHI = cart2pol(scX,scY)
                                    
        X = np.ravel( np.linspace(-sc[0],sc[0],constShape[0])[:,np.newaxis,np.newaxis]*np.ones((1,constShape[1],pyrChan)), 'C')[:,np.newaxis]
        Y = np.ravel( np.linspace(-sc[1],sc[1],constShape[1])[np.newaxis,:,np.newaxis]*np.ones((constShape[0],1,pyrChan)), 'C')[:,np.newaxis]
        gradX = X+.5
        gradY = Y+.5
        RAD, PHI = cart2pol(X,Y)
        
        #eta = 20
        #ex = .000001
        logRAD = np.log(RAD)
        
        SCL_base = (1-i/order)*np.ones_like(Y)
        SCL = SCL_base/2*np.sign(Y)+.5
        gradPHI = np.abs(PHI/np.pi)            
        gradSCL = SCL*np.ones_like(gradPHI)
        gradRAD = RAD*np.sign(Y)/2+.5
        bound = np.ravel( np.ones_like(gradSCL), 'C')[:,np.newaxis]
        if chemo=='euclidian' or chemo==[]:
            chemoVec = np.concatenate( (gradX,gradY,bound) , axis=1)
        elif chemo=='split-pol-scale' or chemo=='split-pol':
            chemoVec = np.concatenate( (gradPHI,gradSCL,bound) , axis=1)
        elif chemo=='split-pol-rad':
            chemoVec = np.concatenate( (gradPHI,gradRAD,bound) , axis=1)
        else:
            print("PYRAMID: INVALID CHEMO AFFINITY SETTINGS")

        dog_ln = constShape[0]*constShape[1]*pyrChan
        ln = ln + dog_ln
        
        # Store scale info and output
        pyrVec = tf.concat( (pyrVec, dogVec), axis=0 )
        pyrOpList.append(dog)
        pyrSizes.append(constShape)
        pyrInds.append(ln)
        pyrChemoLS.append( chemoVec)
        scaleVecLS.append( i*np.ones( (dog_ln, 1) ))
        
        x_ls.append( np.ravel(X, order='C') )
        y_ls.append( np.ravel(Y, order='C') )
        rad_ls.append( np.ravel(RAD, order='C') )
        log_ls.append( np.ravel(logRAD, order='C') )
        scl_ls.append( np.ravel(SCL_base, order='C') )
        phi_ls.append( np.ravel(gradPHI, order='C') )
        scr_ls.append( np.ravel(scRAD, order='C'))
        
       
    # Concatenate lists of arrays into arrays
    pyrChemo = np.concatenate( pyrChemoLS, axis = 0 )
    scaleVec = np.concatenate( scaleVecLS, axis=0)
    # Store meta information about variation axes
    xVec = np.concatenate(x_ls,axis=0)
    yVec = np.concatenate(y_ls,axis=0)
    radVec = np.concatenate(rad_ls,axis=0)
    logVec = np.concatenate(log_ls,axis=0)
    sclVec = np.concatenate(scl_ls,axis=0)
    phiVec = np.concatenate(phi_ls,axis=0)
    scrVec = np.concatenate(scr_ls,axis=0)
        
    # Setup a library of values and ops for reconstructing the pyramid
    # and or visualizing its output evaluations
    pyrLib = {"std_pix":std_pix,"numF":(order+1), "order":order, "ratio":2,
              "std_low":std_low, "std_high":std_high, "scaleVec":scaleVec,
              "pyrOpList":pyrOpList, "pyrSizes":pyrSizes, "pyrChan":pyrChan,
              "xVec":xVec, 'yVec':yVec, 'radVec':radVec, 'logVec':logVec, 'sclVec':sclVec,
              'phiVec':phiVec, "scrVec":scrVec,
              "pyrInds":pyrInds, "pyrChemo":pyrChemo, "pyrVecLength":pyrInds[-1]}
    
    return pyrVec, pyrLib

def WHCB_Dog_Pyramid_Standard(dat, Int, order, std_pix,  chemo='euclidian'):
    return Dog_Pyramid_Standard(WHCB_to_BWHC(dat), Int, order, std_pix, chemo=chemo)

def Test_Dog_Pyramid_Standard():
    
    tf.reset_default_graph()
    
    # Random image
    img_r = np.random.uniform(size=(200,200,3) )
    
    # Cross Image
    img = np.zeros( (200,200,3) )
    img[80:110,:,0] = 1 
    img[:,80:110,1] = 1    
    img = img +img_r*.2

    dat = tf.reshape(img, [1,200,200,3])
    
    std_pix = 1.5
    Int = 25
    order = 3
    
    # Make a pyramid cropped using endpoint sizes
    pyrVec1, pyrLib1 = Dog_Pyramid_Standard(dat, Int, order, std_pix, 'euclidian')

    with tf.Session() as sess:
        
        pyrVec1_val = sess.run(pyrVec1)
        
    pyrFig1 = PyrFigure(pyrLib1)
    pyrFig1.update(OpposedNorm(pyrVec1_val))
    
    #print(pyrLib1["scales"])
    #print(pyrLib1["ratio"])
    #print([pyrLib1["std_low"],  pyrLib1["std_high"]])
    
    pyrFig1 = PyrFigure(pyrLib1)
    pyrFig1.update(OpposedNorm(pyrVec1_val))
    
    # Display chemo maps
    pyrFigX1 = PyrFigure(pyrLib1)
    pyrFigX1.update(pyrLib1["pyrChemo"][:,0,np.newaxis])
    pyrFigY1 = PyrFigure(pyrLib1)
    pyrFigY1.update(pyrLib1["pyrChemo"][:,1,np.newaxis])
    

#Test_Dog_Pyramid_Standard()


# This functio is for spot checking if the middle spatial frequencies inputs
# are statistically invariant accross scale
def Test_Pyramid_Standard_Independence():
    
    tf.reset_default_graph()
    
    Int      = 25
    order    = 5
    Ext      = Int*2**order
    extShape = [Ext, Ext]
    intShape = [Int, Int]
    std      = 2.0
    
    # Generate random gaussians accross scale
    dat = GaussianGenerator(extShape, 32, aspect=[4, 8], scaleRange=[.5, 64], foveateCenters=True)
    
    # Add channel and batch dimensions
    dat = tf.reshape(dat, [Ext,Ext, 1,1])
    
    # Create scale pyramid
    pyrVec, pyrLib = WHCB_Dog_Pyramid_Standard(dat, Int, order, std, chemo='split-pol')
    
    
    # Show pyramid representation
    imFig = ImageFigure()
    pyrFig = PyrFigure(pyrLib)
    
    with tf.Session() as sess:
        for i in range(0, 200):
            pyrVec_val, image_val = sess.run([pyrVec, dat])
            image_val = np.squeeze(image_val)
            imFig.update(image_val, grey2rgb=True, opNorm=True)
            pyrFig.update(pyrVec_val,opNorm=True)
            plt.pause(1)

#Test_Pyramid_Standard_Independence()
    

# This class creates a figure that displays crossectional views of values and
# Masks accross specified input variation axes. Good for viewing window shape
# in a scale space representation and potentially feature values too.
class CrossSectionViews:
    
    def __init__(self, vecLength, xAxes, yAxes, xNames, yNames, alphaBase = .01, shape=[2,2], allP=True ):
        
        # Store values
        self.xAxes = xAxes
        self.yAxes = yAxes
        self.xNames = xNames
        self.yNames = yNames
        self.alphaBase = alphaBase
        self.allP=allP
        self.vecLength = vecLength
        
        # Display constants
        self.windowColor = np.array([.9, .1, .1])
        self.windowAlpha = np.array([.9])
        
        # Setup unit colors
        self.unitColor = np.eye(3) 
        
        # Setup internal data structures to be manipulated
        self.alpha = np.zeros((vecLength,1))
        self.value = np.zeros((vecLength,1))
        self.color = np.zeros((vecLength,3))
        self.mask  = np.zeros((vecLength)).astype(bool)
        
        
        # Setup figure axes list
        self.fig = plt.figure()
        self.axs = []
        
        for i in range(0, len(self.xAxes) ):
            self.axs.append( self.fig.add_subplot(shape[0],shape[1],i+1) )
        
        self.reset()
        self.refresh()
    
    def reset(self):
        self.alpha = self.alpha*0+self.alphaBase
        self.color = self.color*0
        self.value = self.value*0
        self.mask[:] = False
    
    def refresh(self, vals=[], W_pack=[], Ind_pack=[], dispInds=[], cutoff=.1, cont=False):
        
        # Clear the current color alpha data
        self.reset()
        
        # Test what kind of rendering to do given input data
        if not Ind_pack==[]:
            self.paint_windows(Ind_pack, dispInds)
            
        if not vals==[]:
            self.paint_values(vals, cutoff, cont)
        
        # Render each plot
        for i in range(0, len(self.axs)):
            self.crossplot_rend( self.axs[i], 
                                self.xAxes[i], self.yAxes[i],
                                self.xNames[i], self.yNames[i])
            
        self.fig.canvas.draw()
            
    def paint_windows(self, Ind_pack, dispInds):
        
        # Get more colors if there are not enough
        if dispInds.shape[0]>self.unitColor.shape[0]:
            self.unitColor = np.random.uniform(size=[dispInds.shape[0], 3])
            self.unitColor[:,0] = .8
            self.unitColor = self.unitColor/np.max(self.unitColor)
        
        # Paint each window
        for i in range(0, dispInds.shape[0]):
            # Find the current column to pain
            indCol = Ind_pack[:,dispInds[i]]
            # Remove the pad values and index
            wInds = indCol[indCol>0]-1
            # Apply the window highlight color
            if dispInds.shape[0]==1:
                self.color[wInds,:] = self.windowColor
            # Apply unique random colors (consistant accross updates)
            else:
                self.color[wInds,:] = self.unitColor[i,:]
            self.alpha[wInds,:] = self.windowAlpha
            self.mask[wInds] = True
        
    def paint_values(self, vals, cutoff, cont):

        # Cuttoff variant        
        if not cont:
            sel = np.squeeze(vals>cutoff)
            self.color[sel,:] = self.windowColor
            self.alpha[sel,:] = self.windowAlpha
            self.mask[sel] = True
        else:
            valsScaled = (vals-np.min(vals))/(np.max(vals)-np.min(vals))
            valsScaled = np.reshape(valsScaled, [self.vecLength, 1])
            norm = plt.Normalize()
            self.color[:,:] = np.squeeze(plt.cm.jet(norm(valsScaled)))[:,0:3]
            self.alpha[:,:] = self.windowAlpha*valsScaled*.1


    def crossplot_rend(self, ax, x, y, xName, yName):
        
        rgba = []
        if self.allP:
            # Create the red,green,blue,alpha pattern
            rgba = np.concatenate((self.color,self.alpha),axis=1)
            # Render
        else:
            rgba = np.concatenate((self.color,self.alpha),axis=1)
            rgba = rgba[self.mask,:]
            x = x[self.mask]
            y = y[self.mask]
        ax.cla()    
        ax.scatter(x,y, c=rgba)
        ax.set_xlabel(xName)
        ax.set_ylabel(yName)
    
    
    
def test_CrossSectionViews():
    
    tf.reset_default_graph()
    
    # Random image
    img = np.random.uniform(size=(800,800,3) )    
    dat = tf.reshape(img, [1,800,800,3])
    
    
    # Make a pyramid cropped using endpoint sizes
    #std_pix = 1.5
    #numF = 15
    #Ext = 200
    #Int = 15
    #pyrVec, pyrLib = Dog_Pyramid_EndPoints(dat, Ext, Int, std_pix, numF, 'euclidian')
    
    
    # Make a foveated pyramid using standard method
    std_pix = 1.5
    Int = 25
    order = 5
    
    pyrVec, pyrLib = Dog_Pyramid_Standard(dat, Int, order, std_pix, 'euclidian')
    
    
    xVec   = pyrLib["xVec"]
    yVec   = pyrLib["yVec"]
    sclVec = pyrLib["sclVec"]
    radVec = pyrLib["radVec"]
    
    xText = "Vertical View Angle"    
    yText = "Horizontal View Angle"
    radText = "Radius From Center"
    sclText = "Spatial Frequency Band"

    xAxes =  [xVec,     xVec  ,     xVec]    
    yAxes =  [yVec,     sclVec,     radVec]
    xNames = [xText,    xText,      xText]
    yNames = [yText,    sclText,    radText]

    vecLength = pyrLib["pyrVecLength"]
    
    csvFig = CrossSectionViews(vecLength, xAxes, yAxes, xNames, yNames, alphaBase=.005, allP=True )
    csvFig2 = CrossSectionViews(vecLength, xAxes, yAxes, xNames, yNames, alphaBase=.005, allP=True )
    
    # Make a fake Ind_pack for a central window
    # Assumed 0 is pad index so add 1
    N = 40
    indOrder = np.arange(0,vecLength)+1
    Ind_pack = np.zeros( (vecLength, N) ).astype(int)
    for i in range(0, N):
        c = np.random.uniform(size=[3])-.5
        include = (((xVec-c[0])**2+(yVec-c[1])**2+((sclVec-.5)/2-c[2])**2)**.5) <.1
        
        colInds = indOrder[ include ]
        colLen = colInds.shape[0]
        Ind_pack[0:colLen,i] = colInds  
        dispInds = np.arange(N).astype(int)
    csvFig.refresh(Ind_pack=Ind_pack,dispInds=dispInds)
    
    csvFig2.refresh(vals=yVec, cutoff=0, cont=True)

    
#test_CrossSectionViews()
    
        


# Need a visualizer for windows on multi-scale input
class MultiScaleWindowFig:
    
    def __init__(self, fieldInds, pyrLib, name="BLA", bg=[]):
        self.fig = plt.figure()
        self.fig.suptitle(name, fontsize=16)
        self.axes = []
        self.fieldInds = fieldInds
        self.pyrLib = pyrLib
        self.vecLen = pyrLib["pyrVecLength"]
        self.numF = pyrLib["numF"]
        self.sizes = pyrLib["pyrSizes"]
        
        
        self.len = len(self.fieldInds) 
        
        self.figGridSz = [self.len, self.numF]
        c = 0
        for i in range(0,np.prod(self.figGridSz)):
            self.axes.append( self.fig.add_subplot(self.figGridSz[0],self.figGridSz[1],c+1) )
            c = c+1
            
        if bg=='black':
            self.fig.set_facecolor('black')
            
            
    def update(self, Ind_val, W_pack=[]):
        
        c = 0
        for i in range(0,self.len):
        
            fInd = self.fieldInds[i]
            ind_col = Ind_val[:,fInd]        
            
            windowVec = np.zeros((self.vecLen,1))

            # If inputs have been split like on-off, just take the on side
            binaryIndex = np.logical_and( 0<ind_col,  ind_col<self.vecLen)
            inputInds =  ind_col[binaryIndex]-1

            if W_pack==[]:
                windowVec[inputInds] = 1
            else:
                W_col = W_pack[:,fInd]     
                inputWeights = W_col[binaryIndex]
                windowVec[inputInds] = inputWeights
                windowVec = windowVec/np.max(windowVec)

            images = PyrVec_to_Imgs(windowVec, self.pyrLib)
            
            for j in range(0,self.numF):
                #print("axes Len:"+str(len(self.axes) ))
                #print(" i:"+str(i)+" j:"+str(j)+" c:"+str(c))
                self.axes[c].cla()
                self.axes[c].imshow(images[j])
                self.axes[c].axis("off")
                c = c+1
                
        self.fig.canvas.draw()

# Multi-Scale Map Visualizer
class MultiScaleMapFig:
    
    def __init__(self, mapSize, pyrLib, name="BLA", bg=[]):
        self.fig = plt.figure()
        self.fig.suptitle(name, fontsize=16)
        self.mapSize = mapSize
        self.pyrLib = pyrLib
        self.scaleInds = pyrLib["pyrInds"]
        self.numSc = len(self.scaleInds)-1
        self.axis1 = self.fig.add_subplot(1,2,1)
        self.axis2 = self.fig.add_subplot(1,2,2)
        
    def update(self, Ind_val):
        
        SC        = np.arange(0,self.numSc)[np.newaxis,:]
        NbySC     = np.zeros([Ind_val.shape[1], self.numSc]) 
        NbySC_nnz = np.zeros_like(NbySC) 
        
        # Correct for zero padding
        Ind_val = Ind_val-1
        
        # Find the number of inputs in each scale range for each neuronal window
        for i in range(0,self.numSc):
            
            ithRange = np.logical_and( self.scaleInds[i]<=Ind_val,  Ind_val<self.scaleInds[i+1])
            ithCount = np.sum(ithRange,axis=0)
            NbySC    [:,i] = ithCount
            NbySC_nnz[:,i] = ithCount>0 
            
        avg = np.sum(SC*NbySC,axis=1)/np.sum(NbySC, axis=1)
        scm = SC[0,np.argmax(NbySC, axis=1)]
        
        avgIm = np.reshape(avg, self.mapSize, order='F')[:,:,np.newaxis]*np.ones((1,1,3))
        scmIm = np.reshape(scm, self.mapSize, order='F')[:,:,np.newaxis]*np.ones((1,1,3))      
         
        self.axis1.cla()         
        self.axis1.imshow(avgIm/self.numSc)
        self.axis1.axis("off")
        self.axis2.cla()         
        self.axis2.imshow(scmIm/self.numSc)
        self.axis2.axis("off")
        self.fig.canvas.draw()
        
    
    
"""
BATCH FUNCTIONS _______________________________________________________________
_______________________________________________________________________________
"""


def WindowLinear_batch(W_pack, S_winds):  
    r = tf.reduce_sum( W_pack[:,:,tf.newaxis]*tf.squeeze(S_winds), axis=0)   
    return r

#using fixed batch size, for ease of writing...
def InPack_batch(S, Inds, b_ln ):
    #using fixed batch size, for ease of writing...
        
    # add expected zero element at index 0
    zero = np.zeros( (1,b_ln) ).astype(np.float32)
    S_pad = tf.concat((zero, S), axis=0 )
    
    # Gather s values into packed windows
    S_winds = tf.reshape( tf.gather(S_pad, Inds, axis=0), [Inds.shape[0], Inds.shape[1], b_ln] )
    
    return S_winds
    

def SurroundConnected_batch(s, imShape, b_ln, lType="Malsburg", radii = [2, 6], order='F', pad='SYMMETRIC'):
    # This defines a fixed filter for use in making lateral connectivity layers
    # Importantly need to figure our how tensor flow does reshaping, it seems
    # To favor the final index first??
    
    d0 = radii[0]*2+1
    d1 = np.ceil(radii[1]*2+1).astype(int)
    r0 = radii[0]+.5
    r1 = radii[1]+.5
    
    # create offset images for constructing filter
    Yoff, Xoff = np.meshgrid(np.linspace(-radii[1],radii[1],d1), np.linspace(-radii[1],radii[1],d1), indexing='ij')

    lFilt = []
    # Define Static Filter
    if lType=='Malsburg':
        dist = np.sqrt( np.power(Xoff,2)+np.power(Yoff,2) )
        E = parabolicSpot(dist, r0, 1)
        I = parabolicSpot(dist, r1, 1)
        E = E/np.sum(E)
        I = I/np.sum(I)
        lFilt = E-I
    elif lType=='OnOff':
        dist = np.sqrt( np.power(Xoff,2)+np.power(Yoff,2) )
        E = dist<r0
        I = dist<r1
        E = E/np.sum(E)
        I = I/np.sum(I)
        lFilt = E-I

    elif lType=='Gaussian':
        std = radii[0]
        gau = np.exp( - (np.power(Xoff,2)+np.power(Yoff,2)) /(2*std*std) )
        gau = gau/np.sum(gau)
        lFilt = gau
    
    elif lType=='DoG':
        std1 = radii[0]/3
        gau1 = np.exp( - (np.power(Xoff,2)+np.power(Yoff,2)) /(2*std1*std1) )
        gau1 = gau1/np.sum(gau1)
        std2 = radii[1]/3
        gau2 = np.exp( - (np.power(Xoff,2)+np.power(Yoff,2)) /(2*std2*std2) )
        gau2 = gau2/np.sum(gau2)
        dog = gau1-gau2
        lFilt = dog
    
        
    # Ensure that reshaping does not misalign image due to assumed index order
    # If images are square unnescessary
    if order=='F':
        a = [0, 0]
        a[0] = imShape[1]
        a[1] = imShape[0]
        imShape = a

    # Switchign batch to first index
    s = tf.transpose(s, [1, 0])
    
    s_im = tf.reshape(s,(b_ln, imShape[0], imShape[1]), name='s_img')
    
    # Create padding
    pS = np.round(radii[1]).astype(int)
    padSizes = tf.constant([[0,0], [pS,pS], [pS,pS]])
    s_im_mir = tf.pad(s_im, padSizes, pad)
    
    # Translate the image vector into the shape used by tf.nn.conv2d
    # batch width hight channel
    s_im_nwhc = tf.reshape(s_im_mir,(b_ln, imShape[0]+pS*2, imShape[1]+pS*2, 1), name='s_img_nwhc')
    
    # width hight inChan outChan
    lFilt = np.reshape(lFilt, (d1,d1,1,1), order='F')
    
    
    # Perform the convolution
    r_im_nwhc = tf.nn.conv2d(s_im_nwhc, lFilt, (1,1,1,1), "VALID")
    
    # Put back into vector form
    r = tf.reshape(r_im_nwhc,(b_ln, imShape[0]*imShape[1]), name='r')
    
    # Switch batch back to last index
    r = tf.transpose(r, [1,0])
    
    return r, lFilt    


"""
SINGLE INPUT FUNCTIONS ________________________________________________________
_______________________________________________________________________________
"""


def ColWiseVec(A):
    """In many place I assume -F or first index first vectorizing, whereas
    tensor flow prefers last index first vectorizations. This function swaps
    the index order and vectorizes using reshape."""
    
    dimNum = len( A.shape )
    indOrd = np.arange(dimNum)
    indOrdFlip = np.flip(indOrd, axis=0)
    
    A_trans = tf.transpose(A, indOrdFlip, "A_trans")
    
    Avec = tf.reshape(A_trans, (np.prod(A.shape), 1), "vec_A")
    
    return Avec


def MiniBatchIndexer( in_ln, buffSize=1000 ):
    # This holds a mini batch whose stimuli can be indexed
    # the batch can be set with a place_holder_with default
    # and the pointer index can also be set with a placeholder
    # with default, the placeholders will remember the previous values
    # eliminating the need to constantly re-upload the buffer
    # look into the dataset methodologies later
    #
    # Using an indexed batch like this can massively improve performance by avoiding
    # GPU-CPU memory bottlenecking when each stimulus has to be sent for every
    # Run
    
    # Setup Batch and Batch update ops
    S_buff = tf.get_variable("S_buff", shape=[in_ln, buffSize])
    S_feed = tf.placeholder_with_default(S_buff, shape=[in_ln, buffSize], name="S_feed")
    S_up   = tf.assign(S_buff, S_feed)[0,0]
    
    # Setup Index and Batch Index Ops
    # Convenient if you switch to another input source
    i      = tf.get_variable("index", shape=[], dtype=tf.int32)
    i_feed = tf.placeholder_with_default(i, shape=[], name="i_feed")
    i_up   = tf.assign(i, i_feed)
    
    # Setup the indexed stimulus
    s_i = tf.reshape(S_buff[:,i_feed], shape=[in_ln, 1])
    
    # Return s_i, and i
    return s_i, S_feed, S_up, i_feed, i_up,  

def InputSwitch(in_ln, InputSources):
    # Use variables, and assign to switch the input sources for a neural net
    # Give a list of switch ops, place the switchOp with the corresponding
    # Index to use that input source in a sess.run command,
    # This means that sources who's switchOp's are not calculated may not be
    # Calculated, due to tf's lazy evaluation
    # source triggers may need to be called before a run command that uses
    # The stimulus...
    
    s_sw = tf.get_variable("s_sw", shape=(in_ln, 1))
    
    # Initialize a list of source triggering ops
    sourceTrigs = []
    
    for i in range(0,len(InputSources)):
        # Create an assign op that sends source i as input
        source_trig = tf.assign(s_sw, InputSources[i])
        # Add to list of source assign ops
        sourceTrigs.append(source_trig)
    
    return s_sw, sourceTrigs
    

def WeightSpecial(name, shape, init, div=1, setDict={'N':10, 'STD':.01}):
    # This is a list of weight definitions that might be usefull for
    # Creating windowed network with special requirements
    
    W_pack = []
    
    # BASICS
    if init=="uniform":
        W_pack = tf.get_variable(name, shape, dtype=tf.float32, initializer= tf.random_uniform_initializer(0,1/div))
    elif init=="gau":
        W_pack = tf.get_variable(name, shape, dtype=tf.float32, initializer= tf.random_normal_initializer(stddev=1/div))
    elif init=="gauPos":
        W_pack = tf.get_variable(name, dtype=tf.float32, initializer= tf.abs(tf.random_normal(shape, stddev=1/div)))
    elif init=='ones':
        W_pack = tf.get_variable(name, shape, dtype=tf.float32, initializer= tf.initializers.ones) # Need to add div somehow

    # Weird Sparse Inits
    elif init=='sp:one': # Create weights where all windows only have 1 nnz value
        W_p_val = OneNNZWindows(shape)
        W_pack = tf.get_variable(name, shape, dtype=tf.float32, initializer= tf.initializers.constant(W_p_val))
    
    elif init=='sp:one+gau': # Add scaled gaussian noise to onennz windows
        W_p_val = OneNNZWindows(shape)
        W_p_val = W_p_val + np.random.normal(loc=0, scale=setDict["STD"], size=shape)/div
        W_pack = tf.get_variable(name, shape, dtype=tf.float32, initializer= tf.initializers.constant(W_p_val))
    
    elif init=='sp:ones': # Add scaled gaussian noise to onennz windows
        W_p_val = OneNNZWindows(shape, setDict['N'])
        W_pack = tf.get_variable(name, shape, dtype=tf.float32, initializer= tf.initializers.constant(W_p_val))    
    
    elif init=='sp:ones+gau': # Add scaled gaussian noise to onennz windows
        W_p_val = OneNNZWindows(shape, setDict['N'])
        W_p_val = W_p_val + np.random.normal(loc=0, scale=setDict["STD"], size=shape)/div
        W_pack = tf.get_variable(name, shape, dtype=tf.float32, initializer= tf.initializers.constant(W_p_val))    
    
    elif init=='sp:square(gauPos)': # Rais gaissain noise to power, to cause outliers to dominate
        W_p_val = np.power( np.abs( np.random.normal(loc=0, scale=setDict["STD"], size=shape) ), 4 )/div
        W_pack = tf.get_variable(name, shape, dtype=tf.float32, initializer= tf.initializers.constant(W_p_val))
    
    elif init=='sp:softmax(gauPos)': # Explicitly cause larger values to dominate using softmax
        vals = np.abs(np.random.normal(loc=0, scale=setDict["STD"], size=shape))
        vals = np.exp(vals/.1)
        W_p_val = vals/np.sum(vals,axis=0)/div
        W_pack = tf.get_variable(name, shape, dtype=tf.float32, initializer= tf.initializers.constant(W_p_val))
    
    else:
        print("Invalid initialization method")
        
    return W_pack   
    
def OneNNZWindows(shape, N=1):

    W_p_val = np.zeros( (shape) )
    for i in range(0,N):
        j_one = np.arange(shape[1])
        i_one = np.random.choice(shape[0], size=shape[1] )
        temp = np.zeros( (shape) )
        temp[i_one,j_one] = 1
        W_p_val = W_p_val+temp    

    return W_p_val
    
def WindowLinear(W_pack, s_winds):
    # Calculate linear output of windowed or locally connected layer
    r = tf.reduce_sum( W_pack*tf.squeeze(s_winds), axis=0)[:, tf.newaxis]
    return r
    
def WindowPool(W_pack, s_winds):
    p = tf.reduce_max( W_pack*tf.squeeze(s_winds), axis=0)[:, tf.newaxis]
    return p
    
    
def InPack(s, Inds):
    # indexes s into windows according to packed indices
    # indices can be numpy, or tf constants variables and tensors 
    # usefull for repeating windowed and fully connected layers
    
    # add expected zero element at index 0
    zero = np.zeros( (1,1) ).astype(np.float32)
    s_pad = tf.concat((zero, s), axis=0 )
    
    # Gather s values into packed windows
    s_winds = tf.reshape( tf.gather(s_pad, Inds), Inds.shape )
    
    return s_winds


def DownSample(pack, isTensor, imShape, factor ):
    # Use this to take an already created W_pack or Inds_pack and downsample them
    # Essientially only use some of the windows/weights
    
    # Create the downsample mask
    square1 = np.zeros( (factor,factor), dtype=int )
    cent = np.round(factor/2).astype(int)
    square1[cent,cent] = 1
    numX = np.round(imShape[0]/factor).astype(int)
    numY = np.round(imShape[1]/factor).astype(int)
    mask = np.tile(square1, [numX, numY])
    maskVec = np.ravel(mask, order='F')
    slices = maskVec>0
    #slices = np.squeeze((np.nonzero( maskVec )[0]).astype(int))
    
    print(slices.shape)
    
    # sample W_pack and Inds_pack
    down = []
    if isTensor:
        down = tf.boolean_mask( pack, slices, axis=1 )
    else:
        down = pack[:,slices]
    
    dwnShape = (np.array(imShape)/factor).astype(int)

    # return W_down and Inds_down and dwnShape
    return down, dwnShape
    
    
def CropRenorm(W_pack, limit, axis=0):
    # Crop weights above a certain threshold then renormalize
    mask = tf.to_float(W_pack>limit)
    W_pack_crop = W_pack*(1-mask)+limit*mask 
    W_pack_cropren = tf.nn.l2_normalize( W_pack_crop, axis = axis )
    return W_pack_cropren

def L2Step(W_pack, rate):
    # immitate an l2step
    return W_pack-W_pack*rate
    
    
def assign_addRen(W_pack, additive, lRate, rType = 'sum', limit=0, l2Rate=0):
    # add values to a packed weight matrix
    # then renormalize them, then assign the value
    
    W_pack_prime = W_pack + lRate*additive
    W_pack_new = []
    if rType == 'unit':
        W_pack_new = tf.nn.l2_normalize( W_pack_prime, axis = 0 )
    elif rType== 'sum':
        W_pack_new = W_pack_prime/(tf.reduce_sum(W_pack_prime, axis=0, keepdims=True)+.00000000001) 
    elif rType== 'max':
        W_pack_new = W_pack_prime/(tf.reduce_max(W_pack_prime, axis=0, keepdims=True)+.00000000001)
    else:
        print("Invalid norm type")
        
    if limit != 0:
        # Crop value to prevent over concentration
        W_pack_new = CropRenorm(W_pack_new, limit)
    
    if l2Rate != 0:
        # Apply weight decay that grow with weight size
        W_pack_new = L2Step(W_pack_new, l2Rate*lRate)
        
    # create assignment op to update W_pack
    W_pack_ren = tf.assign(W_pack, W_pack_new)
    return W_pack_ren
        
    
def CroppedSig(s, Min, Max):
    #    __
    # __/   Peicewise sigmoid [Min, Max]->[0, 1]
    span = Max-Min
    h_scaled = (s-Min)/(span+.0000000000001)
    h = tf.clip_by_value(h_scaled, 0, 1)
    return h
    

def SurroundConnected(s, imShape, lType="Malsburg", radii = [2, 6], order='F', pad='SYMMETRIC', opStr=[1, 1]):
    # This defines a fixed filter for use in making lateral connectivity layers
    # Importantly need to figure our how tensor flow does reshaping, it seems
    # To favor the final index first??
    
    d0 = radii[0]*2+1
    d1 = np.ceil(radii[1]*2+1).astype(int)
    r0 = radii[0]+.5
    r1 = radii[1]+.5
    
    # create offset images for constructing filter
    Yoff, Xoff = np.meshgrid(np.linspace(-radii[1],radii[1],d1), np.linspace(-radii[1],radii[1],d1), indexing='ij')

    lFilt = []
    # Define Static Filter
    if lType=='Malsburg':
        dist = np.sqrt( np.power(Xoff,2)+np.power(Yoff,2) )
        E = parabolicSpot(dist, r0, 1)
        I = parabolicSpot(dist, r1, 1)
        E = E/np.sum(E)*opStr[0]
        I = I/np.sum(I)*opStr[1]
        lFilt = E-I
    elif lType=='Parabolic':
        dist = np.sqrt( np.power(Xoff,2)+np.power(Yoff,2) )
        P = parabolicSpot(dist, r0, 1)
        P = P/np.sum(P)
        lFilt = P
        
    elif lType=='OnOff':
        dist = np.sqrt( np.power(Xoff,2)+np.power(Yoff,2) )
        E = dist<r0
        I = dist<r1
        E = E/np.sum(E)*opStr[0]
        I = I/np.sum(I)*opStr[1]
        lFilt = E-I

    elif lType=='Gaussian':
        std = radii[0]
        gau = np.exp( - (np.power(Xoff,2)+np.power(Yoff,2)) /(2*std*std) )
        gau = gau/np.sum(gau)
        lFilt = gau
    
    elif lType=='DoG':
        std1 = radii[0]/3
        gau1 = np.exp( - (np.power(Xoff,2)+np.power(Yoff,2)) /(2*std1*std1) )
        gau1 = gau1/np.sum(gau1)*opStr[0]
        std2 = radii[1]/3
        gau2 = np.exp( - (np.power(Xoff,2)+np.power(Yoff,2)) /(2*std2*std2) )
        gau2 = gau2/np.sum(gau2)*opStr[1]
        dog = gau1-gau2
        lFilt = dog
    else:
        print("INVALID_LATERAL_CONNECTION_TYPE")
    
        
    # Ensure that reshaping does not misalign image due to assumed index order
    if order=='F':
        a = [0, 0]
        a[0] = imShape[1]
        a[1] = imShape[0]
        imShape = a

    # Reshape into image
    s_im = tf.reshape(s,(imShape[0], imShape[1]), name='s_img')
    
    # Create padding
    pS = np.round(radii[1]).astype(int)
    padSizes = tf.constant([[pS,pS], [pS,pS]])
    s_im_mir = tf.pad(s_im, padSizes, pad)
    
    # Translate the image vector into the shape used by tf.nn.conv2d
    # batch width hight channel
    s_im_nwhc = tf.reshape(s_im_mir,(1, imShape[0]+pS*2, imShape[1]+pS*2, 1), name='s_img_nwhc')
    # width hight inChan outChan
    
    lFilt = np.reshape(lFilt, (d1,d1,1,1), order='F')
    
    
    # Perform the convolution
    r_im_nwhc = tf.nn.conv2d(s_im_nwhc, lFilt, (1,1,1,1), "VALID")
    
    # Put back into vector form
    r = tf.reshape(r_im_nwhc,(imShape[0]*imShape[1], 1), name='r')
    
    return r, lFilt
    
    
def parabolicSpot(x, width, hight):
    # Parabola hight of 1, intercepts [-1,1]
    y = -np.power(x/width, 2)+1
    y = y*hight
    y = np.maximum(y,0)
    return y
        

def WindowConnected(W_packShape, s, returnInd=False):
    # Given a packed weight and index matrix create tensor flow layer
    # whose weights can be updated externally or by the tf graph
    
    # Define weights
    W_pack = tf.get_variable("W_pack", W_packShape, dtype=tf.float32,)
    Ind_pack = tf.get_variable("Ind_pack", W_packShape, dtype=tf.int32,)
    
    # Define placeholder_with_default (0 matrix or current weights)
    W_pack_feed = tf.placeholder_with_default(W_pack, W_packShape, "W_pack_feed")
    Ind_pack_feed = tf.placeholder_with_default(Ind_pack, W_packShape, "Ind_pack_feed")
    
    # Define weights assign 
    W_pack_set = tf.assign(W_pack, W_pack_feed)
    Ind_pack_set = tf.assign(Ind_pack, Ind_pack_feed)
    
    # Trigger update
    W_up = tf.cast( tf.reduce_sum(W_pack_set), dtype=tf.int32)+tf.reduce_sum(Ind_pack_set)
    
    # add expected zero element at index 0
    zero = np.zeros( (1,1) ).astype(np.float32)
    s_pad = tf.concat((zero, s), axis=0 )
    
    # Gather s values into packed windows
    s_winds = tf.gather(s_pad, Ind_pack)
    
    # Caculate linear output
    #r = tf.reduce_sum( W_pack*tf.squeeze(s_winds), axis=0)
    
    # return relavent tensors
    if not returnInd:
        return s_winds, W_pack, W_up, W_pack_feed, Ind_pack_feed
    else:
        return s_winds, W_pack, Ind_pack, W_up, W_pack_feed, Ind_pack_feed
    
     

def LocallyConnected(s, imShape, radius, wShape = "disk", init='uniform', initDict={'N':2}, downSample=1, checkWinds=False): 
    # Creates a tensor flow layer similar to keras.locally_connected
    # S is a 2D array or 3D array first 2 dims are image dims
    # This layers weights will only be updated by the model

    #### Setup the window index matrix
    # Initialize reference values
    d = radius*2+1
    X,Y = np.meshgrid(np.arange(imShape[0]), np.arange(imShape[1]), indexing='ij')
    Yoff, Xoff = np.meshgrid(np.linspace(-radius,radius,d), np.linspace(-radius,radius,d), indexing='ij')

    
    # Convert to vectors
    x = np.ravel(X, order='F')[np.newaxis,:]
    y = np.ravel(Y, order='F')[np.newaxis,:]
    xoff = np.ravel(Xoff, order='F')[:,np.newaxis]
    yoff = np.ravel(Yoff, order='F')[:,np.newaxis]
    
    if wShape=="disk":
        roff = np.sqrt(np.power(xoff,2)+np.power(yoff,2))
        inDisk = roff<=(radius+.5) # Generous Cutoff
        xoff = xoff[inDisk][:,np.newaxis]
        yoff = yoff[inDisk][:,np.newaxis]

    win_ln = xoff.shape[0]

    # Create xy window matrices
    xWin = x+xoff
    yWin = y+yoff
    
    # Find out of bound elements
    outOfB = BoundMask(xWin, yWin, imShape )
    
    # Setup the packed window indices and get rid of outbound elements
    # Also rais indices by 1 to correctly index 0 pad
    Ind_val = UnwrapInd2(imShape, xWin, yWin)
    Ind_val[outOfB] = -1
    Ind_val = Ind_val + 1
    
    #### Create the tensorflow ops
    # add expected zero element at index 0
    zero = np.zeros( (1,1) )
    s_pad = tf.concat((zero, s), axis=0 )
    
    # Initialize W 
    W_pack = [] 
    
    Ind_dwn_shape = np.array(Ind_val.shape)
    Ind_dwn_shape[1] = int(Ind_dwn_shape[1]/downSample/downSample)  

    if not checkWinds: 
        
        if downSample==1:
            W_pack = WeightSpecial("W_pack", Ind_val.shape, init, div=1, setDict=initDict)
        else:
            W_pack = WeightSpecial("W_pack", Ind_dwn_shape, init, div=1, setDict=initDict)
        
    else:
        W_pack = tf.get_variable("W_pack", dtype=tf.float32, initializer=tf.constant(np.ones(Ind_val.shape).astype(np.float32)) )
     
    # Gather s values into packed windows
    Ind_val = Ind_val.astype(int)
    s_winds = tf.squeeze( tf.gather(s_pad, Ind_val) )
     
    dwnShape = []
    if downSample>1:
        s_winds, dwnShape = DownSample(s_winds,True, imShape, downSample)
    
    
    # return relavent tensors/numpy arrays
    if not checkWinds:
        if downSample==1:
            return s_winds, W_pack, Ind_val
        else:
            return s_winds, W_pack, Ind_val, dwnShape
        
    else:
        return s_winds, W_pack, Ind_val, x, y, xWin, yWin, Ind_val, outOfB

    
# Takes 1D index and converts to 2D index: Parrallel Compatable    
def WrapInd2(shape, c):

    i = np.mod(c,shape[0])
    j = np.floor(c/shape[0]).astype(np.int32)
    return i,j
    
def UnwrapInd2(shape, i, j):
    
    return j*shape[0]+i
    
# Set locations out of bounds to val
def BoundMask(X,Y, sz):
    
    outBound = np.logical_or( np.logical_or(X<0, (sz[0]-1)<X), np.logical_or(Y<0, (sz[1]-1)<Y) )
    return outBound
    


    

def TestUtils():
    
    print("\n__________________________WindowConnected Layer")
    # Cleanup graph
    tf.reset_default_graph()
    
    s_val = np.arange(10)[:,np.newaxis]+1
    W_p_val = np.ones([3,4])
    Ind_p_val = np.random.choice(11, size=[3,4]).astype(int)
    
    # Define the input
    s = tf.placeholder(dtype = tf.float32, shape=[10,1])
    # Define the windowed layer
    s_winds, W_p, W_up, W_p_feed, Ind_p_feed = WindowConnected([3,4], s)
    # Define the output
    r = WindowLinear(W_p, s_winds)
    
    with tf.Session() as sess:        
        sess.run(tf.global_variables_initializer())
        sess.run([W_up], feed_dict={W_p_feed:W_p_val, Ind_p_feed:Ind_p_val})
        
        r_val, W_p_val = sess.run([r, W_p], feed_dict={s:s_val})
        
    print("r_val:")
    print(np.transpose(r_val).astype(int) )
    print("\nWeights:")
    print(W_p_val)
    print("\nIndices:")
    print(Ind_p_val)
    print("\ns_val")
    print(np.transpose(s_val) )
    
    diff = np.sum( np.transpose(r_val)-np.sum(Ind_p_val,axis=0)  )
    if diff==0:
        print("\n-----------------PASS")
    else:
        print("\n-----------------FAIL")
        
    print("\n\n\n__________________________LocallyConnected Layer")
    
    # Cleanup graph
    tf.reset_default_graph()
    
    # 4x4 grid
    s_val = (np.arange( 16 )+1)[:,np.newaxis]
    s = tf.placeholder(dtype=tf.float32, shape=[16,1])
    
    # Define the locally connected layer
    s_winds, W_pack, Ind_val, x, y, xWin, yWin, Ind_val, outOfB = LocallyConnected(s, (4,4), 2, wShape="disk", checkWinds=True)
    
    # Define the output
    r = WindowLinear(W_pack, s_winds)
    
    
    with tf.Session() as sess:        
        sess.run(tf.global_variables_initializer())        
        r_val= sess.run(r, feed_dict={s:s_val})
    
    print("\nx: ")
    print(x.astype(int))
    print("\ny: ")
    print(y.astype(int))
    print("\n x offsets:")
    print(xWin.astype(int))
    print("\n y offsets:")
    print(yWin.astype(int))
    print("\nOut of bounds:")    
    print(outOfB.astype(int))
    print("\nr_val:")
    print(np.transpose(r_val).astype(int))
    print("\nWindow Indices:")    
    print(Ind_val.astype(int))
    print("\n Input Ind Ref:")
    print( np.reshape(np.arange(16)+1, [4,4], order='F') ) 
    
    diff = np.sum( np.transpose(r_val)-np.sum(Ind_val,axis=0)  )
    if diff==0:
        print("\n-----------------PASS")
    else:
        print("\n-----------------FAIL")
        
        
    print("\n\n\n__________________________SurroundConnected Layer")
    # Cleanup graph
    tf.reset_default_graph()
    
    s_val = (np.arange( 16 )+1)[:,np.newaxis]
    s = tf.placeholder(dtype=tf.float32, shape=[16,1])
    
    r, lFilt = SurroundConnected(s, (4,4), radii=[1,3] )
    
    print("\nFilter:")
    print(np.round(np.squeeze(lFilt),2 ) )
    
    
    print("\n\n\n__________________________MiniBatchIndexer")
    # Cleanup graph
    tf.reset_default_graph()
    
    S_val = np.reshape((np.arange( 20 )+1), (5,4) )
    s_i, S_feed, S_up, i_feed, i_up = MiniBatchIndexer(5, 4)
    
    with tf.Session() as sess:        
        sess.run(tf.global_variables_initializer())        
        dum = sess.run([S_up], feed_dict={S_feed:S_val})
        
        s_i_val, dum = sess.run([s_i, i_up], feed_dict={i_feed:1})
        s_i_val2     = sess.run( s_i       , feed_dict={i_feed:1})
        
        
    print("\nS Batch:")
    print(np.round(S_val,0))    
    print("\ns_i With index Update:")
    print(np.round(s_i_val,0))
    
    print("\ns_i No Index Update:")
    print(np.round(s_i_val2,0))
    
    diff = np.sum( np.abs(S_val[:,1]-s_i_val[:,0])) + np.sum( np.abs(S_val[:,1]-s_i_val2[:,0]) ) 
    if diff==0:
        print("\n-----------------PASS")
    else:
        print("\n-----------------FAIL")
    
        
    print("\n\n\n__________________________InputSwitch")
    # Cleanup graph
    tf.reset_default_graph()
    
    s_1_val = np.reshape( np.arange( 5 ), (5,1) )
    s_2_val = np.reshape( np.arange( 5 )*2, (5,1))
    
    s_1 = tf.constant( s_1_val, dtype=tf.float32 )
    s_2 = tf.constant( s_2_val, dtype=tf.float32 )
    
    s, sourceTrigs = InputSwitch(5, [s_1, s_2])
    trig1 = sourceTrigs[0]
    trig2 = sourceTrigs[1]
    
    r = s*2
    
    with tf.Session() as sess:        
        sess.run(tf.global_variables_initializer())   
        
        r_val , dum = sess.run([r, trig1])
        r_val2, dum = sess.run([r, trig1])
        r_val3, dum = sess.run([r, trig2])
        r_val4, dum = sess.run([r, trig2])
        
        # Test if it can be left blank without error
        r_val4, dum = sess.run([r, []])
        
    print("\nSource Set to 1: run 1")
    print(r_val)
    
    print("\nSource Set to 1: run 2")
    print(r_val2)
    
    print("\nSource Set to 2: run 3")
    print(r_val3)
    
    print("\nSource Set to 2: run 4")
    print(r_val4)
    
    diff = np.sum(np.abs(r_val-s_1_val*2)) +np.sum(np.abs(r_val3-s_2_val*2))
    if diff==0:
        print("\n-----------------PASS")
    else:
        print("\n-----------------FAIL")
    
    print("\n\n\n__________________________ColWiseVec")
    # Cleanup graph
    tf.reset_default_graph()
    
    vecOr = np.arange(16)[:,np.newaxis]
    A_val = np.reshape(vecOr, [4, 4], order='F')
    
    A = tf.constant( A_val.astype(np.float32) )
    
    Avec = ColWiseVec(A)
    
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())   
        
        Avec_val = sess.run(Avec)
        
    diff = np.sum(np.abs(Avec_val-vecOr))
    
    print("\nA")
    print(A_val)
    print("\nvec(A)")
    print(Avec_val.transpose())
    
    
    if diff==0:
        print("\n-----------------PASS")
    else:
        print("\n-----------------FAIL")
        
        
    
    
#TestUtils()


def test_WeightSpecial():
    
    import matplotlib.pyplot as plt
    
    shape = (200, 10000)
    
    tf.reset_default_graph()
    
    W1 = WeightSpecial('W1', shape, 'sp:one')
    W2 = WeightSpecial('W2', shape, 'sp:one+gau')
    W3 = WeightSpecial('W3', shape, 'sp:square(gauPos)')
    W4 = WeightSpecial('W4', shape, 'sp:softmax(gauPos)')
    W5 = WeightSpecial('W5', shape, 'sp:ones',          setDict={'N':10})
    W6 = WeightSpecial('W6', shape, 'sp:ones+gau',      setDict={'N':10})
    
    
    
    with tf.Session() as sess:        
        sess.run(tf.global_variables_initializer())        
        W1v, W2v, W3v, W4v, W5v, W6v = sess.run([W1, W2, W3, W4, W5, W6])
    
    fig = plt.figure()
    ax1 = fig.add_subplot(241)
    ax2 = fig.add_subplot(242)
    ax3 = fig.add_subplot(243)
    ax4 = fig.add_subplot(244)
    ax5 = fig.add_subplot(245)
    ax6 = fig.add_subplot(246)
    
    
    
    ax1.set_title('sp:one')        
    ax1.hist(np.ravel(W1v, order='F'), bins=50, range=(-.5, 2), color='pink' )
    
    ax2.set_title('sp:one+gau')        
    ax2.hist(np.ravel(W2v, order='F'), bins=50, range=(-.5, 2), color='pink' )
    
    ax3.set_title('sp:square(gauPos)')        
    ax3.hist(np.ravel(W3v, order='F'), bins=50, range=(-.5, 2), color='pink')
    
    ax4.set_title('sp:softmax(gauPos)')        
    ax4.hist(np.ravel(W4v, order='F'), bins=50, range=(-.5, 2), color='pink')
    
    ax5.set_title('sp:ones')        
    ax5.hist(np.ravel(W5v, order='F'), bins=50, range=(-.5, 2), color='pink' )
    
    ax6.set_title('sp:ones+gau')        
    ax6.hist(np.ravel(W6v, order='F'), bins=50, range=(-.5, 2), color='pink' )


    
#test_WeightSpecial()


def Test_BatchFunctions():
    
    
    tf.reset_default_graph()
    
    b_ln = 2
    # Create batch surregate
    s_val = (np.arange( 16 )+1)[:,np.newaxis]*np.ones((1,b_ln))
    s = tf.placeholder(dtype=tf.float32, shape=[16,b_ln])
    
    W_pack = np.ones( (3,4) )
    Ind_pack = np.random.choice(17, size=[3,4]).astype(int)
    
    
    s_winds = InPack_batch(s, Ind_pack, b_ln)
    r = WindowLinear_batch(W_pack, s_winds)
    
    with tf.Session() as sess:        
        sess.run(tf.global_variables_initializer())        
        
        s_winds_val, r_val = sess.run([s_winds, r], feed_dict={s:s_val})
    
    print("Testing Batch In Pack")
    print("\nInds:")
    print(Ind_pack)
    
    print("\ns_winds b=0:")
    print(s_winds_val[:,:,0])
    
    print("\ns_winds b=1:")
    print(s_winds_val[:,:,1])

    print("\nout b=0")
    print(r_val[:,0])
    print("\nout b=1")
    print(r_val[:,0])
        
    
    tf.reset_default_graph()
    
    b_ln = 2
    # Create batch surregate
    s_val = np.zeros((100,b_ln))
    s_val[9,:] = 100
    s = tf.placeholder(dtype=tf.float32, shape=[100,b_ln])
    
    r, lFilt = SurroundConnected_batch(s, [10,10], b_ln, lType="Malsburg", radii = [5, 6], order='F', pad='SYMMETRIC')
    
    with tf.Session() as sess:        
        sess.run(tf.global_variables_initializer())        
        
        r_val = sess.run(r, feed_dict={s:s_val})
    
    r_val = np.round(r_val)
    
    s_im = np.reshape(s_val, (10,10,2), order='F').astype(int)[:,:,0]
    r_im = np.reshape(r_val, (10,10,2), order='F').astype(int)[:,:,0]
    
    print("\n\nTesting Batch Surround Connected")
    print("\ns_im")
    print(s_im)
    print("\nr_im")
    print(r_im)
    
    
#Test_BatchFunctions()

