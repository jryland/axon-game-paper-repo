# -*- coding: utf-8 -*-
"""
Created on Tue Jun 11 15:39:13 2019

@author: jwr071000
"""
import numpy as np

import Ry_VisionInput as vin
import Ry_AxonGame as rag
import Ry_TF_Util as tfu
import tensorflow as tf

import matplotlib.pyplot as plt
import time

from PIL import Image
    


# Demonstrate a pixels channel's correlational window with spontanious data
# Windows from Correlational Neighborhoods
# s sample is expect to be a row
def RowCorr(S_batch, index):
    
    S_norm = S_batch-np.mean(S_batch,axis=1,keepdims=True)
    S_norm = S_norm/np.linalg.norm(S_norm, axis=1,keepdims=True)
    s_vec = S_norm[index,:]
    s_vec = s_vec[np.newaxis,:]
    corrVec = np.square(np.sum( s_vec*S_norm, axis=1, keepdims=True))
    
    return corrVec
    
def test_ScaleSpaceCorr():
    
    randSample = np.random.uniform(size=[10000,1000])/100000
    
    corrVec = RowCorr(randSample, 1)
    
    print(corrVec[0:10])
    
#test_ScaleSpaceCorr()

def ExpTransform(x, mult=5, base=2.718281828459045, shift=0):

    # Function used by Engel and Glover
    x = np.power(base,(x+shift)*mult)
    x = (x-np.min(x))/(np.max(x)-np.min(x))
    return x

def AccumTransform(x):
    indexer = np.argsort(x+np.random.normal(scale=.00, size=x.shape))
    x = np.zeros(indexer.shape)
    x[indexer] = np.arange(len(indexer))
    x = (x-np.min(x))/(np.max(x)-np.min(x))
    return x
    
    
def ChemMaker_V1(chemoLGN, scaleRad, size, opt='basic', method='EXP', mult=5, base=2.718281828459045):
    
    # Edit input chemo map
    # Assume that it is using split radius encoding
    chemoLGN[:,:,2] = (chemoLGN[:,:,1]<.5)*(scaleRad[:,np.newaxis]<.5)
    chemoLGN[:,:,1] = chemoLGN[:,:,1]*chemoLGN[:,:,2]
    chemoLGN[:,:,1] = chemoLGN[:,:,1]*2

    # Visualize distribution accross visual angle
    xPoints = np.ravel(chemoLGN[:,:,1])[np.ravel(chemoLGN[:,:,2])==1]

    if method == 'EXP':
        xPoints = ExpTransform(xPoints, mult, base)
    if method == 'ACCUM':
        xPoints = AccumTransform(xPoints)
        

    fig = plt.figure()
    ax1 = fig.add_subplot(1,2,1)
    ax1.hist(xPoints, bins=10)
    
    ap = np.ones(size)
    dv = np.ones(size)
    b = np.zeros(size)
        
    ap = ap * np.linspace(0,1,ap.shape[0])[:,np.newaxis]
    dv = dv * np.linspace(0,1,ap.shape[1])[np.newaxis,:]
        
    # Perform exp transform
    if method == 'EXP':
        chemoLGN[:,:,1] = ExpTransform(chemoLGN[:,:,1], mult, base)
    if method == 'ACCUM':
        chemoVec = np.ravel(chemoLGN[:,:,1])
        valid = np.ravel(chemoLGN[:,:,2])==1
        xPoints = chemoVec[valid]
        xAccum = AccumTransform(xPoints)
        chemoVec[valid] = xAccum
        chemoLGN[:,:,1] = np.reshape(chemoVec, chemoLGN[:,:,1].shape)

    #dv = ExpTransform(dv)    
                      
    bound_file = []
    if opt == 'basic_LR':
        bound_file = './bound_V1_LR.png'
    elif opt == 'basic_L':
        bound_file = './bound_V1_L_V2.png'
    else:
        print("INVALID BOUND SELECTION")
                          
    img = Image.open(bound_file)
    img = img.resize(np.flip(size,axis=0)) # resizes image in-place
    b = (np.asarray(img)[:,:,2]>0).astype(float)                       
    
    # Concatentate chemo-signales
    chemoV1 = np.concatenate( (ap[:,:,np.newaxis],dv[:,:,np.newaxis], b[:,:,np.newaxis]), axis=2 )
    
    return chemoLGN, chemoV1
    
def test_ChemMaker_V1():
    
    fig = plt.figure()
    ax1 = fig.add_subplot(1,3,1)
    ax2 = fig.add_subplot(1,3,2)
    ax3 = fig.add_subplot(1,3,3)
    
    chemo1 = ChemMaker_V1([100,200],opt='basic_LR')
    #print(chemo1.shape)
    ax1.imshow(chemo1)
    
    chemo2 = ChemMaker_V1([100,200],opt='basic_L')
    #print(chemo2.shape)
    ax2.imshow(chemo2)
    
    ax3.plot(chemo2[0,:,1])
    
    
#test_ChemMaker_V1()
    

def Map_Guides(rez, scaleNum, std):
    
    xTicks = np.linspace(-.5,.5, rez+1)
    yTicks = np.linspace(-.5,.5, rez+1)
    
    X, Y = np.meshgrid(xTicks, yTicks)
    
    RHO, PHI = tfu.cart2pol(Y,X)
    
    PHI[RHO>.5] = 0
    RHO[RHO>.5] = .5
    
    # use only left field
    mid = int(rez/2)
    RHO = RHO[:,:mid]
    PHI = PHI[:,:mid]    

    fig1 = plt.figure()
    fig2 = plt.figure()
    ax1 = fig1.add_subplot(1,1,1)
    ax2 = fig2.add_subplot(1,1,1)
    
    ax1.imshow(-RHO,cmap='jet')
    ax1.axis('off')
    ax2.imshow(PHI,cmap='jet')
    ax2.axis('off')
    
    width = std*2*2**scaleNum
    xTicks = np.linspace(-width/2, width/2, width+1)
    yTicks = np.linspace(-width/2, width/2, width+1)
    X, Y = np.meshgrid(xTicks,yTicks)
    
    gau_ls= []
    
    for i in range(1, scaleNum+1):
        gauPos = np.exp( -(X**2+Y**2)/(2*std*2**i))
        gauPos = gauPos/np.sum(gauPos)
        gauNeg = np.exp( -(X**2+Y**2)/(2*std*2**(i+1)))
        gauNeg = gauNeg/np.sum(gauNeg)
        onOff = gauPos-gauNeg
        onOff = onOff/np.max(np.abs(onOff))
        gau_ls.append(onOff)
        
    scaleIm = np.concatenate(gau_ls, axis=0)
    
    fig3 = plt.figure()
    ax3 = fig3.add_subplot(1,1,1)
    ax3.imshow(scaleIm, cmap="seismic", vmin=-1,vmax=1)
    ax3.axis("off")
    
    fig4 = plt.figure()
    ax4 = fig4.add_subplot(1,1,1)
    ax4.imshow(-np.arange(0,scaleNum*8)[:,np.newaxis], cmap="jet")
    ax4.axis("off")
    
    

def AxonGame_Scale_and_Occularity(largeMap=False, dispAg=True, dispCorr=False):
    
    tf.reset_default_graph()
    
    expMult  = 5
    expBase  = np.e
    Int      = 50
    order    = 3
    method   = 'ACCUM'

    
    if largeMap:
        Int = 50
        order = 5
        expMult = 15
        expBase = 2.0
        method   = 'ACCUM'
        
    Ext      = Int*2**order  # Should be 800
    extShape = [Ext, Ext]
    intShape = [Int, Int]
    std      = .75
        
    # Define input generator and processing
    with tf.variable_scope("in"):
        
        # Generate random gaussians accross scale
        dat = tfu.GaussianGenerator(extShape, 32, aspect=[4, 8], scaleRange=[.25, 512], foveateCenters=True)
    
        # Add channel and batch dimensions
        dat = tf.reshape(dat, [Ext,Ext, 1,1])
    
        # Create scale pyramid
        pyrVec, pyrLib = tfu.WHCB_Dog_Pyramid_Standard(dat, Int, order, std, chemo='split-pol-rad')
        
        # Create random variance between eyes by changing amplitude
        r_mult = tf.random_uniform( [2],minval=-.8,maxval=1 )
        
        # Double eye input vec
        pyrVec2 = tf.concat( (pyrVec*r_mult[0], pyrVec*r_mult[1]), axis=0 )
    
        
    # Make 
    inSize = [pyrLib["pyrVecLength"]*2, 1]
    in_ln = inSize[0]
    cSize = [170,250] 
    if largeMap:
        cSize = [170,250]
    outSize = np.prod(cSize)    
    plex = 1
    
    # Target depth to set branch num
    targDepth = 16 # 5 was good
    branchNum = int(np.ceil(targDepth*outSize/inSize[0]))
    
    ones = np.ones( pyrLib["pyrVecLength"]*1  )
    eyeVec = np.concatenate( (ones*-1,ones), axis=0 )
    scaleVec = np.concatenate( (pyrLib["scaleVec"], pyrLib["scaleVec"]), axis=0)
    angleVec = np.concatenate( (pyrLib["phiVec"], pyrLib["phiVec"]), axis=0)
    radVec = np.concatenate((pyrLib["radVec"], pyrLib["radVec"]), axis=0)
    scrVec = np.concatenate((pyrLib["scrVec"], pyrLib["scrVec"]), axis=0)
    
    # Setup input target map
    pyrMap   = pyrLib["pyrChemo"][:,np.newaxis,:]
    chemMap1 = np.concatenate( (pyrMap,pyrMap), axis=0)
    

    # Setup output target map
    chemMap1, chemMap2 = ChemMaker_V1(chemMap1, scrVec, cSize, opt='basic_L', mult=expMult, base=expBase, method=method)
    
    print([np.min(chemMap1[:,:,0]), np.max(chemMap1[:,:,0])])
    print([np.min(chemMap2[:,:,0]), np.max(chemMap2[:,:,0])])
    
    print([np.min(chemMap1[:,:,1]), np.max(chemMap1[:,:,1])])
    print([np.min(chemMap2[:,:,1]), np.max(chemMap2[:,:,1])])
    
    # Correct for the fact that we use the 'C' ravel order for generating
    # the stimulus vectors
    #chemMap1 = np.reshape(chemMap1, [in_ln,1,3], order='C') 
    
    
    # DEFINE AXON 
    #ag = rag.AxonGame(chemMap1, chemMap2, coactNum=10, branchNum=25, autoScale=True, coType="Cov")
    chemoMult = .1#.2
    chemoStr1=.30 * chemoMult
    chemoStr2=.08 * chemoMult
    boundStr = 50
    seniorStrs =  [.002, .04]# [.001, .5]
    chemoAspect = [1, 1]    

    exubPeriod = 600
    stabPeriod = 200

    
    ag = rag.AxonGame(chemMap1, chemMap2, coactNum=50, branchNum=branchNum, boundStr=boundStr, autoScale=True, coType="Cov",
                      senByCo = True,
                      seed=True, # True
                      seedNoise = 0.05,#.2,
                      senByCoBeta = .05,
                      plexNum=plex,
                      chemoStr1=chemoStr1, 
                      chemoStr2=chemoStr2,
                      chemoAspect = chemoAspect,
                      corrStr= 6, #1
                      compStr= .016,#.016, 
                      exubPeriod = exubPeriod,
                      stabilizePeriod = stabPeriod,
                      branchExub = [2.5,  .025],  #[.5,  .05],
                      growProps = [1, .4],
                      seniorStrs=seniorStrs)
    
    
    if (largeMap):
        chemoMult = 1#.2# 1 was Cool
        chemoStr1=.30 * chemoMult
        chemoStr2=.08 * chemoMult
        exubPeriod = 200
        stabPeriod = 100
        #branchNum = 10
        branchNum = 8 #30
        compStr = .08
        boundStr = 10*5
        targNoise = 0#.05
        seedNoise = 0#.05
        ag = rag.AxonGame(chemMap1, chemMap2, branchNum=branchNum, seniorStrs = [.002, .04], chemoStr1=chemoStr1, chemoStr2=chemoStr2, boundStr = boundStr, corrStr=6, seed=True, seedNoise=seedNoise, targNoise=targNoise, coType="Cov", branchExub = [1.5, .025],
                          exubPeriod=exubPeriod, stabilizePeriod = stabPeriod, compStr=compStr)
    
        
    # Print some information
    print("branchNum: "+str(branchNum))
    
    
    # Figures
    inFig = tfu.PyrFigure(pyrLib)
    agFig = rag.AxonGameFig(ag)
    agEye   = rag.AxonGameTraceFig(ag, np.squeeze(eyeVec), "Binocularity", cmap='gray') 
    agScale = rag.AxonGameTraceFig(ag, np.squeeze(scaleVec), "Spatial Frequency", cmap='jet') 
    agAngle = rag.AxonGameTraceFig(ag, np.squeeze(angleVec), "Visual Angle: Polar Angle", cmap='jet') 
    agRad = rag.AxonGameTraceFig(ag, -np.squeeze(radVec), "Visual Angle: Polar Eccentricity", cmap='jet') 
    
    # Cross Section figure
    xVec   = np.concatenate( (pyrLib["xVec"],pyrLib["xVec"]), axis=0 )
    yVec   = np.concatenate( (pyrLib["yVec"],pyrLib["yVec"]), axis=0 )
    sclVec = np.concatenate( (pyrLib["sclVec"],pyrLib["sclVec"]), axis=0 )
    xText = "Vertical View Angle"    
    yText = "Horizontal View Angle"
    eyeText = "Occularity"
    sclText = "Spatial Frequency Band"
    xAxes =  [xVec,     xVec  ,     xVec]    
    yAxes =  [yVec,     sclVec,     eyeVec]
    xNames = [xText,    xText,      xText]
    yNames = [yText,    sclText,    eyeText]
    vecLength = pyrLib["pyrVecLength"]*2
    csvFig = tfu.CrossSectionViews(vecLength, xAxes, yAxes, xNames, yNames, alphaBase=.005, allP=True )
    dispInds = np.random.choice(np.prod(cSize), size=[5])
    
    start = time.time()
    
    #plt.pause(5)
    
    with tf.Session() as sess:
    
        t = 0
        
        # Produce some spontanious activities
        print("_____GETTING ACTIVITES")
        batch = 2000
        spontActs = np.zeros( (inSize[0], batch) )
        for i in range(0, batch):
            pyrVec2_val = sess.run(pyrVec2)
            spontActs[:,i] = pyrVec2_val[:,0]

            if np.mod(i,100)==0:
                #print("Stims Generated: "+str(i))
                print("Percent Complete: "+str(  np.round(i/batch*100) ))
                pyrVec_val = sess.run(pyrVec)
                inFig.update(tfu.OpposedNorm(pyrVec_val))
                plt.pause(.01)

                
        # Demonstrate Correlational Windows
        if dispCorr:
            for i in range(0, 10):
                csvFig2 = tfu.CrossSectionViews(vecLength, xAxes, yAxes, xNames, yNames, alphaBase=.005, allP=True )
                corrInd = np.random.choice(inSize[0])
                corr = RowCorr(spontActs, corrInd)
                print("Corr Range: "+str([np.min(corr), np.max(corr)]))
                #corr = np.minimum(corr,.5)*2
                csvFig2.refresh(vals= corr, cutoff=.3, cont=True )
                
        if dispAg:
            # Grow axon game
            print("Development For: "+str(ag.finishTime))
            for i in range(0,ag.finishTime):
                
                # Update model
                ag.run(1, newActivity = spontActs)
                
                # update graphics
                agFig.refresh()
                agEye.refresh()
                agScale.refresh()
                agAngle.refresh()
                agRad.refresh()
                if np.mod(i, 20)==0:
                    W_pack, Ind_pack = ag.getPackedW()
                    csvFig.refresh( Ind_pack=Ind_pack, dispInds=dispInds )
                
                # Calculate time
                mins = int( np.floor((time.time()-start)/60))
                secs = int( np.floor((time.time()-start)-mins*60))
                
                print("\n____________________")
                print("Time      : "+str(mins)+"m  "+str(secs)+"s")
                print("Step      : "+str(t))
                print("Exuberance: "+str(np.round(ag.gauWidth, 3)))
                print("Grow Prop : "+str(np.round(ag.growProp*100))+"%")
                print("Seniority : "+str(np.round(ag.seniorStr, 6)))
                print("Sparsity  : "+str(np.round(ag.W.nnz/np.prod(ag.W.shape), 4 ))+"%")
                print("Max Depth : "+str(np.max(ag.branchDepth))+" brchs")
                print("Avg Depth : "+str(np.round(np.mean(ag.branchDepth)))+" brchs")
                #print("Pac Depth : "+str(W_pack.shape[0])+' brchs')
                print("Penalty   : "+str(np.round(ag.avgFit, 3))+" p/b")
                print(ag.timeReport)
                
                t = t + 1
                
                plt.pause(.1)
                    
                
                
#Map_Guides(200, 4, .75)
#AxonGame_Scale_and_Occularity(largeMap = False, dispAg = True, dispCorr=False)

#Map_Guides(200, 6, .75)

# This command will replicate the simulatio in the paper
AxonGame_Scale_and_Occularity(largeMap = True, dispAg = True, dispCorr=False)

