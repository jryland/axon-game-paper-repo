# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 15:10:40 2018

@author: jwr071000
"""

import numpy as np
import scipy as sp
import scipy.sparse as spr
from scipy import ndimage as nd
import matplotlib.pylab as plt
import Ry_MathUtil as mu

def ApplyOnOff(grey, width=1, mode="same"):
    # Apply LGN like on-off-center and off-on-center filtering then
    # splice the signals together to maintain orginal size
    grey = nd.filters.gaussian_laplace(grey, width, mode='nearest')
    onoff = np.maximum(grey,0)
    offon = np.maximum(-grey,0)
    evens = np.arange(0, grey.shape[1],2)
    grey = onoff
    
    if mode=="same":
        grey[:,evens] = offon[:,evens]
    else:
        grey = np.concatenate( (onoff,offon ), axis=1)
        
    return grey


def SimpleBoxSweeps(shape, sweepNum, imagesPerSw, width, onoffSuround = False):
    print("WIP")
    # reparametize 1D gaussain allong random vector in 2D
    xticks = np.linspace(-shape[0]/2,shape[0]/2, shape[0])
    yticks = np.linspace(-shape[1]/2,shape[1]/2, shape[1])
    
    xv, yv = np.meshgrid(xticks, yticks, sparse=False, indexing='ij')
    
    acts = np.zeros((np.prod(shape),sweepNum*imagesPerSw))
    imgs = np.zeros((shape[0], shape[1], sweepNum*imagesPerSw))
    
    # Sweep loop
    t = 0
    for s in range(0, sweepNum):
        
        # pick random direction
        rDir = np.random.normal(1, 1, size=[2])
        rDir = rDir/np.sqrt(np.sum(np.power(rDir,2)))
        
        # needed to avoid correlation in the center of image
        randOffset = np.random.normal(1,1) * shape[0]/3
        
        # Give both offon and onoff symetric activity
        randSign = np.sign(np.random.normal())        
        
        # Image Loop
        for i in range(0, imagesPerSw):
        
            o = 1.7*np.max(shape)/imagesPerSw*i-np.max(shape) 
            z = xv*rDir[0]+yv*rDir[1]
            grey = np.exp( -np.power((z+o+randOffset)/width,2) )
            
            # apply on/off center surround processing
            if onoffSuround:
                grey = grey*randSign
                grey = ApplyOnOff(grey)
                
            imgs[:,:,t] = grey
            acts[:,t] = np.ravel(grey, order='F')
            t = t + 1
            
    return acts, imgs

def SimpleBiSweeps(shape, sweepNum, imagesPerSw, width, onoffSuround = False):
    
    actsL, imgsL = SimpleBoxSweeps(shape, sweepNum, imagesPerSw, width, onoffSuround = onoffSuround)
    actsR, imgsR = SimpleBoxSweeps(shape, sweepNum, imagesPerSw, width, onoffSuround = onoffSuround)
    
    numPix = actsL.shape[0]
    splice = np.arange(0, numPix, 2)
    
    actsR[splice,:] = actsL[splice,:]
    
    numSteps = actsL.shape[1]
    imgs = np.reshape(actsR, (shape[0],shape[1],numSteps), order='F') 

    eye = np.ones( (numPix) ) 
    eye[splice] = -1
    
    return actsR, imgs, eye 
        
        
def TestBoxSweeps():
    
    eye = []
    #acts, imgs = SimpleBoxSweeps([50,50], 10, 20, 5, onoffSuround=True)
    acts, imgs, eye = SimpleBiSweeps([50,50], 10, 20, 5, onoffSuround=True)
    
    fig = plt.figure()
    ax = plt.subplot(111)
    
    #print(imgs.shape)
    
    for i in range(0, imgs.shape[2]):
        
        #print(i)
        ax.cla()
        ax.imshow(imgs[:,:,i])    
        plt.show()
        plt.pause(.01) 
        
#TestBoxSweeps()



def SimpleGaussianGenerator(shape, gauNum=1, aspect = [1, 8], sizes=[.5, 2], contrasts = [.1, 1], onoff=True ):
    xticks = np.linspace(-shape[0]/2,shape[0]/2, shape[0])
    yticks = np.linspace(-shape[1]/2,shape[1]/2, shape[1])
    
    xv, yv = np.meshgrid(xticks, yticks, sparse=False, indexing='ij')
    
    z = np.concatenate( (xv[:,:,np.newaxis], yv[:,:,np.newaxis]),  axis=2)
    
    im = np.zeros(shape)
    
    C = np.diag(aspect/np.linalg.norm(aspect))
    
    
    for i in range(0,gauNum):
        
        # Find center
        xc = np.random.uniform( np.min(xticks)*1.33, np.max(xticks)*1.33)
        yc = np.random.uniform( np.min(yticks)*1.33, np.max(yticks)*1.33)        
        cent = np.array([xc, yc])
        
        # scale and rotate covariance matrix
        sc = np.random.uniform(sizes[0], sizes[1])
        C_scaled = C/sc
        R = RotMatrix2D(np.random.uniform(-np.pi,np.pi))
        C_rot = np.matmul(C_scaled, R)
        C2 = np.matmul(np.transpose(C_rot),C_rot)
        C_inv = np.linalg.inv(C_rot)
        
        # Hack for testing purposes
        #C2 = C
        
        # Compute gaussain value for all pixels
        z_cent = z-cent[np.newaxis,np.newaxis,:]
        # Tensor op Z_xyi * C_ij * Z_xyj = G_xy 
        B = np.sum(z_cent[:,:,:,np.newaxis]*C2[np.newaxis,np.newaxis,:,:], axis=2 )
        # Tensor op B_xyj * Z_xyj = G_xy
        D = np.sum(B*z_cent, axis = 2)
        G = np.exp(-np.abs(D))
        
        # Normalize and give sign
        #G = G/(np.sum(G)+.000000000000001)
        G = G/(sc*sc)
        
        if onoff:
            G = G * np.sign( np.random.normal() )
        
        im = im+G
    
    if onoff:
        im = ApplyOnOff(im)
    s = np.ravel(im, order='F')[:,np.newaxis]
        
    return s, im
  

    
def RotMatrix2D(theta):
    c, s = np.cos(theta), np.sin(theta)
    R = np.array(((c,-s), (s, c)))
    return R
    
    
def TestSimpleGaussianGenerator():
    
    
    fig = plt.figure()
    ax = plt.subplot(111)
    
    for i in range(0, 1000):
        
        s, im = SimpleGaussianGenerator( (50,50) )
        
        im = im-np.min(im)/(np.max(im)-np.min(im))
        
        ax.cla()
        ax.imshow(im)    
        plt.show()
        plt.pause(.5) 
#TestSimpleGaussianGenerator()


def SimpleBatteryOR(shape, width, deg=12, gap=36, onoffSurround=True, mode="same"):
    # Create a simple battery for detecting which neurons are sensitive
    # To specific oriented edges
    
    # CRITICAL INFORMATION:
    # the gab must be fairly large, if too closely spaced lateral inhibition
    # in these models will produce inaccurate maps!
    
    xticks = np.linspace(-shape[0]/2,shape[0]/2, shape[0])
    yticks = np.linspace(-shape[1]/2,shape[1]/2, shape[1])
    
    xv, yv = np.meshgrid(xticks, yticks, sparse=False, indexing='ij')
    
    szMult = 1
    if mode=="full":
        szMult = 2
    
    orData = np.zeros((np.prod(shape)*szMult,gap*deg*2))
    orImgs = np.zeros((shape[0], shape[1]*szMult, gap*deg*2))
    orVal = np.zeros((gap*deg*2))
    
    t = 0
    sign = [1,-1]
    
    for s in range(0,len(sign) ):
        for i in range(0, deg):
            
            rDir = mu.pol2cart(1, i/deg*np.pi)
            
            for p in range(0, gap):
                
                grey = np.zeros(shape)
                z = xv*rDir[0]+yv*rDir[1]
                
                # make twenty bars at gap offset
                for o in range(-20, 20):
                    grey = grey + np.exp( -np.power((z+o*gap+p)/width,2) )
                
                grey = grey*sign[s]
                
                if onoffSurround:
                    grey = ApplyOnOff(grey, mode=mode)
                    
                orData[:, t] = np.ravel(grey, order='F')
                orImgs[:,:,t] = grey
                orVal[t] = i/deg
                t = t + 1
                
    return orData, orImgs, orVal


def SimpleBatteryOR_sin(shape, widths=[3, 4, 5] , deg=12, cycle=10, onoffSurround=True):
    # Create a simple battery for detecting which neurons are sensitive
    # To specific oriented edges
    xticks = np.linspace(-shape[0]/2,shape[0]/2, shape[0])
    yticks = np.linspace(-shape[1]/2,shape[1]/2, shape[1])
    
    xv, yv = np.meshgrid(xticks, yticks, sparse=False, indexing='ij')
    
    wNum = len(widths)
    
    orData = np.zeros((np.prod(shape),wNum*deg*cycle))
    orImgs = np.zeros((shape[0], shape[1], wNum*deg*cycle))
    orVal = np.zeros((wNum*deg*cycle))
    
    t = 0 
    for i in range(0, deg):
        
        rDir = mu.pol2cart(1, i/deg*np.pi)
        
        for w in range(0, wNum):
            
            for c in range(0, cycle):
                
                grey = np.zeros(shape)
                z = xv*rDir[0]+yv*rDir[1]
                
                # make twenty bars at gap offset
                grey = np.sin((z/widths[w])*np.pi*2 + (c/cycle)*np.pi*2)
                
                # adjust amplitude
                grey = grey*widths[w]
                
                if onoffSurround:
                    grey = ApplyOnOff(grey)
                #else:
                #    grey = np.maximum(grey, 0)
                    
                orData[:, t] = np.ravel(grey, order='F')
                orImgs[:,:,t] = grey
                orVal[t] = i/deg
                t = t + 1
            
    return orData, orImgs, orVal
    

#_____________________________________________________MAKE A BETTER VERSION OF THIS    
def SimpleBatteryOC(eye):
    # Create a simple battery for detecting which neurons are sensitive
    # To eye dominance
    eye = eye[:,np.newaxis]
    ocData = np.concatenate( (np.zeros_like(eye), np.maximum(eye, 0),np.maximum(-eye, 0)), axis=1 ) 
    ocVal = np.array([.5, 0, 1,])
    
    return ocData, ocVal

    
    
def TestSimpleBatteryOR():
    
    orData, orImgs, orVal = SimpleBatteryOR([50,50], 3, deg=24, gap=25, onoffSurround=True, mode = 'full')
    
    fig = plt.figure()
    ax = plt.subplot(111)
    
    print(orVal.shape)
    
    for i in range(0, orImgs.shape[2]):
        
        #print(orVal)
        ax.cla()
        ax.imshow(orImgs[:,:,i])    
        plt.show()
        plt.pause(.01) 
        
def TestSimpleBatteryOR_sin():
    
    
    orData, orImgs, orVal = SimpleBatteryOR_sin([50,50], widths=[3,5,6], deg=24, cycle=10, onoffSurround=False)
    
    fig = plt.figure()
    ax = plt.subplot(111)
    
    print(orVal.shape)
    
    for i in range(0, orImgs.shape[2]):
        
        #print(orVal)
        ax.cla()
        ax.imshow(orImgs[:,:,i])    
        plt.show()
        plt.pause(.01)
        
#TestSimpleBatteryOR_sin()
        
#TestSimpleBatteryOR()