# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 15:23:43 2018

@author: James Ryland

In this file we will be exploring models that combine aspects of cortical
amplification and aspect of GCAL

Observational Notes:
    



"""

import tensorflow as tf
import numpy as np
import scipy as sp
import scipy.misc as sm
import Ry_SimpleStim as ss
import Ry_TF_Util as tfu
import Ry_MathUtil as mu
import Ry_AxonGame as ag


def GainControlLayer(s, imShape, std = 6, actStr=[.2, .6], k=.11 ):
    # This layer type is used in contructing a GCAL model from
    # a lissom like model. This model uses divisive normalization
    # in a gaussian surround. It is typically applied to the input sheet of 
    # neurons. This may or may not be sandwitched between cortical models
    # Importantly this is a non-temporal version
    
    # Half-wave rectify
    s_relu = tf.nn.relu(s)
    
    # Find gaussian smoothing
    s_gau, lFilt = tfu.SurroundConnected( s_relu, imShape, lType='Gaussian', radii = [std, np.ceil(std*3)] )
    
    # Calculate divisive normalization
    s_gc = (s_relu*actStr[0])/(k+s_gau*actStr[1])
    
    return s_gc
    


# Observational notes, the target oRate seems to need to be set based on the
# activity of the network after only a little bit of learning?
# Hand setting this seems somewhat unreasonable from a practicality perspective
         
def HomeoLissomLayer(W_pack, s_winds, imShape, reps = 10, 
                     ds = [1, 4], eBoost=1.0, iBoost=1.0,
                     eBeta=.004, eTarg=.5, eRel=True, cBeta=.004, 
                     oRate=.001, oTarg=.50, oRelTo="unlearn", oBool=False,
                     lRate=0.25, wRatio = 0, l2Rate=0, latStyle = 'fixed', libOut=False):
    
    # theta0  : activity suppression parameter 
    # epsilon : soft-normalization parameter
    
    # eRel  : whether or not epsilon should be relative to input window activity
    # eBeta : speed of running average of for input window length 
    # eTarg : relative target for epsilon (which is the soft normalization parameter) 
    # cBeta : 
    # oRelTo: what statistic theta should be made relative to
    # oRate : theta0 learning rate 
    # oBeta : speed of running average for unit activity
    
    # latStyle: (fixed, Ilearn) choose whether to use fixed lateral connections
    #            or learnable lateral connections
    
    im_ln = np.prod(imShape)
    
    # Calculate adaptivity constraints_________________________________________
    theta0Init = 0
    theta0 = tf.get_variable("theta1", (im_ln,1), initializer= tf.constant_initializer(theta0Init*np.ones((im_ln,1))))
    
    epsilon = tf.get_variable("epsilon", [], initializer= tf.constant_initializer(eTarg))
    
    # Running average stats____________________________________________________
    h_bin_init = .10
    h_bin = tf.get_variable("h_bin", (im_ln,1), initializer= tf.constant_initializer(h_bin_init*np.ones((im_ln,1))))
    
    # used to figure out what the raw levels of activity would be, without thresholding
    h_raw_init = .10
    h_raw = tf.get_variable("h_raw", (im_ln,1), initializer= tf.constant_initializer(h_raw_init*np.ones((im_ln,1))))
    
    # Envirnmental expected window norms
    windNorm_init = .01
    windNorm = tf.get_variable("s_norm_run", (im_ln,1), initializer= tf.constant_initializer(windNorm_init*np.ones((im_ln,1))))
    
    
    # Get first activation of h
    with tf.variable_scope("Input"):
        
        print(epsilon.shape)
        
        # Basic Normalize
        #s_winds_norm = tf.nn.l2_normalize(s_winds, axis = 0, epsilon=epsilon )
        # Soft Normalize
        s_winds_norm = s_winds/(tf.norm(s_winds, axis=0, keepdims=True)+epsilon)
        # Neighborial Normalize
        #s_winds = tf.squeeze(s_winds)
        #s_norm = tf.norm(s_winds, axis=0, keepdims=True)
        #s_norm_smooth, filt = tfu.SurroundConnected(tf.transpose(s_norm,[1,0]), imShape, lType='Parabolic', radii = [ds[1]*2, ds[1]*2], opStr = [1, 1])
        #s_norm_smooth = tf.nn.relu(tf.transpose(s_norm_smooth,[1,0]))
        #gamma = 1.0
        #s_winds_norm = s_winds/(s_norm*(1.0-gamma)+s_norm_smooth*(gamma)+epsilon)
        
        # Find r1 pre-threshold
        r_prethresh = tfu.WindowLinear(W_pack, s_winds_norm)
        
        # Find the biased Cosine 
        r_1 = r_prethresh-theta0

        # Rescale so possible range is from 0-1
        #r_1 = r_1/(1-tf.minimum(theta0, .9))      

        # Apply non-linearity to first activity
        h_1 = tf.nn.relu(r_1)
        
        # Calculate a measure of activity 
        # to make activation target relative too:
        #1. what the activity whould be without theta0 and lateral inhibition
        #2. what the first activity looks like without lateral inhibition
        #3. what the activity would be without theta0 and random weights 
        h_unmod = []
        if oRelTo=="unthresh" or oRelTo==[]:
            h_unmod = tf.nn.relu(r_prethresh)
        elif oRelTo=="first":
            h_unmod = tf.nn.relu(r_1)
        elif oRelTo=="unlearn":
            W_rand = tf.nn.l2_normalize(tf.abs(tf.random_normal( W_pack.shape )), axis = 0)
            r_rand = tfu.WindowLinear(W_rand, s_winds_norm)
            h_unmod = tf.nn.relu(r_rand)
        
    
    # setup s at time step i
    h_i = h_1
    r_i = r_1
    r_all = [r_1]
    h_all = [h_1]
    
    # Choose whether to learn inhibitory connections or leave fixed
    #        fixed:  Neither the exitatory nor inhiborory connections learn
    #       Ilearn:  Inhibitory connections learn (recommend smaller inh window, as more intensive)
    I_pack = []
    I_winds = []
    I_Ind_val = []
    I_up = np.array([[0]])

    if latStyle == 'Ilearn':
        # Create inhibitory connections
        with tf.variable_scope("Inhib"):
            dum, I_pack, I_Ind_val  = tfu.LocallyConnected(h_1, imShape, radius = ds[1], init="ones")

    # define fixed activation stabilization loop
    for i in range(0, reps):
        
        EI_lat =[]
        # lateral strength
        sigma = 1.1 # mildly amplifying            

        # Lateral Dynamics
        with tf.variable_scope("EI_Lateral"+str(i)): 
            
            # Fixed lateral dynamics
            if   latStyle == 'fixed':
                #EI_lat, lFilt = tfu.SurroundConnected(h_i , imShape, lType='Malsburg', radii = [ds[0], ds[1]], opStr = [1, 1])
                
                EI_lat = tfu.GauCenterSurround(h_i , imShape, 1, ds[0], ds[1], eBoost=eBoost, iBoost=iBoost)
                EI_lat = EI_lat * sigma
                
            # Fixed Excitatory / Learnable inhibitory dynamics
            elif latStyle == 'Ilearn':
                # Excite
                E_lat, lFilt = tfu.SurroundConnected(h_i , imShape, lType='Parabolic', radii = [ds[0], ds[1]], opStr = [1, 1])
                # Inhib
                I_winds = tfu.InPack(h_i, I_Ind_val)
                I_lat = tfu.WindowLinear(I_pack, I_winds)*iBoost
                # Conjoin
                EI_lat = E_lat-I_lat
                EI_lat = EI_lat*sigma
            
        # updateMethod
        # Additive 1: Constant non-linear input and compounding lat & thresh (Seems to work really fast)
        # Additive 2: Constant non-linear input and compounding lat          (Works well)
        # Additive 3: Constant linear input and compounding lat              (Slower, but consitent with other papers)
        additive = 3
        if   additive == 1:
            r_i = h_1 + EI_lat - theta0
        elif additive == 2:
            r_i = h_1 + EI_lat
        elif additive == 3:
            r_i = r_1 + EI_lat
        
        # Apply non-linearty
        h_i = tf.nn.relu(r_i)
        
        # Add to ordered list of ops
        r_all.append(r_i) 
        h_all.append(h_i)
    

    # get last activations transpose 
    h_i_T = tf.transpose(h_i)
    
    # define W weights renormalized updates and decide what kind of
    # normalization and weight limits to have
    rType = 'unit'
    wLimit=0
    if wRatio!=0:
        wLimit = 1/np.sqrt(W_pack.get_shape().as_list()[0]*wRatio)
    W_up = tfu.assign_addRen(W_pack, h_i_T*tf.squeeze(s_winds_norm), lRate, rType=rType,
                             limit=wLimit, l2Rate=l2Rate )
    
    # If inhibitory lateral connections are learnable       
    if latStyle=="Ilearn":
        #I_winds_norm = tf.nn.l2_normalize(I_winds, axis = 0 )
        iRate = lRate*.01
        I_up = tfu.assign_addRen( I_pack, h_i_T*tf.squeeze(I_winds), iRate, rType="sum" )
    
    
    # Update running average, either of activity or binary on-off activation
    h_i_bin = h_i
    if oBool:
        h_i_bin = tf.to_float(h_i>0)
        h_unmod = tf.to_float(h_unmod>0)
    
    # Choose appropriate running average beta for the unmod estimate   
    rBeta = .1*cBeta
    if oRelTo=='unthresh':
        rBeta = cBeta
    
    # Update running averages of raw actual activations and unmod activations
    h_bin_up = tf.assign(h_bin,  (1.0-cBeta)*h_bin + (cBeta)*h_i_bin )
    h_raw_up = tf.assign(h_raw,  (1.0-rBeta)*h_raw + (rBeta)*h_unmod )
    
    # Update the running average of input window activation vector lengths
    # Only where those lengths are greater than 0
    windNorm_c = tf.reshape(tf.sqrt(tf.reduce_sum( tf.square(s_winds), axis= 0)), [im_ln,1] )
    sw = tf.to_float(windNorm_c>0)
    windNorm_c = windNorm_c*(sw)+windNorm*(1-sw)
    windNorm_up = tf.assign(windNorm,  (1.0-eBeta)*windNorm + (eBeta)*windNorm_c )
    
    # Decide whether to make the activity-target relative to something
    oTargRel = oTarg
    if not oRelTo==[]:
        oTargRel = tf.reduce_mean(h_raw_up)*oTarg

    # Decide whether to make input normalization epsilon relative to the
    # average input length
    eTargRel = eTarg
    if eRel:
        eTargRel = eTarg*tf.reduce_mean(windNorm)
    
    # Update the homeostatic parameters
    lbCut1 = -.8 
    theta0_up  = tf.assign(theta0  , tf.maximum( theta0  + oRate*(h_bin-oTargRel), lbCut1 ) ) 
    epsilon_up = tf.assign(epsilon , tf.maximum( epsilon - oRate*(epsilon-eTargRel), .00000001 ) ) 
    
    # combine updates into learnig op
    learn    = W_up[0,0] + I_up[0,0] + h_bin_up[0,0] + h_raw_up[0,0] + windNorm_up[0,0] + theta0_up[0,0] + epsilon_up
    preLearn = W_up[0,0] + I_up[0,0] + h_bin_up[0,0] + h_raw_up[0,0] + windNorm_up[0,0] + epsilon_up   

    adapt_all = [ h_bin, theta0, h_raw, oTargRel, windNorm, epsilon]

    # Return important values
    if not libOut:
        return r_all, h_all, learn, adapt_all 
        
    else:
        lib = {"r_all":r_all, "h_all":h_all, "learn":learn, "preLearn":preLearn,
               "h_bin":h_bin, "h_raw":h_raw, "oTargRel":oTargRel, 
               "windNorm":windNorm, "epsilon":epsilon, "sig_all":adapt_all}
        return lib
  
    
    
import matplotlib.pyplot as plt 
import matplotlib.colors as col
from scipy import ndimage as nd
from PIL import Image
import Ry_ImagePost as ip

class TopoFig:
    def __init__(self, sess, s, h, in_ln, imShape, testData, testVals, mapType, title, rez=500, smooth=False, center=False, batchIndFeed=[], input_switch=[], levelSets=0):
        # Setup reference variables
        self.s = s
        self.h = h
        self.stimNum = testData.shape[1]
        self.testData = testData
        self.testVals = testVals
        self.sess = sess
        self.imShape = imShape
        self.in_ln = in_ln
        self.h_ln = np.prod( imShape )
        self.title=title
        self.mapType = mapType
        self.rez = np.round((imShape/np.min(imShape))*rez).astype(int)
        self.smooth = smooth # Decide whether to smooth activity or not
        self.center = center
        self.smSc = 1
        self.selAvgs = [0]
        self.levelSets=levelSets        

        # Setup figure
        self.fig = plt.figure()
        self.ax1 = self.fig.add_subplot(221)
        self.ax2 = self.fig.add_subplot(222)
        self.ax3 = self.fig.add_subplot(223)
        self.ax4 = self.fig.add_subplot(224)
        self.valIm = np.zeros( imShape )
        self.selIm = np.zeros( imShape )
        self.actIm = np.zeros( imShape )
        self.actMeanIm = np.zeros(imShape)
        self.actBuff = np.zeros( (imShape[0], imShape[1])  )
        self.valImRez = np.zeros( rez )
        self.feed_dict = {}
        self.input_switch = input_switch
        self.batchIndFeed = batchIndFeed
        

    def refresh(self):
        
        self.runBattery()
        #self.plotValsGrey(self.ax1, self.actIm, False, "Activity Sum")
        self.plotValueCycle(self.ax1, "Value Cycle")
        #self.plotValsGrey(self.ax2, self.selIm/np.max(self.selIm), False, "Selectivity")
        self.plotSelAvgs(self.ax2, "Selectivity over Time")
        cIm = []
        if self.mapType=='linear':
            cIm = self.plotValsGrey(self.ax3, self.valIm, False, self.title)
            cIm = self.plotValsGrey(self.ax4, self.valIm, True, self.title)
        elif self.mapType=='circ':
            cIm = self.plotValsCirc(self.ax3, self.valIm, False, self.title)
            cIm = self.plotValsCirc(self.ax4, self.valIm, True,  self.title)
        self.fig.canvas.draw()
        
        return self.valIm, cIm
    
    def plotSelAvgs(self, ax, title ):
        
        ax.cla()        
        ax.set_title(title)    
        ax.plot(self.selAvgs)
        
        
    def plotValueCycle(self, ax, title, mode = 'unique'):
        
        # Find the unique values in the test vals
        uniqueVals = np.unique( self.testVals )
        
        # find the summed activity of responese for each unique value
        valActMeans = np.zeros( (self.imShape[0], self.imShape[1], uniqueVals.shape[0]) )
        
        # Loop over each value, and sum over all respones for that value
        for i in range(0, uniqueVals.shape[0]):
            mask = (self.testVals==uniqueVals[i])
            valActMeans[:,:,i] = np.mean( self.actBuff[:,:,mask], axis=2)
           
        valActMeansNormed = valActMeans-self.actMeanIm[:,:,np.newaxis]  
        
        if self.center:
            valActMeans = valActMeansNormed

        #valActMeansNormed = (valActMeansNormed-np.min(valActMeansNormed))/(np.max(valActMeansNormed)-np.min( valActMeansNormed ))
        
        # Display the fruits of our labor
        dispIm = np.zeros( (1, self.imShape[1]*2) )
        
        for i in range(0, uniqueVals.shape[0]-1,2):
            valActIm1 = valActMeans[:,:,i]
            valActIm1 = (valActIm1-np.min(valActIm1))/(np.max(valActIm1)-np.min(valActIm1))
            valActIm2 = valActMeans[:,:,i+1]
            valActIm2 = (valActIm2-np.min(valActIm2))/(np.max(valActIm2)-np.min(valActIm2))
            row_i = np.concatenate( ( valActIm1, valActIm2), axis=1 )
            dispIm = np.concatenate( (dispIm, row_i), axis=0 )
            
        ax.cla()        
        ax.set_title(title)        
        ax.imshow(dispIm[:,:,np.newaxis]*np.ones((1,1,3)))
        ax.axis('off')  # clear x- and y-ticks
        
            
    def runBattery(self):
        
        h_buff = np.zeros( (self.h_ln, self.stimNum) )
        
        if self.batchIndFeed==[]:
            # Run through stimulus battery feeding each item to the model
            for i in range(0, self.stimNum):
                # set s element of feed dictionary
                self.feed_dict[self.s] = self.testData[:,i,np.newaxis]
                # Run the input item and collect in activity buffer
                h_val, dum = self.sess.run([self.h, self.input_switch], feed_dict = self.feed_dict )
                h_buff[:,i] = np.squeeze(h_val)
        else:
            # Run through an IndexedBatch already shipped to the model
            for i in range(0, self.stimNum):
                # set the index for the input currently in the batch
                self.feed_dict[self.batchIndFeed]=i
                # Run the input item and collect in activity buffer
                h_val, dum = self.sess.run([self.h, self.input_switch], feed_dict = self.feed_dict )
                h_buff[:,i] = np.squeeze(h_val)
            
                
        # Smooth activation
        if self.smooth:
            imBuff = self.buff_vecToIm(h_buff)
            imBuffSm = nd.gaussian_filter(imBuff, (self.smSc, self.smSc, 0), order=0 )
            h_buff = self.buff_imToVec(imBuffSm)
        
            
        # find indices with maximum value along rows
        avgVal = []
        if self.mapType=='linear':
            avgVal = np.sum( h_buff * np.transpose(self.testVals), axis=1)/(np.sum(h_buff, axis=1)+.0000001)
            avgVal = (avgVal-np.min(avgVal))/(np.max(avgVal)-np.min(avgVal)+.00001)
        elif self.mapType=='circ':
            avgVal, mag = mu.circMean(np.transpose(self.testVals), h_buff, axis=1)
            self.selIm = np.reshape( mag , self.imShape ,order='F')
            self.selAvgs.append(np.mean(mag) )
        # reshape into cortical image
        self.valIm = np.reshape( avgVal , self.imShape ,order='F')

    
        
        #Get activity heatmap
        actMean = np.mean(h_buff, axis=1)
        actVec = (actMean-np.min(actMean))/(np.max(actMean)-np.min(actMean))
        self.actIm = np.reshape(actVec, self.imShape, order='F')
        self.actMeanIm = np.reshape(actMean, self.imShape, order='F')
        self.actBuff = self.buff_vecToIm(h_buff)
        
    def buff_vecToIm(self,buff):
        return np.reshape( buff, (self.imShape[0], self.imShape[1], buff.shape[1]) ,order='F')
    
    def buff_imToVec(self,buff): 
        return np.reshape( buff, (self.imShape[0]*self.imShape[1], buff.shape[2]) ,order='F')
        
    def plotValsGrey(self, ax, valIm, upRez, title):
        # Expecting values from [0-1]
        
        vIm = valIm
        if upRez:    
            vIm = sm.imresize(valIm, size=self.rez)
            
        vIm = vIm[:,:,np.newaxis]*np.ones((1,1,3))
        
        ax.cla()        
        ax.set_title(title)        
        ax.imshow()
        ax.axis('off')  # clear x- and y-ticks
        return vIm
        
    def plotValsCirc(self, ax, valIm, upRez, title):
        # Expecting values from [0-1] - mapping to 'hsv'
        hsvIm = np.ones( (self.imShape[0], self.imShape[1], 3) )
        hsvIm[:,:,0] = valIm
        cicIm = col.hsv_to_rgb(hsvIm)

        if upRez:  
            cicIm = sm.imresize(cicIm, size=self.rez, interp='cubic')
            sigma = self.rez[0]/self.imShape[0]/2
            cicIm = ip.imBlurSharp(cicIm, sigma)
            if self.levelSets>0:
                cicIm = ip.levelSets(cicIm, self.levelSets)
                cicIm = ip.imAA(cicIm,sigma)
            
        ax.cla()        
        ax.set_title(title)        
        ax.imshow(cicIm)
        ax.axis('off')  # clear x- and y-ticks
        return cicIm

    
class TopoActFig:
    def __init__(self,imShape, title1, title2):
        # Setup reference variables
        self.imShape = imShape
        self.fig = plt.figure()
        self.ax1 = self.fig.add_subplot(221)
        self.ax2 = self.fig.add_subplot(222)
        self.ax3 = self.fig.add_subplot(223)
        self.ax4 = self.fig.add_subplot(224)
        self.title1 = title1
        self.title2 = title2
        
    def refresh(self, act1,act2):
        self.plotAct(self.ax1, act1, self.title1)
        self.plotAct(self.ax2, act2, self.title2)
        self.plotActHist(self.ax3, act1, self.title1)
        self.plotActHist(self.ax4, act2, self.title2)
        self.fig.canvas.draw()
        
        
    def plotAct(self, ax, act, title):
        
        # Expected range from 0 to 1
        actIm = np.reshape(act, self.imShape, order='F')
        actIm = (actIm-np.min(actIm))/(np.max(actIm)-np.min(actIm)+.00000001)
        actIm = actIm[:,:,np.newaxis]*np.ones((1,1,3))
        ax.cla()        
        ax.set_title(title)        
        ax.imshow(actIm)
        ax.axis('off')  # clear x- and y-ticks

    def plotActHist(self, ax, act, title):
        
        if np.max(act)>0:
            act=act[act>0]
        ax.cla()        
        ax.set_title(title+" histogram")        
        ax.hist(np.ravel(act, order='F'), bins=50, range=(0, 2), color='red' )

class TopoFeatureFig:
    def __init__(self,inShape, outShape, title):
        # Setup reference variables
        self.inShape = inShape
        self.in_ln = np.prod(inShape)
        self.outShape = outShape
        self.out_ln = np.prod(outShape)
        self.title = title
        
        # Randomly sample 9 features     
        self.dWidth = 3
        self.dNum = self.dWidth*self.dWidth
        self.featInds = np.random.choice( self.out_ln, size=[self.dNum])
        
        # Setup storage for feature structures
        self.W_pack =[]
        self.Ind_pack = []

        # Setup plot
        self.fig = plt.figure()
        self.ax1 = self.fig.add_subplot(111)
        
    def refresh(self, W_pack, Ind_pack, order='F'):
        self.W_pack = W_pack*(Ind_pack>0)
        self.Ind_pack = Ind_pack
        self.plotFmatrix(self.ax1,self.title,order=order)
        self.fig.canvas.draw()
        
    def plotFmatrix(self, ax, title,order='F'):
        
        dispIm = np.zeros( (self.inShape[0]*self.dWidth, self.inShape[1]*self.dWidth) )
        
        k = 0
        for i in range(0, self.dWidth):
            
            for j in range(0, self.dWidth):
                
                fIm_ij = self.fImage(k,order=order)
                k = k+1
                a0 = i*self.inShape[0]
                a1 = (i+1)*self.inShape[0]
                b0 = j*self.inShape[1]
                b1 = (j+1)*self.inShape[1]
                
                dispIm[a0:a1, b0:b1] = fIm_ij
        
        dispIm = (dispIm-np.min(dispIm))/((np.max(dispIm)-np.min(dispIm))+.00000001)
        dispIm = dispIm[:,:,np.newaxis]*np.ones((1,1,3))
        ax.cla()        
        ax.set_title(title)        
        ax.imshow(dispIm)
        ax.axis('off')  # clear x- and y-ticks

    def fImage(self, col, order='F'):
        # Create an image of a feature within the input image landscape
        fVec = np.zeros( (self.in_ln+1)  )
        
        # Fill input vector space with weights
        colInds = self.featInds[col]
        fVec[self.Ind_pack[:,colInds]] = self.W_pack[:,colInds]
        
        # Remove 0 pad
        fVec = fVec[1:self.in_ln+1]

        # Reshape into an image
        fIm = np.reshape(fVec, self.inShape, order=order)
        fIm = fIm/(np.max(np.abs(fIm))+.000001)
        
        return fIm
        

# This class creates a topographic map based on
# input values and learned weights (assumed to be positive)
class TopoByWeight:
    
    def __init__(self, cShape):
        self.cShape = cShape
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(1,1,1)
        
    def refresh(self, W_pack, Ind_pack, valVec, cmap='gray'):

        # Add first element pad
        valVec = np.concatenate([[0], np.squeeze(valVec) ])
        
        # Find the weighted average value
        weighted = (valVec[Ind_pack]*W_pack)
        cVec = np.sum(weighted,axis=0)/np.sum(W_pack, axis=0)
        
        # Convert to image shape
        valImg = np.reshape(cVec, self.cShape, order='F')
        
        # Graph the topographic map
        self.ax.imshow(valImg, cmap=cmap)
        
        # Return the value image for other operations
        return valImg
        
    


def MaxNormStim(stim):
    # use the maximum value to normalize the input
    return stim/(np.max(stim,axis=0,keepdims=True)+.000001)

def UnitNormStim(stim):
    # use the maximum value to normalize the input
    return stim/(np.linalg.norm(stim,axis=0,keepdims=True)+.000001)
    
    
#______________________________________________________________________________
#______________________________________________________________________________
#______________________________________________________________________________
#___________________TEST_FUNCTIONS______________________________________________
#______________________________________________________________________________
#______________________________________________________________________________
#______________________________________________________________________________

import time

def test_Models(testModel = 'HomeoLissom', GC=True, A=False, testType = 'learn'):
    
    imShape = [100,100]
    in_ln = np.prod(imShape)
    swWidth = .5
    onOff =  True#True
    gauNum = 3
    
    print("Setting Up Simulation:")
    
    # Make on-off stimulus wave generator
    eye = np.zeros(in_ln)
    #acts, imgs = ss.SimpleBoxSweeps(imShape, 200, imShape[0], swWidth, onoffSuround=onOff)

    # Remove blank images from training data
    #nonBlank = np.max(acts, axis=0)>.01
    #acts = acts[:,nonBlank]
    #imgs = imgs[:,:,nonBlank]
    
    
    #acts, imgs, eye = ss.SimpleBiSweeps(imShape, 200, 50, swWidth, onoffSuround=onOff)
    
    # Make orientation test battery
    
    print("Creating OR Battery")
    # Making the orientation battery is a huge bottle neck!!
    orData, orImgs, orVal = ss.SimpleBatteryOR(imShape, swWidth, deg=6, gap=36, onoffSurround=onOff)
    #orData, orImgs, orVal = ss.SimpleBatteryOR_sin(imShape, widths=[4,5,6], deg=12, cycle=12, onoffSurround=onOff)
    
    # Make ocularity test battery
    print("Creating OC Battery")
    ocData, ocVal = ss.SimpleBatteryOC(eye)
    
    # Scale and adjust inputs
    scaling = 1#4  # 4/sp:ones
    #acts = MaxNormStim(acts)*scaling
    orData = MaxNormStim(orData)*scaling
    ocData = MaxNormStim(ocData)*scaling
    
    # Cleanup graph
    tf.reset_default_graph()
    
    inRadius = 8
    
    # Define Hard coded Locally Connected Input layer  Fovea->Cort
    with tf.variable_scope("in"):
        s = tf.placeholder(dtype = tf.float32, shape=[in_ln, 1])
        s_gc = s
        if GC:
            print("Gain Control")
            s_gc = GainControlLayer(s, imShape)
        # Define basic locally connected layer
        print("Input Locally Connected")
        s_winds, W_pack, Ind_val = tfu.LocallyConnected(s_gc, imShape, inRadius, wShape='disk', init="uniform", initDict={'N':10, 'STD':.01})

    # Define ops that need to be displayed
    r_1 = []
    r_2 = []
    r_k = []    
    h_1 = []
    h_2 = []
    h_k = []
    h_all = []
    r_all = []
    learn = []     
    sig_all = []    

    print("Test Model: "+testModel)

    if testModel=='HomeoLissom':
    
        # Define Lissom layer
        with tf.variable_scope("protolis"):
            r_all, h_all, learn, sig_all = HomeoLissomLayer(W_pack, s_winds, imShape,
                                                            reps = 15, ds = [1, 8.0], eBoost=1, iBoost=2,
                                                            eBeta=.004, eTarg=.005, eRel=True, cBeta=.004, 
                                                            oRate=.001, oTarg=.5, oRelTo="unthresh", oBool=False,
                                                            lRate=0.15, wRatio = 1/5, l2Rate=0.5, latStyle = 'fixed')
            r_1 = r_all[0]
            r_2 = r_all[1]
            r_k = r_all[-1]    
            h_1 = h_all[0]
            h_2 = h_all[1]
            h_k = h_all[-1]    

    

    print("Done Beginning Simulation")

    # setup tensor flow session
    with tf.Session() as sess:    
        
        # Inittialize tf variables
        sess.run(tf.global_variables_initializer())    
        
        # Create display for lissom layer
        figAct = TopoActFig(imShape, "1st Activity", "k-th Activity")
        #figIn = TopoActFig(imShape, "In", "In Gain Control")
        figOR = TopoFig(sess, s, h_k, in_ln, imShape, orData, orVal, 'circ', "Orientation Columns", smooth=False)
        #figOC = TopoFig(sess, s, r_1, in_ln, imShape, ocData, ocVal, 'linear', "Ocular Dominance")
        
        figF = TopoFeatureFig(imShape, imShape, 'Feature Plot')
        
        s_val = []
        h_1_val = []
        h_k_val = []


        if testType=='input':
            # Quick test of neighborhood for input transform
            for i in range(0, 300):
                print(i)
                pixelInd = np.random.choice(acts.shape[0])
                s_val = np.zeros((in_ln,1) )
                s_val[pixelInd] = 1
                feed_dict={s:s_val}
                # Display activation of random pixels neighborhood
                h_1_val, h_k_val = sess.run([h_1, h_k], feed_dict=feed_dict)
                figAct.refresh(h_1_val,h_k_val)    
                plt.pause(1)
            
        else:
            # Main Loop for training test
            # Insure that all values are normalized before running
            sess.run([learn], feed_dict={s:np.zeros((in_ln,1))})
            epochs = 500
            epochSteps = 100
            start = time.time()
            for i in range(0, epochs):
                
                # Training Loop
                for j in range(0,epochSteps):
                    
                    # Get the training stimulus
                    #s_val = acts[:,np.random.choice(acts.shape[1]), np.newaxis]
                    
                    # Generate training stimulus
                    s_val, im = ss.SimpleGaussianGenerator(imShape, gauNum=gauNum, onoff = onOff, sizes=[.25,8])
                    #s_val = s_val*scaling/(np.max(s_val)+.0001)
                    #s_val = np.zeros_like(s_val)
                    
                    # Setup the feed dictionary             
                    feed_dict={s:s_val}
                                 
                    # Show 
                    W_pack_val, s_gc_val, r_1_val, r_2_val, h_1_val, h_2_val, h_k_val, sig0_val, sig1_val, sig2_val, sig3_val, sig4_val, sig5_val, dum = sess.run([W_pack, s_gc, r_1, r_2, h_1, h_2, h_k, sig_all[0], sig_all[1], sig_all[2], sig_all[3], sig_all[4], sig_all[5], learn], feed_dict=feed_dict)
                
                    
                mins = int( np.floor((time.time()-start)/60))
                secs = int( np.floor((time.time()-start)-mins*60))
                
                print("\n_________________________"+testModel)
                print("Time        : "+str(mins)+"m  "+str(secs)+"s")
                print("Epoch       : "+str(i))
                print("Step        : "+str(i*epochSteps))
                print("---")
                print("Max s_gc    : "+str(np.max(s_gc_val)) )
                print("---")
                print("Avg  h_1    : "+str(np.round(np.mean(h_1_val),3 ))  )
                print("Span h_1    : ["+str(np.round(np.min(h_1_val),3 )) +', '+ str(np.round(np.max(h_1_val),3 ))  +']' )
                print("---")
                print("Avg  h_k    : "+str(np.round(np.mean(h_k_val),3 ))  )
                print("Span h_k    : ["+str(np.round(np.min(h_k_val),3 )) +', '+ str(np.round(np.max(h_k_val),3 ))  +']' )
                print("---")
                print("Run h_k     : "+str(np.round(np.mean(sig0_val),3 )))
                print("Theat0 Val  : "+str(np.round(np.mean(sig1_val),3)))
                print("Run h_un    : "+str(np.round(np.mean(sig2_val),3)))
                print("Rel oTarg   : "+str(np.round(np.mean(sig3_val),3)))
                print("Run windNorm: "+str(np.round(np.mean(sig4_val),3)))
                print("Epsilon     : "+str(np.round(np.mean(sig5_val),3)))
                
                
                print("---")
                print("Span W      : ["+str(np.round(np.min(W_pack_val),3 )) +', '+ str(np.round(np.max(W_pack_val),3 ))  +']' )
                print("Dist W      : avg="+str(np.round(np.mean(W_pack_val),4))+", std="+str(np.round(np.std(W_pack_val), 4)))
                print("---")
                print("Selectivity : "+str(np.round(np.mean(figOR.selIm),4)))
                
                    
                
                # Display (/) Run Test battery
                figAct.refresh(h_1_val,h_k_val)
                #figIn.refresh(s_val,s_gc_val)
                figOR.refresh()
                #figOC.refresh()
                figF.refresh(W_pack_val, Ind_val)
                plt.pause(.1)
            
            
            
#test_Models(testModel = 'HomeoLissom', GC=False, testType = 'learn')                


