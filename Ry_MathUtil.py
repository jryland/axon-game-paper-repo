# -*- coding: utf-8 -*-
"""
Created on Tue Sep 18 15:22:18 2018

@author: jwr071000
"""

import numpy as np


# Some simple functions for dealing with polar coordinate conversions
# Source stack exchange
def cart2pol(x, y):
    rho = np.sqrt(x**2 + y**2)
    phi = np.arctan2(y, x)
    return(rho, phi)

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return(x, y)
    
def randomDir2D():
    
    return pol2cart(1, np.random.uniform()*2*np.pi)

def circMean( values, weights, axis, keepdims=True,  inter=[0,1]):
    # Calculate the mean direction on a circle
    
    # Scale values to span the circle
    values = (values-inter[0])/(inter[1]-inter[0])
    values = values*np.pi*2
    
    # Convert to cartesian coords
    x, y = pol2cart(1, values )
    
    # Either lose negative values
    weights = np.maximum(weights,0)
    
    # Add minimum
    #weights = weights+np.min(weights,0)
    
    # Find mean direction
    xm = np.sum(x*weights, axis=axis,keepdims=keepdims)/(np.sum(weights, axis=axis, keepdims=keepdims)+.00000000001)
    ym = np.sum(y*weights, axis=axis,keepdims=keepdims)/(np.sum(weights, axis=axis, keepdims=keepdims)+.00000000001) 
    
    # Convert back to "mean" theta
    r, theta = cart2pol(xm, ym)
    
    # Scale theta to original span
    mVal = theta/(np.pi*2)+.5
    mVal = mVal*(inter[1]-inter[0])+inter[0]
    
    return mVal, r
    

