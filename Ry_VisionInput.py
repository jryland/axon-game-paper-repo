# -*- coding: utf-8 -*-
"""
Created on Mon Oct 29 17:38:15 2018

@author: jwr071000



This set of functions has several purposes:
    1. Generate Spontanious input patterns using TF
    2. Process raw input patterns using TF
    3. Put the processed patterns into a format that ICL can use
    4. May start with grey scale only
    
Bio-Inspiration:
    1. Retina, multi-scale foveation
    2. On-Center/Off-Center; Off-Center/On-Center cells
    3. Lateral Geniculate Nucleaus (Opponent color)
    

"""

import numpy as np
import tensorflow as tf
import matplotlib.pylab as plt
import time

def GaussianGenerator(shape, gauNum, aspect = [4,8], scaleRange = [.5,8], foveateCenters=False):
    """ This function creates a tensor flow op that returns an image with 
    randomly placed/shaped gaussians, this sould in theory be much faster than 
    generating spontanious stimuli on the CPU side"""
    print("WIP")
    
    
    xticks = np.linspace(-shape[0]/2,shape[0]/2, shape[0])
    yticks = np.linspace(-shape[1]/2,shape[1]/2, shape[1])
    
    xv, yv = np.meshgrid(xticks, yticks, sparse=False, indexing='ij')
    
    z = np.concatenate( (xv[:,:,np.newaxis], yv[:,:,np.newaxis]),  axis=2)
    
    im = np.zeros(shape)
    
    C = np.diag(aspect/np.linalg.norm(aspect))
    C = C.astype(np.float32)
    
    
    
    for i in range(0,gauNum):
        
        
        # Create random 2D rotation matrix
        ortho = np.array([[0,1],[-1,0]]).astype(np.float32)
        vDir = tf.nn.l2_normalize( tf.random_normal([1,2] ), axis=1 )
        vOrtho = tf.matmul(vDir, ortho)
        R = tf.concat((vDir, vOrtho), axis=0 )
        
        # Pick scale according to range
        sc = []
        cent = []
        if not foveateCenters:
            a0 = np.log(np.sqrt(scaleRange[0]))
            a1 = np.log(np.sqrt(scaleRange[1]))
            sc = np.square(tf.exp(tf.random_uniform([], a0, a1)))
            cent = tf.random_uniform([1,1,2], np.min(xticks)*1.33, np.max(xticks)*1.33)
        else:
            sc = tf.random_uniform([], scaleRange[0], scaleRange[1])
            mod = sc/scaleRange[1]
            cent = tf.random_uniform([1,1,2], np.min(xticks)*1.33*mod, np.max(xticks)*1.33*mod)
            
        
        # Rotate covariance matrix
        C_rot = tf.matmul(C/sc, R)
        C2 = tf.matmul(tf.transpose(C_rot),C_rot)

        # Compute gaussain value for all pixels
        z_cent = z-cent
        # Tensor op Z_xyi * C_ij * Z_xyj = G_xy 
        B = tf.reduce_sum(z_cent[:,:,:,tf.newaxis]*C2[tf.newaxis,tf.newaxis,:,:], axis=2 )
        # Tensor op B_xyj * Z_xyj = G_xy
        D = tf.reduce_sum(B*z_cent, axis = 2)
        G = tf.exp(-tf.abs(D))
        
        # Normalize and give sign
        G = G*(sc*sc/scaleRange[1]/scaleRange[1])
        
        G = G * tf.sign( tf.random_uniform([], -5, 5) )
        
        im = im+G
    
    return im

def ApplyOnOff(grey, shape, width=1, pad = 'reflect', cMap=[], gain=False):
    """Filter inputs with a LoG filter and half-wave rectify both ways, return
    conjoined sets of outputs. This is designed to mimich the onoff and offon
    cells in the retinal ganglia / Lateral Geniculate Nucleus. An included 
    chemo affinity map will be updated to reflect the doubled number of units"""

    
    logFilt = LOGkernel(width)

    
    
    pS = np.floor(logFilt.shape[0]/2).astype(int)
    padSizes = tf.constant([[pS,pS], [pS,pS]])
    grey_pad = tf.pad(grey, padSizes, pad)
    
    # Translate the image vector into the shape used by tf.nn.conv2d
    # batch width hight channel
    grey_nwhc = tf.reshape(grey_pad,(1, shape[0]+pS*2, shape[1]+pS*2, 1), name='grey_nwhc')
    # width hight inChan outChan
    d1 = logFilt.shape[0]
    logFilt = np.reshape(logFilt, (d1,d1,1,1), order='F')
    
    
    # Perform the convolution
    grey_nwhc = tf.nn.conv2d(grey_nwhc, logFilt, (1,1,1,1), "VALID")
    
    # Put back into vector form
    r = tf.reshape(grey_nwhc,(shape[0],shape[1]), name='OnOff')

    onoff = tf.nn.relu(r)
    offon = tf.nn.relu(-r)
    
    # ADD GAIN CONTROLL OPTION HERE
    if gain:
        onoff = ImageGainControl(onoff, shape)   
        offon = ImageGainControl(offon, shape) 
    
    both = tf.concat((onoff,offon), axis = 1 )
    
    newShape = shape
    newShape[1] = newShape[1]*2
    
    if cMap==[]:
        return both
    else:
        cMapBoth = np.concatenate( (cMap,cMap), axis = 1 )
        return both, cMapBoth, newShape
    

def ImageGainControl(im, imShape, std = 6, actStr=[.2, .6], k=.11, pad = 'SYMMETRIC' ):
    
    radii = [std, np.ceil(std*3)]
    
    # Setup descriptive dimensions
    d1 = np.ceil(radii[1]*2+1).astype(int)

    
    # create offset images for constructing filter
    Yoff, Xoff = np.meshgrid(np.linspace(-radii[1],radii[1],d1), np.linspace(-radii[1],radii[1],d1), indexing='ij')

    std = radii[0]
    gau = np.exp( - (np.power(Xoff,2)+np.power(Yoff,2)) /(2*std*std) )
    gau = gau/np.sum(gau)
    gFilt = gau
    
    im_relu = tf.nn.relu(im)
    
    # Create padding
    pS = np.round(radii[1]).astype(int)
    padSizes = tf.constant([[pS,pS], [pS,pS]])
    im_pad = tf.pad(im_relu, padSizes, pad)
    
    im_nwhc = tf.reshape(im_pad,(1, imShape[0]+pS*2, imShape[1]+pS*2, 1), name='im_nwhc')
    
    
    gFilt = np.reshape(gFilt, (d1,d1,1,1), order='F')

    # Perform the convolution
    im_gau_nwhc = tf.nn.conv2d(im_nwhc, gFilt, (1,1,1,1), "VALID")
    
    im_gau = tf.reshape(im_gau_nwhc, imShape)
    
    # Calculate divisive normalization
    im_gc = (im_relu*actStr[0])/(k+im_gau*actStr[1])
    
    return im_gc
    

def LOGkernel(scale, clip=2):
    # Go 4 standard deviations out
    sz = np.ceil(scale).astype(int)*(clip*2)+1
    xticks = np.linspace(-sz/2,sz/2, sz)
    yticks = np.linspace(-sz/2,sz/2, sz)
    
    x, y = np.meshgrid(xticks, yticks, sparse=False, indexing='ij')
    
    sc = scale
    
    # Equation for the LoG function 
    logKernel = - 1/(np.pi*np.power(sc,4)) * (1-(x*x+y*y)/(2*sc*sc)) * np.exp(-(x*x+y*y)/(2*sc*sc)) 
    
    return logKernel
    
def testLOG():

    fig = plt.figure()
    ax = plt.subplot(111)
        
    im = LOGkernel(2)
            
    im = im-np.min(im)/(np.max(im)-np.min(im))
            
    ax.cla()
    ax.imshow(im)    
    plt.show()

#testLOG()    
    
def TestGaussianGenerator():
    
    shape = [200,200]
    
    tf.reset_default_graph()
    
    
    im = GaussianGenerator( shape, 16 )
    both = ApplyOnOff(im, shape, gain=True)
    o_sum = tf.reduce_sum(both)
    
    with tf.Session() as sess:        
        sess.run(tf.global_variables_initializer())
    
        # Time stimulus generation
        start = time.time()
        numStim = 1000
        for i in range(0, numStim):
            sess.run(o_sum)
        stop = time.time()
        inter = stop-start
        
        print("Generating "+str(numStim)+" Stimuli: "+str(np.round(inter,3))+"sec")
        print("\n   "+str(numStim/inter)+" Stimuli Per Second")
        
    
        fig = plt.figure()
        ax = plt.subplot(111)
        
        for i in range(0, 1000):
            
            im_val = sess.run(both)
            
            im_val = im_val-np.min(im_val)/(np.max(im_val)-np.min(im_val))
            
            ax.cla()
            ax.imshow(im_val)    
            plt.show()
            plt.pause(.5) 


#TestGaussianGenerator()
    


def ImageInsert():
    """ This function takes a small image and embeds it in a larger image,
    while also scaling the smaller image's size. The (x,y) position will
    probably be a random offset and the scale will probably be a random offset
    with 
    """
    print("WIP")

    
    
def FoveatedRetina():
    """ This function takes a large image as input and processes it foveally, 
    the center of the input image is assumed to be the center of the retina"""
    print("WIP")
    
    # To find scaling constant use this formula
    #B1=B2c^p
    #(B1/B0)^(1/p) = c   
    