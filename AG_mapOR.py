# -*- coding: utf-8 -*-
"""
Created on Wed Jun 26 19:14:01 2019

@author: jwr071000
"""

import numpy as np

import Ry_AxonGame as rag
import Ry_TF_Util as tfu
import tensorflow as tf
import pickle as pkl
import Ry_SimpleCell as rsc
import Ry_SimpleStim as ss
import matplotlib.pyplot as plt
import time


def pickleWindowData(pDict):
    # Store data (serialize)
    with open('AG_OR_windows.pickle', 'wb') as handle:
        pkl.dump(pDict, handle, protocol=pkl.HIGHEST_PROTOCOL)

def unpickleWindowData():
    # Load data (deserialize)
    with open('AG_OR_windows.pickle', 'rb') as handle:
        unserialized_data = pkl.load(handle)
    
    return unserialized_data


# Will need to be called twice
def CreateInputStream(eyeSize, eyePad, std):
    
        
    # Define input generator and processing
    with tf.variable_scope("in"):
        
        # Pad for eye movemtent
        eyeSize_pad = np.array(eyeSize)+eyePad
        
        # Make input placeholder
        vec_sz = [np.prod(eyeSize_pad),1]
        vec_feed = tf.placeholder_with_default(np.zeros(vec_sz,dtype=np.float32), shape=vec_sz)
        
        # reshape//Assume vectorized input image is square...
        # Tot avoid order issues..
        im_feed = tf.reshape(vec_feed, eyeSize_pad)   

        # Make switch placeholder
        feedOn = tf.placeholder(tf.bool, shape=[])
        feedOn_fl = tf.to_float(feedOn)
        
        # Generate random gaussians accross scale
        im_spont = tfu.GaussianGenerator(eyeSize_pad, 32, aspect=[2, 16], scaleRange=[std/2, std*2], foveateCenters=False)
    
        # Use switch to determine whether to use the feed input
        im =  im_spont*(1-feedOn_fl) + im_feed*feedOn_fl
        
        # Add batch and channel dimensions
        im = tf.reshape(im, [1, eyeSize_pad[0], eyeSize_pad[1], 1])
        
        im_cent = tfu.Gaussian_Blur(im, std)
        im_surr = tfu.Gaussian_Blur(im, std*2)
        
        # Remove batch and channel dimensions
        im_cent = tf.reshape(im_cent, [eyeSize_pad[0], eyeSize_pad[1]])
        im_surr = tf.reshape(im_surr, [eyeSize_pad[0], eyeSize_pad[1]])
        
        # Create random variance between eyes by changing amplitude
        dog = im_cent-im_surr
        
        # Random shift for LR eyemovement
        dog_L = tf.random_crop(dog, [eyeSize[0], eyeSize[1]])
        dog_R = tf.random_crop(dog, [eyeSize[0], eyeSize[1]])
        
        # If feed is on, don't shift the images
        # Just grap upper left
        padHalf = int(eyePad/2)
        dog_L_no = dog[padHalf:-padHalf,padHalf:-padHalf]
        dog_R_no = dog[padHalf:-padHalf,padHalf:-padHalf]

        # Choose shifted or unshifted
        dog_L = dog_L*(1-feedOn_fl) + dog_L_no*(feedOn_fl)
        dog_R = dog_R*(1-feedOn_fl) + dog_R_no*(feedOn_fl)
        
        # Apply on off coding
        dog_onoff_L = tf.nn.relu( dog_L)
        dog_offon_L = tf.nn.relu(-dog_L)
        dog_onoff_R = tf.nn.relu( dog_R)
        dog_offon_R = tf.nn.relu(-dog_R)
        
        inputLS = [dog_onoff_L,dog_offon_L,dog_onoff_R,dog_offon_R]

        ones = np.ones( eyeSize )
        eyeMapLS = [ones*0,ones*0,ones,ones]
        
        # On-Center and Off-Center cells
        inputIm = tf.concat( inputLS, axis=1 )
        
        # Make parallel map of which eye the inputs are comming from 
        eyeIm = np.concatenate( eyeMapLS, axis=1)
        
        # Findout what the eventual input vector length will be
        in_ln = eyeSize[0]*eyeSize[1]*4
        
        # Reshape into input vectors
        inputVec = tf.reshape(inputIm, [in_ln,1])
        eyeVec = np.reshape(eyeIm, [in_ln])
        
    chemMap1 = rag.AreaChemMaker(eyeSize, aType='rectangle-no-bound')
    chemMap = np.tile(chemMap1, [1,4,1])
    # Correct for the fact that we use the 'C' ravel order for this
    chemMap = np.reshape(chemMap, [in_ln,1,3], order='C') 
    
    inputImShape = [eyeSize[0], eyeSize[1]*4]
    
    return inputVec, in_ln, inputIm, inputImShape, vec_feed, feedOn, chemMap, eyeVec 
        
def test_CreateInputStream():
    
    tf.reset_default_graph()
    
    eyeSize = [100,100]
    eyePad = 20
    imSize = np.array(eyeSize)+eyePad
    
    s, in_ln, inputIm, inputImShape, im_feed, feedOn, chemMap, eyeVec = CreateInputStream(eyeSize,eyePad, 1)
    
    with tf.Session() as sess:
        
        
        inputIm_val_spont = sess.run(inputIm, feed_dict={feedOn:False})
        
        inputIm_val_feed = sess.run(inputIm, feed_dict={feedOn:True,im_feed:np.random.uniform(size=imSize)})
        
    
    fig = plt.figure()
    
    ax1 = fig.add_subplot(1,3,1)
    ax1.imshow(chemMap)

    ax2 = fig.add_subplot(1,3,2)
    ax2.imshow(inputIm_val_spont)
    
    ax2 = fig.add_subplot(1,3,3)
    ax2.imshow(inputIm_val_feed)
    
#test_CreateInputStream()
     
    
def Train_AxonGame():
    
    eyeSize = [50, 50]
    eyePad = 20
    imSize = np.array(eyeSize)+eyePad   

    plex = 1     # 2, 1, 1
    dRad = 1     # 1, 3, 7 
    dStride = 1
    inSize = eyeSize          
    outSize = np.array([100,100])
    cSize = (outSize/plex).astype(int) 
    out_ln = np.prod(cSize)    
    

    tf.reset_default_graph()
    
    STD = .5 # Was .75 Found ERROR Just made consistent (used to not match post) check if results hold!!!
    s, in_ln, inputIm, inputImShape, vec_feed, feedOn, chemMap1, eyeVec  = CreateInputStream(eyeSize, eyePad, STD)
    
    
    # Target depth to set branch num using radius in input space
    radius = 3.0 # 3 worked// trying larger
    targDepth = np.pi*np.square(radius)*4
    branchNum = int(np.ceil(targDepth*out_ln/in_ln))
    print("branchNum: "+str(branchNum))
    
    # Figure out the axon arber radius on the cortical sheet
    aRad = np.sqrt( np.mean(branchNum)/np.pi )
    

    # Setup output target map
    chemMap2 = rag.AreaChemMaker( cSize, aType="rectangle-no-bound")
    
    
    # DEFINE AXON 
    chemoMult = 1 #.1 
    chemoStr1=.30 * chemoMult
    chemoStr2=.16 * chemoMult
    boundStr = 5
    seniorStrs =  [.001, 1] # senByCo version
    #seniorStrs = [.0005, .0200] # Normal version
    

    ag = rag.AxonGame(chemMap1, chemMap2, coactNum=10, branchNum=branchNum, boundStr=boundStr, autoScale=True, coType="Cov",
                      senByCo = True,
                      seed=True, # True
                      seedNoise = .1,# .1,
                      senByCoBeta = .1,
                      plexNum=plex,
                      dendRad = dRad,
                      dendStride= dStride,
                      chemoStr1=chemoStr1, 
                      chemoStr2=chemoStr2,
                      corrStr= 500, # 500 was good
                      compStr=.016, 
                      exubPeriod = 200,
                      stabilizePeriod = 100,
                      branchExub = [2,  .025],  #just changed to the standard gaussain formula
                      growProps = [1, .40],
                      seniorStrs=seniorStrs)
    
    # Figures
    agFig   = rag.AxonGameFig(ag)
    agEye   = rag.AxonGameTraceFig(ag, np.squeeze(eyeVec), "Binocularity", cmap='gray') 
    wnFig   = rsc.TopoFeatureFig(inputImShape, outSize, "Window Structure")
    imFig   = tfu.ImageFigure()
    ecFig   = tfu.ContourFigure()
    
    start = time.time()
    
    W_pack = np.zeros((1,1)) 
    Ind_pack = np.zeros((1,1))
    
    aRad = 0
    
    with tf.Session() as sess:
    
        t = 0
               
        # Grow axon game
        for i in range(0,300):
            
            
            batch = 50
            #boost = 10
            spontActs = np.zeros( (in_ln, batch) )
            for j in range(0, batch):
                s_val = sess.run(s, feed_dict={feedOn:False})
                spontActs[:,j] = s_val[:,0]/np.max(s_val)
            
            im_val = sess.run(inputIm, feed_dict={feedOn:False})
            
            # Update model
            ag.run(1, newActivity = spontActs)
            
            # update graphics
            imFig.update(im_val, grey2rgb=True, opNorm=True)
            agFig.refresh()
            eyeValImg = agEye.refresh()
            ecFig.refresh([eyeValImg],[4],['gray'])
            #eyeValImg = tfu.SigSplit(eyeValImg,10)
            if np.mod(i, 10)==0:
                W_pack, Ind_pack = ag.getPackedW()
                wnFig.refresh(np.ones_like(W_pack), Ind_pack, order='C')
                
                
            # Calculate time
            mins = int( np.floor((time.time()-start)/60))
            secs = int( np.floor((time.time()-start)-mins*60))
            
            # Figure out the axon arber radius
            aRad = np.sqrt( np.mean(branchNum)/np.pi )
            
            print("\n____________________")
            print("Time      : "+str(mins)+"m  "+str(secs)+"s")
            print("Step      : "+str(t))
            print("Exuberance: "+str(np.round(ag.gauWidth, 3)))
            print("Grow Prop : "+str(np.round(ag.growProp*100))+"%")
            print("Seniority : "+str(np.round(ag.seniorStr, 6)))
            print("Sparsity  : "+str(np.round(ag.W.nnz/np.prod(ag.W.shape), 4 ))+"%")
            print("Max Depth : "+str(np.max(ag.branchDepth))+" brchs")
            print("Avg Depth : "+str(np.round(np.mean(ag.branchDepth)))+" brchs")
            print("DW  Depth : "+str(W_pack.shape[0])+' brchs')
            print("Dend Rad  : "+str(dRad))
            print("Axon Rad  : "+str(aRad))
            print("Penalty   : "+str(np.round(ag.avgFit, 3))+" p/b")
            print(ag.timeReport)
            
            t = t + 1
            
            plt.pause(.1)
        
        # Save learned window structure for later
        print("Saving Window Structure")
        W_pack, Ind_pack = ag.getPackedW()
        eyeValImg = agEye.refresh()
        pDict = {"inputImShape":inputImShape, "outSize":outSize, "W_pack":W_pack, "Ind_pack":Ind_pack, "eyeVec":eyeVec, "eyeValImg":eyeValImg,
                 "aRad":aRad}
        pickleWindowData(pDict)
        
#Train_AxonGame()


def test_saved_windows():
    
    pDict = unpickleWindowData()
    W_pack_val = pDict["W_pack"]
    Ind_pack_val = pDict["Ind_pack"]
    inputImShape = pDict["inputImShape"]
    outSize = pDict["outSize"]
    eyeValImg = pDict["eyeValImg"]
    aRad = pDict["aRad"]
    
    print("Axon Arbor Radius: "+str(aRad))

    wnFig = rsc.TopoFeatureFig(inputImShape, outSize, "Window Structure")
    wnFig.refresh(np.ones_like(W_pack_val), Ind_pack_val, order='C')
    
    # use a saturating contrast
    #sigEyeImg = tfu.SigSplit(eyeValImg,10)
    ecFig = tfu.ContourFigure()
    ecFig.refresh([eyeValImg],[4],['gray'])
    
    
#test_saved_windows()


def AG_OR(dRad=3, epochs=500):
    
    # Cleanup graph
    tf.reset_default_graph()
    
    # Get window matrices
    pDict = unpickleWindowData()
    W_pack_val = pDict["W_pack"]
    Ind_pack_val = pDict["Ind_pack"]
    eyeValImg = pDict["eyeValImg"]
    cShape = pDict["outSize"]
    aRad = pDict["aRad"]
    
    # vary the dendritic window size
    # Note this can be done inside the axon game instance
    # but would require saving the axon game itself (which has a high storage cost)
    Ind_pack_val = rag.DendriticWindow(cShape, Ind_pack_val, radius=dRad)
    W_pack_val = np.random.uniform( size=Ind_pack_val.shape)
    
    eyeSize = [50, 50]
    eyePad = 20
    imSize = np.array(eyeSize)+eyePad   

    outSize = np.array([100,100])
    
    # Corrected the filtering operation the 2c**2 instead of c**2
    STD = .5 # Was .375, but changed to be consistent with pre-dev learning...
    s, in_ln, inputIm, inputImShape, vec_feed, feedOn, chemMap1, eyeVec  = CreateInputStream(eyeSize, eyePad, STD)
    
    
    print("Setting Up Simulation:")
    
    # Making the orientation battery is a huge bottle neck!!
    swWidth = .5
    onOff =  False#True
    # Gap increased to 36, from 12 (in paper)
    # This has been changed from the version in the paper, but should largely
    # eliminate some high-frequency noise in the orientation map rather than
    # change the global structure of the map
    orData, orImgs, orVal = ss.SimpleBatteryOR(imSize, swWidth, deg=12, gap=36, onoffSurround=onOff)
    
    
    scaling = 4  # 4/sp:ones
    #orData = rsc.MaxNormStim(orData)*scaling


    # Define windowed inputs
    with tf.variable_scope("in"):
        s_winds, W_pack, W_up, W_pack_feed, Ind_pack_feed = tfu.WindowConnected(W_pack_val.shape, s)

        
    # Define ops that need to be displayed
    r_1 = []
    r_2 = []
    r_k = []    
    h_1 = []
    h_2 = []
    h_k = []
    h_all = []
    r_all = []
    learn = []     
    sig_all = []    

    print("Setup H-LISSOM")
    # Define Lissom layer # Used with current paper
    with tf.variable_scope("protolis"):
        r_all, h_all, learn, sig_all = rsc.HomeoLissomLayer(W_pack, s_winds, outSize, oRelTo="unlearn",
                                                            oTarg=.65,
                                                            ds=[1, 6] #ds=[1.3, 8]
                                                            )
    
    # Define Lissom layer (Tweaked version) # More stable
    #with tf.variable_scope("protolis"):
    #    r_all, h_all, learn, sig_all = rsc.HomeoLissomLayer(W_pack, s_winds, outSize,
    #                                                        reps = 15, ds = [1, 8.0], eBoost=1, iBoost=2,
    #                                                        eBeta=.004, eTarg=.005, eRel=True, cBeta=.004, 
    #                                                        oRate=.001, oTarg=.5, oRelTo="unthresh", oBool=False,
    #                                                        lRate=0.05, wRatio = 1/5, l2Rate=0.5, latStyle = 'fixed')
        r_1 = r_all[0]
        r_2 = r_all[1]
        r_k = r_all[-1]    
        h_1 = h_all[0]
        h_2 = h_all[1]
        h_k = h_all[-1]    

    

    print("Start Simulation")

    # setup tensor flow session
    with tf.Session() as sess:    
        
        # Initialize tf variables
        sess.run(tf.global_variables_initializer())    
        
        # Create display for lissom layer
        figAct = rsc.TopoActFig(outSize, "1st Activity", "k-th Activity")
        #figIn = TopoActFig(outSize, "In", "In Gain Control")
        
        print(vec_feed.shape)
        
        figOR = rsc.TopoFig(sess, vec_feed, h_k, in_ln, outSize, orData, orVal, 'circ', "Orientation Columns", smooth=False, levelSets=2)
        figOR.feed_dict[feedOn] = True
        imFig   = tfu.ImageFigure()
        wnFig   = rsc.TopoFeatureFig(inputImShape, outSize, "Window Structure")
        conFig  = tfu.ContourFigure()
        conFigPre   = tfu.ContourFigure()
        conFigPost  = tfu.ContourFigure()
        conFigComp  = tfu.ContourFigure()
        figOC   = rsc.TopoByWeight(outSize)
        
        
        s_val = []
        h_1_val = []
        h_k_val = []


        # Setup window structure
        sess.run([W_up], feed_dict={feedOn:False, W_pack_feed:W_pack_val,Ind_pack_feed:Ind_pack_val})

            
        # Main Loop for training test
        # Insure that all values are normalized before running
        sess.run([learn], feed_dict={feedOn:False, s:np.zeros((in_ln,1))})
        
        epochSteps = 100
        start = time.time()
        for i in range(0, epochs):
            
            # Training Loop
            for j in range(0,epochSteps):
                
                             
                feed_dict={feedOn:False}
                             
                W_pack_val, im_val, s_val, r_1_val, r_2_val, h_1_val, h_2_val, h_k_val, sig0_val, sig1_val, sig2_val, sig3_val, sig4_val, sig5_val, dum = sess.run([W_pack, inputIm, s, r_1, r_2, h_1, h_2, h_k, sig_all[0], sig_all[1], sig_all[2], sig_all[3], sig_all[4], sig_all[5], learn], feed_dict=feed_dict)
            
                
            mins = int( np.floor((time.time()-start)/60))
            secs = int( np.floor((time.time()-start)-mins*60))
            
            print("\n_________________________")
            print("Time        : "+str(mins)+"m  "+str(secs)+"s")
            print("Epoch       : "+str(i))
            print("Step        : "+str(i*epochSteps))
            print("---")
            print("Max s_gc    : "+str(np.max(s_val)) )
            print("---")
            print("Avg  h_1    : "+str(np.round(np.mean(h_1_val),3 ))  )
            print("Span h_1    : ["+str(np.round(np.min(h_1_val),3 )) +', '+ str(np.round(np.max(h_1_val),3 ))  +']' )
            print("---")
            print("Avg  h_k    : "+str(np.round(np.mean(h_k_val),3 ))  )
            print("Span h_k    : ["+str(np.round(np.min(h_k_val),3 )) +', '+ str(np.round(np.max(h_k_val),3 ))  +']' )
            print("---")
            print("Run h_k     : "+str(np.round(np.mean(sig0_val),3 )))
            print("Theat0 Val  : "+str(np.round(np.mean(sig1_val),3)))
            print("Run h_un    : "+str(np.round(np.mean(sig2_val),3)))
            print("Rel oTarg   : "+str(np.round(np.mean(sig3_val),3)))
            print("Run windNorm: "+str(np.round(np.mean(sig4_val),3)))
            print("Epsilon     : "+str(np.round(np.mean(sig5_val),3)))
            
            
            print("---")
            print("Span W      : ["+str(np.round(np.min(W_pack_val),3 )) +', '+ str(np.round(np.max(W_pack_val),3 ))  +']' )
            print("Dist W      : avg="+str(np.round(np.mean(W_pack_val),4))+", std="+str(np.round(np.std(W_pack_val), 4)))
            print("---")
            print("Selectivity : "+str(np.round(np.mean(figOR.selIm),4)))
            
            
            # Pick a random orientation battery image
            pick=np.random.choice(orData.shape[1])
            im_val2 = sess.run(inputIm, feed_dict={feedOn:True, vec_feed:orData[:,pick,np.newaxis]})    
            imFig.update(im_val2, grey2rgb=True, opNorm=True)
            figAct.refresh(h_1_val,h_k_val)
            #figIn.refresh(s_val,s_gc_val)
            
            # Get orientation map
            if np.mod(i,2)==0:
                orValIm, orIm  = figOR.refresh()
                orFold1 = np.abs(orValIm-.5)
                orFold2 = np.abs(np.mod(orValIm+.5,1)-.5)
                
                ocImg  = figOC.refresh(W_pack_val, Ind_pack_val, eyeVec)
                
                conFig.refresh([ocImg],[1],['gray'],f=[False],bg=orIm)
                
                conFigPre.refresh( [eyeValImg],[6, 6],['gray'],f=[True])
                conFigPost.refresh([ocImg],[6, 6],['gray'],f=[True])
                
                bgGray = np.ones([2,2,3])*.9
                conFigComp.refresh([eyeValImg, ocImg],[3, 3],['Blues', 'Reds'],f=[False, False], bg=bgGray)
                
                # Dark grey background
                #bgGray = np.ones([2,2,3])*.9
                #orGray = orIm*.5
                #conFig.refresh([orIm[:,:,0],orIm[:,:,1],orIm[:,:,2], ocImg],[1, 1, 1, 1],[],colors=['blue','blue','blue','red'], bg=bgGray)
                
                
                
            #figOC.refresh()
            wnFig.refresh(W_pack_val, Ind_pack_val, order='C')
            plt.pause(.1)
        
    return ocImg, eyeValImg
    
#AG_OR()
    

def CouplingTest():
    # Radius is approximately 6
    
    postOC_ls = []
    preOC_ls = []
    dRad_ls = []
    
    #              3, 18, 3    
    for i in range(2, 10, 2):
        postOC, preOC = AG_OR(i,150)
        postOC_ls.append(postOC)
        preOC_ls.append(preOC)
        dRad_ls.append(i)

        saveDict = {"preOC_ls":preOC_ls, "postOC_ls":postOC_ls, "dRad_ls":dRad_ls}
    
        # Store data (serialize)
        print("\n________\n________\n________\n________")
        print("Saving")
        with open('couplingData.pickle', 'wb') as handle:
            pkl.dump(saveDict, handle, protocol=pkl.HIGHEST_PROTOCOL)
    
    
    
#CouplingTest()    

from scipy.stats.stats import pearsonr   
def CouplingDisplay():
    
    
    # Load data (deserialize)
    with open('couplingData.pickle', 'rb') as handle:
        saveDict = pkl.load(handle)
    
    postOC_ls = saveDict["postOC_ls"]
    preOC_ls  = saveDict["preOC_ls"]
    dRad_ls  = saveDict["dRad_ls"]
    #dRad_ls = [2,4,6,8]

    fig = plt.figure()
    fig2 = plt.figure()
    
    plotNum = len(postOC_ls)
    
    x = np.linspace(0, 1, postOC_ls[0].shape[0])
    y = np.linspace(0, 1, postOC_ls[0].shape[1])
    X, Y = np.meshgrid(x, y)
    
    for i in range(0, len(postOC_ls)):
        ax = fig.add_subplot(1,plotNum,i+1)
        ax2 = fig2.add_subplot(1,plotNum,i+1)
        
        preImg = preOC_ls[i]
        postImg = postOC_ls[i]
        dRad = dRad_ls[i]
    
        numLev = 3
        
        #bg = np.ones( (3,3,3) )*.9
        norm = tfu.BoxNorm(tfu.SigSplit(postImg-np.mean(postImg),50))
        ax.imshow(norm, cmap='gray', interpolation='nearest', origin='lower',
                  extent=(0, 1, 0, 1))
        
        ax2.imshow(norm, cmap='gray', interpolation='nearest', origin='lower',
                  extent=(0, 1, 0, 1))
        ax2.axis('off')
        
        ax.contour(X,Y,preImg,numLev, cmap='Reds', linewidths=2 )
        #ax.contour(X,Y,postImg,numLev, cmap='Reds', linewidths=2 )
        ax.axis('off')
        
        preVec  = np.ravel(preImg)
        postVec = np.ravel(postImg)
        
        r, p = pearsonr(preVec,postVec)
        r2 = r**2

        print("Pre-Post Correlation (D="+str(dRad)+", A=6): "+str(np.round(r2,3))+",  p <= "+str(np.round(p,6)))

# These commands should run the entire simulation
Train_AxonGame()
CouplingTest()
CouplingDisplay()